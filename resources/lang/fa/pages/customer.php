<?php
return [
    'page_title' => 'مشتریان',
    'index' =>[
        'id' => 'شناسه',
        'first_name'=>'نام',
        'last_name'=>'نام خانوادگی',
        'mobile'=>'شماره همراه',
        'status'=>'وضعیت',
        'edit'=>'ویرایش',
        'national_code'=>'کد ملی',
        'address'=>'مدیریت آدرس ها',
        'tels'=>'تلفن ها',
        'prepend'=>'پیشوند',
        'name'=>'نام',
        'operation'=>'عملیات',
        'not_found'=>'هیچ آدرسی یافت نشد',
    ],
    'manage_address'=>[
        'sender_title' => 'لیست آدرس های مبداء',
        'receiver_title' => 'لیست آدرس های مقصد',
        'id'=>'شناسه',
        'title'=>'عنوان آدرس',
        'name'=>'نام گیرنده',
        'sender_province'=>'استان مبداء',
        'sender_city'=>'شهر مبداء',
        'receiver_province'=>'استان مقصد',
        'receiver_city'=>'شهر مقصد',
        'operation'=>'عملیات',
        'mobile'=>'شماره همراه',
        'tels'=>'تلفن ',
    ],

];

