<?php
return [
    'page_title' => 'سفارشات',
    'not_found'=>'موردی پیدا نشد',
    'index' =>[
        'id' => 'شناسه',
        'tarefe' => 'تعرفه',
        'product' => 'محصول',
        'status'=>'وضعیت',
        'edit'=>'ویرایش',
        'sender'=>'فرستنده',
        'receiver'=>'گیرنده',
        'driver'=>'راننده',
        'tonage'=>'تناژ',
        'price'=>'قیمت',
        'customer'=>'مشتری',
        'address'=>'آدرس',
        'new'=>'سفارش جدید',
        'operation'=>'عملیات',
    ],

];
