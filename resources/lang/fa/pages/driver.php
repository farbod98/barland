<?php
return [
    'page_title' => 'رانندگان',
    'not_found'=>'موردی پیدا نشد',
    'index' =>[
        'id' => 'شناسه',
        'prepend' => 'پیشوند',
        'name'=>'نام راننده',
        'mobile'=>'شماره همراه',
        'status'=>'وضعیت',
        'edit'=>'ویرایش',
        'national_code'=>'کد ملی',
        'address'=>'آدرس',
        'tels'=>'تلفن ها',
        'smart_code'=>'کد هوشمند',
        'car'=>'ماشین',
        'car_type'=>'نوع خودرو'
    ],

];
