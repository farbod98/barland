<?php
return [
    'toastr_success' => 'عملیات موفق',
    'toastr_error' => 'عملیات ناموفق',
    'failed_captcha' => 'کپچای وارد شده صحیح نیست',
    'captcha_label' => 'حروف تصویر را وارد کنید',
    'sessionTimeout_title' => 'اعلان پایان اعتبار صفحه',
    'sessionTimeout_message' => 'به علت عدم فعالیت، مدت زمان اعتبار حضور شما در این صفحه به اتمام رسیده است',
    'sessionTimeout_timer' => ['0'=>'پس از ','1'=>' ثانیه به صفحه قفل هدایت می شوید'],
    'sessionTimeout_logoutButton' => 'خروج',
    'sessionTimeout_keepAliveButton' => 'متصل بمان',
    'lock_screen' => 'قفل کردن صفحه',
    'new_record' => 'ثبت جدید',
    'choose_an_option' => 'یک گزینه را انتخاب کنید',
    'export' => 'خروجی',
    'export_type' => [
        'print' => 'پرینت',
        'copy' => 'کپی',
        'excel' => 'اکسل',
        'csv' => 'CSV',
        'pdf' => 'PDF',
    ],
];

