<?php
    return [
        'copyright' => 'حقوق کپی رایت محفوظ است.',
        'Whoops!' => 'اوه!',
        'Hello' => 'سلام!',
        'Regards' => 'با احترام',
        'link_not_worked'=> "اگر در کلیک بر روی دکمه :actionText مشکلی دارید, لینک زیر را در مرورگر وب خود کپی و جای گذاری کنید: [:actionURL](:actionURL)",

        'reset_password_subject'=>'بخش تنظیم مجدد گذرواژه شما',
        'reset_password_title'=>'ما یک درخواست بازنشانی رمزعبور را برای حساب شما دریافت کرده ایم.',
        'reset_password_btn' => [
            'text' => 'بازیابی رمز عبور',
        ],
        'reset_password_no_request'=>'اگر شما این درخواست را نداده اید، نیازی نیست هیچ اقدامی انجام دهید.',
    ];

