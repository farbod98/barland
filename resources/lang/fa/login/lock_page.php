<?php
    return [
        'title' => 'صفحه قفل شده است',
        'btn' => 'ورود',
        'password' => 'رمز عبور',
        'before_name' => 'شما ',
        'after_name' => ' نیستید؟',
    ];

