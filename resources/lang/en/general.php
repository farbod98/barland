<?php
return [
    'toastr_success' => 'Successful operation',
    'toastr_error' => 'Failed operation',
    'failed_captcha' => 'The imported captcha is not correct',
    'captcha_label' => 'Enter the letters of the image',
    'sessionTimeout_title' => 'Session Timeout Notification',
    'sessionTimeout_message' => 'Your session is about to expire.',
    'sessionTimeout_timer' => ['0'=>'Redirecting in ','1'=>' seconds.'],
    'sessionTimeout_logoutButton' => 'Logout',
    'sessionTimeout_keepAliveButton' => 'Stay Connected',
    'lock_screen' => 'Lock screen',
    'new_record' => 'New Record',
    'choose_an_option' => 'Choose an option',
    'export' => 'Export',
    'export_type' => [
        'print' => 'Print',
        'copy' => 'Copy',
        'excel' => 'Excel',
        'csv' => 'CSV',
        'pdf' => 'PDF',
    ],
];