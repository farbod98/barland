<?php
    return [
        'copyright' => 'All rights reserved.',
        'Whoops' => 'Whoops!',
        'Hello' => 'Hello!',
        'Regards' => 'Regards',
        'link_not_worked'=>"If you’re having trouble clicking the :actionText button, copy and paste the URL below\n into your web browser: [:actionURL](:actionURL)",
        'reset_password_subject'=>'Your Reset Password Subject Here',
        'reset_password_title'=>'You are receiving this email because we received a password reset request for your account.',
        'reset_password_btn' => [
            'text' => 'Reset Password',
        ],
        'reset_password_no_request'=>'If you did not request a password reset, no further action is required.',
    ];

