<?php
    return [
        'title' => 'Locked',
        'btn' => 'Login',
        'password' => 'Password',
        'before_name' => 'Not ',
        'after_name' => ' ?',
    ];

