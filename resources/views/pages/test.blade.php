<form class="kt-form">
    <div class="kt-portlet__body">
        <div class="kt-form__section kt-form__section--first">
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Full Name:</label>
                <div class="col-lg-6">
                    <input type="email" class="form-control" placeholder="Enter full name">
                    <span class="form-text text-muted">Please enter your full name</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Email address:</label>
                <div class="col-lg-6">
                    <input type="email" class="form-control" placeholder="Enter email">
                    <span class="form-text text-muted">We'll never share your email with anyone else</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Communication:</label>
                <div class="col-lg-12 col-xl-8">
                    <div class="kt-checkbox-inline kt-padding-top-3">
                        <label class="kt-checkbox">
                            <input type="checkbox"> Email
                            <span></span>
                        </label>
                        <label class="kt-checkbox">
                            <input type="checkbox"> SMS
                            <span></span>
                        </label>
                        <label class="kt-checkbox">
                            <input type="checkbox"> Phone
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Credit Card</label>
                <div class="col-lg-6 ">
                    <div class="input-group">
                        <input type="text" class="form-control" name="creditcard" placeholder="Enter card number">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="la la-credit-card"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="kt_repeater_2">
                <div class="form-group  row">
                    <label class="col-lg-3 col-form-label">Contact:</label>
                    <div data-repeater-list="" class="col-lg-6">
                        <div data-repeater-item class="kt-margin-b-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
																			<span class="input-group-text">
																				<i class="la la-phone"></i>
																			</span>
                                </div>
                                <input type="text" class="form-control form-control-danger" placeholder="Enter telephone">
                                <div class="input-group-append">
                                    <a href="javascript:;" class="btn btn-danger btn-icon">
                                        <i class="la la-close"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col">
                        <div data-repeater-create="" class="btn btn btn-warning">
																	<span>
																		<i class="la la-plus"></i>
																		<span>Add</span>
																	</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="reset" class="btn btn-brand">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
