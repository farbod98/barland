@extends('master')

@section('before-styles')
<link href="{{asset('custom-assets/pages/custom/jstree/jstree.bundle.rtl.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('custom-assets/pages/custom/jstree/jstree.bundle.min.css')}}" rel="stylesheet" type="text/css" />--}}
@endsection

@section('style')
    <style>
        /*Style Section*/
        #kt_tree_4 ul li{line-height: 30px;}
        #kt_tree_4 ul li a{font-size: 18px;}
    </style>
@endsection


@section('content')
   {{--
    اجزای داخل صفحه ازینجا شروع میشن
    <br>
    <example-component />--}}

    <div class="row">
        <div class="col-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h2>Contact me</h2>
                </div>
                <div class="card-body">
                    <!--
                        Our component:
                    -->
                    <contact-form></contact-form>

                    {{--<input type="text" id="text" value="">
                    <input type="text" id="mobile" value="">
                    <input type="password" id="password" value="">
                    <input type="password" id="password_confirmation" value="123">--}}
                </div>
            </div>
        </div>
    </div>



   <div class="col-lg-6">
       <!--begin::Portlet-->
       <div class="kt-portlet">
           <div class="kt-portlet__head">
               <div class="kt-portlet__head-label">
                   <h3 class="kt-portlet__head-title">
                       Contextual Menu
                   </h3>
               </div>
           </div>
           <div class="kt-portlet__body">
               <div id="kt_tree_4" class="tree-demo"></div>
           </div>
       </div>
   </div>

@endsection


@section('script')
{{--    <script src="{{asset('custom-assets/pages/custom/jstree/jstree.bundle.js')}}" type="text/javascript"></script>--}}
    <script src="{{asset('custom-assets/pages/custom/jstree/jstree.bundle-fa.js')}}" type="text/javascript"></script>
    <script src="{{asset('custom-assets/pages/components/extended/treeview.js')}}" type="text/javascript"></script>
    {{--<script>
        $(document).ready(function () {
            new_validate();
        });
        /*Script Section*/
        function new_validate() {
            let fields = {
                'text': 'label:عنوان|required:true|min:3|max:100',
                'mobile': 'label:تلفن همراه|required:true|min:11|max:11',
                'password': 'label:رمز عبور|required:true|min:6|required_with:password_confirmation|same:password_confirmation|min:6|max:15',
                'password_confirmation': 'label:تکرار رمز عبور|required:true|min:6|max:15'
            };

            let inputs={};
            inputs['errors']={};
            $.each(fields,function(key,field){
                inputs[key]={};
                let input_value=$(`#${key}`).val();
                //console.log(key);
                //console.log(field);
                let filters = field.split('|');
                $.each(filters,function(index,filter){
                    let kind=filter.split(':');
                    //console.log(kind[0]);
                    switch (kind[0]) {
                        case 'label':
                            inputs[key][kind[0]]=kind[1];
                            //console.log(kind[1]);
                            break;
                        case 'required':
                            if(input_value==='')
                                inputs['errors'][key]=kind[0];
                            // console.log(kind[1]);
                            break;
                        case 'min':
                            //console.log(kind[1]);
                            break;
                        case 'max':
                            //console.log(kind[1]);
                            break;
                        case 'mobile':
                            //console.log(kind[1]);
                            break;
                        case 'email':
                            //console.log(kind[1]);
                            break;
                        case 'checkbox':
                            //console.log(kind[1]);
                            break;
                        case 'radio':
                            //console.log(kind[1]);
                            break;
                        case 'same':
                            //console.log(kind[1]);
                            break;
                    }
                    //required_with:password_confirmation
                    //(index):1246 3
                    //(index):1247 same:password_confirmation
                });
            });
            console.log(inputs);
        }
    </script>--}}
@endsection