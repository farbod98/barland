<script>
    let all_province_sender=@json(\App\Models\Province::get_accepted_provinces());
    let all_province_receiver=@json(\App\Models\Province::get_provinces()->get());

    let sender_marker='';
    let receiver_marker='';
    let cities=@json(\App\Models\City::all());
    let provinces=@json(\App\Models\Province::get_accepted_provinces());
    let all_provinces=@json(\App\Models\Province::all());
    let marker_owner='';

    function get_all_provinces(type,selected){
        console.log(type);
        let provinces = type=== 'sender' ? all_province_sender : all_province_receiver;
        console.log(provinces);
        let options='<option value=""></option>';
        $.each(provinces,function(key,province){
            options +=`<option value="${province.id}" ${Number(province.id) === Number(selected) ? 'selected' : ''}>${province.title}</option>`;
        });
        return options;
    }

    function get_all_province_cities(type,province,selected_city){
        let options='<option value=""></option>';
        $.each(cities, function (key, city) {
            if (Number(city.province_id) === Number(province) )
                options += `<option value="${city.id}" ${Number(city.id) === Number(selected_city) ? 'selected' : ''}>${city.title}</option>`;
        });
        return options;
    }

    function get_details_of_address(type,id){
        mz_global_async_ajax('{{asset('order/get/address')}}','post',{id:id,type:type}).then( (response) => {
            swal.close();
            if(response.status==='success'){
                let address = response.data;
                let receiver_data='';
                if(type==='receiver') {
                    let phones='';
                    let tels=JSON.parse(address.tels);
                    $.each(tels, function (key, tel) {
                        phones += `${tel} ${key+1 === tels.length ?'':' ، '}`;
                    });
                    receiver_data = `
                        <div class="col-12 item"><span class="title">تلفن همراه</span><span class="description">${address.mobile != null ? address.mobile : ''}</span></div>
                        <div class="col-12 item"><span class="title">تلفن</span><span class="description">${phones}</span></div>`;
                }
                let content=`<div class="row address_detail_box">
                                <div class="col-12 item"><span class="title">استان </span><span class="description">${address.city.province.title}</span></div>
                                <div class="col-12 item"><span class="title">شهر </span><span class="description">${address.city.title}</span></div>
                                <div class="col-12 item"><span class="title">آدرس </span><span class="description">${address.address}</span></div>
                                <div class="col-12 item"><span class="title">پلاک </span><span class="description">${address.pelak != null ? address.pelak :''}</span></div>
                                <div class="col-12 item"><span class="title">واحد </span><span class="description">${address.vahed != null ? address.vahed :''}</span></div>
                                ${receiver_data}
                            </div>`;
                global_modal_label.html('').append(` نمایش جزئیات آدرس: ${type==='sender'?address.title:address.name} `);
                //modal_header.css('display','none');
                global_modal_dialog.addClass('modal-lg').removeClass('mz-modal-width');
                global_modal_form.html('').append(content);
                global_modal_footer.html('').append('<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>');
                global_modal_id.modal('show');
            }
        });
    }

    function get_manage_address(type,id=null){
        let ajax = id!==null;
        marker_owner=type;
        let data={};
        global_modal_form.attr('action',`javascript:store_address_request(this,'${type}','${id}');`);
        global_modal_dialog.removeClass('modal-lg').addClass('mz-modal-width');
        let footer_submit = `<button type="submit" form="global_base_page_modal_form" class="btn btn-primary">${ajax===true?'به روز رسانی آدرس':'ثبت آدرس'}</button>`;
        global_modal_footer.html('').append(`${footer_submit} <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>`);
        if(ajax) {
            mz_global_async_ajax('{{asset('order/edit/address')}}','post',{id:id,type:type}).then( (response) => {
                swal.close();
                if(response.status==='success'){
                    let address = response.data;
                    data={
                        title:type==='sender'?address.title:address.name,
                        province:address.city.province_id,
                        city:address.city_id,
                        address:address.address,
                        no:address.pelak,
                        unit:address.vahed,
                        mobile:type==='sender'?'':address.mobile,
                        phone:type==='sender'?'':JSON.parse(address.tels),
                        zoom:address.zoom
                    };
                    let data_content=appendable_address(type,data);
                    global_modal_label.html('').append(` ویرایش آدرس: ${type==='sender'?address.title:address.name} `);
                    //modal_header.css('display','none');
                    global_modal_form.html('').append(data_content);
                    $('.select2').select2({placeholder:'انتخاب کنید'});
                    global_modal_id.modal('show');
                    let map_data = {latitude:address.lat,longitude:address.long,zoom:address.zoom};
                    setTimeout(function(){get_city_map(map_data,true);},1000);
                }
            });
        }else{
            data={title:'',province:'',city:'',address:'',no:'',unit:'',mobile:'',phone:'',zoom:''};
            global_modal_label.html('').append('ثبت آدرس جدید');
            let data_content=appendable_address(type,data);
            global_modal_form.html('').append(data_content);
            $('.select2').select2({placeholder:'انتخاب کنید'});
            global_modal_id.modal('show');
            setTimeout(function(){get_city_map();},1000);
        }
    }

    function appendable_address(type,data){
        console.log(data);
        let receiver_content=`<div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="receiver_mobile">تلفن همراه<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="receiver_mobile" name="receiver_mobile" aria-describedby="receiver_mobile_help" placeholder="" value="${data.mobile}">
                                                        <small id="receiver_mobile_help" class="form-text text-muted">تلفن همراه گیرنده را وارد کنید</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="receiver_phone">تلفن<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="receiver_phone" name="receiver_phone" aria-describedby="receiver_phone_help" placeholder="" value="${data.phone}">
                                                        <small id="receiver_phone_help" class="form-text text-muted">تلفن را وارد کنید</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
        let html=`<div class="row">
                    <div class="col-5">
                        <div class="add_new_address_inputs">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="${type==='sender'?'sender_title':'receiver_name'}">${type==='sender'?'عنوان آدرس مبداء':'نام گیرنده'} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="${type==='sender'?'sender_title':'receiver_name'}" name="${type==='sender'?'sender_title':'receiver_name'}" aria-describedby="${type==='sender'?'sender_title':'receiver_name'}_help" placeholder="" value="${data.title}">
                                        <small id="${type==='sender'?'sender_title':'receiver_name'}_help" class="form-text text-muted">${type==='sender'?'عنوان آدرس مبداء':'نام گیرنده'} را مشخص کنید</small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="${type}_province">استان ${type==='sender'?'مبداء':'مقصد'}<span class="text-danger">*</span></label>
                                                <select class="form-control select2" onchange="change_province(this,'${type}');" id="${type}_province" name="${type}_province" aria-describedby="${type}_province_help">
                                                    ${get_all_provinces(type,data.province)}
                                                </select>
                                                <small id="${type}_province_help" class="form-text text-muted">استان ${type==='sender'?'مبداء':'مقصد'} را انتخاب کنید</small>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="${type}_city">شهر ${type==='sender'?'مبداء':'مقصد'}<span class="text-danger">*</span></label>
                                                <select class="form-control select2" onchange="change_city(this,'${type}');" id="${type}_city" name="${type}_city" aria-describedby="${type}_city_help">${get_all_province_cities(type,data.province,data.city)}</select>
                                                <small id="${type}_city_help" class="form-text text-muted">شهر ${type==='sender'?'مبداء':'مقصد'} را انتخاب کنید</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" aria-label="zoom" id="${type}_zoom" name="${type}_zoom" value="${data.zoom}">

                            ${type === 'receiver'? receiver_content:''}

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="${type}_address">جزئیات آدرس ${type==='sender'?'مبداء':'مقصد'}<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="${type}_address" name="${type}_address" aria-describedby="${type}_address_help" placeholder="" readonly value="${data.address}">
                                        <small id="${type}_address_help" class="form-text text-muted">آدرس ${type==='sender'?'مبداء':'مقصد'} را روی نقشه مشخص کنید</small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="${type}_no">پلاک</label>
                                                <input type="number" step="0.1" class="form-control" id="${type}_no" name="${type}_no" aria-describedby="${type}_no_help" placeholder="" value="${data.no}">
                                                <small id="${type}_no_help" class="form-text text-muted">پلاک شرکت را مشخص کنید</small>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="${type}_unit">واحد</label>
                                                <input type="number" class="form-control" id="${type}_unit" name="${type}_unit" aria-describedby="${type}_unit_help" placeholder="" value="${data.unit}">
                                                <small id="${type}_unit_help" class="form-text text-muted">واحد شرکت را مشخص کنید</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-7" id="container_map">
                        <div id="map_id" style="height: 500px;"></div>
                    </div>
                </div>`;
        return html;
    }

    function change_province(element,type) {
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let province=$(element).val();
        let data='<option value=""></option>';
        if(type==='sender') {
            $.each(cities, function (key, city) {
                if (Number(city.province_id) === Number(province) && Number(city.accepted) === 1)
                    data += `<option value="${city.id}">${city.title}</option>`;
            });
        }else{
            $.each(cities, function (key, city) {
                if (Number(city.province_id) === Number(province) )
                    data += `<option value="${city.id}">${city.title}</option>`;
            });
        }

        $(`#${type}_city`).html('').append(data);
        let map_data = {latitude:'31.6413517',longitude:'54.4177819',zoom:'7.25'};
        $.each(all_provinces,function(key,item){
            if(Number(item.id)===Number(province))
                map_data=item;
        });
        get_city_map(map_data);
        swal.close();
    }

    function change_city(element,type){
        let city=$(element).val();
        let map_data = {latitude:'31.6413517',longitude:'54.4177819',zoom:'7.25'};
        $.each(cities,function(key,item){
            if(Number(item.id)===Number(city))
                map_data=item;
        });
        get_city_map(map_data);
    }

    function get_city_map(data=null,edit=false){
        $("#container_map").html('').append(`<div id="map_id" style="height: 500px;"></div>`);
        let container = L.DomUtil.get('map_id');if(container != null){ container._leaflet_id = null; }

        let map_data={latitude:'32.1155095',longitude:'54.4595693',zoom:'5'};
        if(data!==null)
            map_data = {latitude: data.latitude, longitude: data.longitude, zoom: data.zoom};
        /////////////////////////////////////////////////////////////////////
        L.mapbox.accessToken = 'pk.eyJ1IjoibXJtYWppZHoiLCJhIjoiY2s2N2Y0eW54MDRmdzNscXQ3a3ZqdW85NCJ9.Tj2XpAzzlVVRu7d380SK0A';
        let current_map = L.mapbox.map('map_id')
            .setView([map_data.latitude, map_data.longitude], map_data.zoom)
            .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
        // current_map.addControl(new MapboxBrowserLanguage({  defaultLanguage: 'fa'}));
        let markers = new L.MarkerClusterGroup();
        current_map.addLayer(markers);
        /////////////////////////////////////////////////////////////////////
        sender_marker = L.marker([1, 1], {draggable: 'true'});
        receiver_marker = L.marker([10, 10], {draggable: 'true'});

        if(data!==null && edit===true) {
            let current_marker = marker_owner==='sender'?sender_marker:receiver_marker;
            current_marker.setLatLng([data.latitude, data.longitude]);
            current_marker.addTo(current_map);
        }
        let lng,lat;
        current_map.on('click', function (e) {
            $(`#${marker_owner}_zoom`).val(current_map.getZoom());
            console.log(current_map.getZoom());
            if(marker_owner==='sender'){
                sender_marker.setLatLng([e.latlng.lat, e.latlng.lng]);
                sender_marker.addTo(current_map);
                lng = sender_marker.getLatLng().lng;
                lat = sender_marker.getLatLng().lat;
            }
            else if(marker_owner==='receiver'){
                receiver_marker.setLatLng([e.latlng.lat, e.latlng.lng]);
                receiver_marker.addTo(current_map);
                lng = receiver_marker.getLatLng().lng;
                lat =receiver_marker.getLatLng().lat;
            }
            $.ajax({
                url: 'https://nominatim.openstreetmap.org/reverse?accept-language=fa',
                data: {
                    format: 'jsonv2',
                    lat: lat,
                    lon: lng,
                },
                success: function (response) {
                    $(`#${marker_owner}_address`).val(response.display_name);
                    console.log(response);
                }
            });
        });
    }

    function store_address_request(element,type,id){
        if (!validate_new_address(element,type)) {
            return;
        }

        let data = {};
        let url;
        data['customer_id'] = '{{$id}}';
        data['address'] = $(`#${type}_address`).val();
        data['zoom'] = $(`#${type}_zoom`).val();
        data['pelak'] = $(`#${type}_no`).val();
        data['vahed'] = $(`#${type}_unit`).val();
        if(type==='sender') {
            data['title'] = $(`#sender_title`).val();
            data['long'] = sender_marker.getLatLng().lng;
            data['lat'] = sender_marker.getLatLng().lat;
            data['city_id']=$("#sender_city").val();
            url=id==='null'?`{{asset('/customer_address')}}`:`{{asset('/customer_address')}}/${id}`;
        }
        else if(type==='receiver'){
            data['name']=$("#receiver_name").val();
            data['mobile']=$("#receiver_mobile").val();
            data['tels']=[];
            data['tels']=$("#receiver_phone").val();
            data['city_id'] = $("#receiver_city").val();
            data['long'] = receiver_marker.getLatLng().lng;
            data['lat'] = receiver_marker.getLatLng().lat;
            url=id==='null'?`{{asset('/receiver')}}`:`{{asset('/receiver')}}/${id}`;
        }

        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        $.ajax(url, {
            data: data,
            type: id==='null'?'post':'patch',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    swal.fire({
                        title: id==='null'?'ثبت':'به روز رسانی',
                        text: `عملیات ${id==='null'?'ثبت':'به روز رسانی'} آدرس با موفقیت انجام شد.`,
                        type: "success",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                    let tr_data='';
                    if(id==='null'){
                        tr_data=[
                            response.address.id,
                            type==='sender'?response.address.title:response.address.name,
                            response.address.city.province.title,
                            response.address.city.title,
                            `<a href="javascript:;" class="table-link-icon" title="جزئیات" onclick="get_details_of_address('${type}','${response.address.id}');"><i class="fas fa-eye"></i></a>
                             <a href="javascript:;" class="table-link-icon" title="ویرایش" onclick="get_manage_address('${type}','${response.address.id}');"><i class="fas fa-edit"></i></a>`
                        ];
                        let table=$(`#DataTables_Table_${type==='sender'?0:1}`).DataTable();
                        let tr_id=`tr_${type}_${response.address.id}`;
                        table.row.add(tr_data).node().id=tr_id;
                        table.draw(false);
                        $(`#${tr_id} td:last-child`).addClass('multi-operand');
                    }else{
                        tr_data=`<td>${response.address.id}</td>
                                <td>${type==='sender'?response.address.title:response.address.name}</td>
                                <td>${response.address.city.province.title}</td>
                                <td>${response.address.city.title}</td>
                                <td class="multi-operand">
                                    <a href="javascript:;" class="table-link-icon" title="جزئیات" onclick="get_details_of_address('${type}','${response.address.id}');"><i class="fas fa-eye"></i></a>
                                    <a href="javascript:;" class="table-link-icon" title="ویرایش" onclick="get_manage_address('${type}','${response.address.id}');"><i class="fas fa-edit"></i></a>
                                </td>`;
                        $(`#tr_${type}_${id}`).html('').append(tr_data);
                    }
                    global_modal_id.modal('hide');
                }else{
                    swal.fire({
                        title: id==='null'?'ثبت':'به روز رسانی',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function validate_new_address(element,type){
        let flag=false;
        let error_message='لطفا ابتدا فیلد های الزامی را پر کرده سپس اقدام به ثبت کنید';
        let fields = {};
        fields['province'] = $(`#${type}_province`).val();
        fields['city'] = $(`#${type}_city`).val();
        fields['address'] = $(`#${type}_address`).val();
        if(type==='sender'){
            fields['title'] = $("#sender_title").val();
        }else{
            fields['name'] = $("#receiver_name").val();
            fields['mobile'] = $("#receiver_mobile").val();
            fields['phone'] = $("#receiver_phone").val();
        }
        $.each(fields,function(key,field){
            if(field==='')
                flag=true;
            if(key==='mobile'){
                let numbers = '0123456789';
                let num = persian_to_latin_num(field);
                let st = num.substr(0,2);
                if(num.length!==11){
                    flag=true;
                    error_message = 'شماره تلفن همراه باید 11 رقمی باشد';
                }
                for(let i=0;i<11;i++){
                    let char = num.substr(i,1);
                    if(numbers.indexOf(char)===-1){
                        flag=true;
                        error_message = 'فرمت تلفن همراه صحیح وارد نشده است';
                    }
                }
                if(st !=='09'){
                    flag=true;
                    error_message = 'شماره تلفن همراه با 09 شروع میشود';
                }
                if(field==='')
                    error_message = 'شماره تلفن همراه نمیتواند خالی باشد';
            }
        });


        if(flag) {
            swal.fire({
                title: 'ثبت آدرس',
                text: error_message,
                type: "error",
                confirmButtonText: 'بسیار خب',
                confirmButtonClass: "btn btn-brand"
            });
            return false;
        }
        return true;
    }

    function persian_to_latin_num(st){
        let ret = '';
        if(st !== '') {
            st  = st+'';
            ret = st.split('۰').join('0');
            ret = ret.split('۱').join('1');
            ret = ret.split('۲').join('2');
            ret = ret.split('۳').join('3');
            ret = ret.split('۴').join('4');
            ret = ret.split('۵').join('5');
            ret = ret.split('۶').join('6');
            ret = ret.split('۷').join('7');
            ret = ret.split('۸').join('8');
            ret = ret.split('۹').join('9');
        }
        return ret;
    }
</script>
