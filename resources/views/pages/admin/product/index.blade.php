@extends('templates.datatable')
@section('style')
    @parent
    <script src="{{asset('metronic-assets/select2/css/select2.min.css')}}"></script>
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }
        .mz_hidden{display:none !important;}
    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                  محصولات
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--                        @include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="customer_new();" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>عنوان</th>
                    <th>وضعیت</th>
                    <th>ویرایش</th>

                </tr>
                </thead>
                <tbody>
                @forelse ($products as $product)
                    <tr id="tr_car_type_{{$product->id}}">
                        <td>{{$product->id}}</td>
                        <td>{{$product->title}}</td>
                        <td><span onclick="status_change(this);" class="btn-small {{$product->active==0?'btn-danger':'btn-success'}}" data-id="{{$product->id}}" data-active="{{$product->active}}">{{$product->active==0?'غیر فعال':'فعال'}}</span></td>
                        <td>
                            <span onclick="car_type_edit(this,'{{$product->id}}');" class="btn-small btn-primary">ویرایش</span>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/application.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <div class="modal fade" id="car_type_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ویرایش </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="car_type_form" action="javascript:car_type_update(this);">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="first_name" class="form-control-label">عنوان</label>
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" id="submit_btn" form="car_type_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="customer_address_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">آدرس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body" id="customer_modal_body">

                </div>
                <div class="modal-footer">
                    <a href="customer_address/create">افزودن</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>






@endsection
@section('script')
    @parent
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}"></script>
    <script>


        function status_change(element) {
            let id=$(element).attr('data-id');
            let active=$(element).attr('data-active');
            if(Number(active)===1)
                active=0;
            else
                active=1;
            $.ajax({
                type:'put',
                url:`/admin/product/${id}`,
                data:{
                    active:active,
                    id:id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response) {
                    console.log(response);
                    if(response.status==='success') {
                        if(Number(response.data.active)===1)
                            $(element).removeClass('btn-danger').addClass('btn-success').attr('data-active',1).text('فعال');
                        else
                            $(element).removeClass('btn-success').addClass('btn-danger').attr('data-active',0).text('غیر فعال');
                    }
                }
            });
        }

        function car_type_edit(element,id){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`/admin/product/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    console.log(response);
                    if(response.status==='success') {
                        $("#title").val(response.data.title);
                        let data='';
                        $("#submit_btn").html('به روز رسانی');



                        $("#car_type_form").attr('action',`javascript:car_type_update(this,${id});`);
                        $("#car_type_edit_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

        function validation(element) {
            let form=$(element);
            let all_rules={rules:{
                    name:{required:true},
                    mobile:{
                        required:true,
                        digits:true,
                    },
                    national_code:{
                        required:true,
                        digits:true,
                    },
                }
            };

            form.validate(all_rules);
            if(!form.valid())
                return false;
            return true;
        }

        function car_type_update(element,id) {
            let form_id='#car_type_form';
            if(!validation(form_id))
                return;
            let title=$("#title").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`/admin/product/${id}`,
                data:{
                    title:title,
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let car_type=response.data;
                    if(response.status==='success') {
                        let data=`<td>${car_type.id}</td>
                                <td>${car_type.title}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${car_type.active===0?'btn-danger':'btn-success'}" data-id="${car_type.id}" data-active="${car_type.active}">${car_type.active===0?'غیر فعال':'فعال'}</span></td>
                                <td><span onclick="car_type_edit(this,'${car_type.id}');" class="btn-small btn-primary">ویرایش</span></td>`;
                        $("#tr_car_type_"+car_type.id).html('').append(data);
                        $("#car_type_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            })}

        function customer_new() {
            $("#title").val('');
            $("#submit_btn").html('ثبت');
            $("#car_type_edit_modal").modal('show');
            $("#car_type_form").attr('action',`javascript:car_type_store();`);
        }

        function car_type_store() {
            let form_id='#car_type_form';
            if(!validation(form_id))
                return;

            let title=$("#title").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'post',
                url:`/admin/product`,
                data:{
                    title:title,
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let car_type=response.data;
                    if(response.status==='success') {
                        let new_row=[
                            car_type.id,
                            car_type.title,
                            `<span onclick="status_change(this);" class="btn-small ${car_type.active===0?'btn-danger':'btn-success'}" data-id="${car_type.id}" data-active="${car_type.active}">${car_type.active===0?'غیر فعال':'فعال'}</span>`,
                            `<span onclick="car_type_edit(this,'${car_type.id}');" class="btn-small btn-primary">ویرایش</span>`
                        ];
                        {{--let data=`<tr id="tr_customer_${customer.id}">--}}
                        {{--        <td>${customer.id}</td>--}}
                        {{--        <td>${customer.name}</td>--}}
                        {{--        <td>${customer.mobile}</td>--}}
                        {{--        <td>${customer.national_id}</td>--}}
                        {{--        <td>${tels}</td>--}}
                        {{--        <td><span onclick="status_change(this);" class="btn-small ${customer.active===0?'btn-danger':'btn-success'}" data-id="${customer.id}" data-active="${customer.active}">${customer.active===0?'غیر فعال':'فعال'}</span></td>--}}
                        {{--        <td><span onclick="customer_address(this,'${customer.id}');" class="btn-small btn-dark" data-id="${customer.id}">@lang(__('pages/customer.index.address'))</span></td>--}}
                        {{--        <td><span onclick="customer_edit(this,'${customer.id}');" class="btn-small btn-primary">ویرایش</span></td>--}}
                        {{--        </tr>`;--}}
                        let table=$("#kt_table_1").DataTable();
                        table.row.add(new_row).node().id=`tr_car_type_${car_type.id}`;
                        table.draw(false);
                        $("#car_type_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

    </script>
@endsection
