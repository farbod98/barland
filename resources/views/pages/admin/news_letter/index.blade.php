@extends('templates.datatable')
@section('style')
    @parent
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('site/map/css/mapbox.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.Default.css')}}" rel="stylesheet" />
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }
        .mz_hidden{display:none !important;}
        .table-link-icon{font-size: 18px;display: inline-block;background: #e3e3e3;padding: 5px 8px 0px;border-radius: 5px;}

        .address_detail_box{}
        .address_detail_box .item{margin-bottom: 10px;padding: 5px 15px;}
        .address_detail_box .item:hover{background-color: #eee;}
        .address_detail_box span{}
        .address_detail_box .title{color:#0b57d5;margin-left: 15px;display: inline-block;width: 75px;}
        .address_detail_box .title:after{content: ':'}
        .address_detail_box .description{color: #333;display: inline;line-height: 35px;}
        .leaflet-touch .leaflet-control-attribution, .leaflet-touch .leaflet-control-layers, .leaflet-touch .leaflet-bar {display: none;}
        .select2{width: 100% !important;}
        .select2-container--default .select2-selection--single .select2-selection__rendered {line-height: 20px !important;}
        .select2-container .select2-selection--single {height: 38px !important;}
        .select2-container--default .select2-selection--single {border: 1px solid #ced4da !important;}
        .select2-container--default .select2-selection--single .select2-selection__arrow b {top: 67% !important;}
        .select2-container--default .select2-selection--single .select2-selection__arrow {top: 18px;}
        .modal.show .modal-dialog.mz-modal-width{
            margin-top: 100px;
        }
    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/customer.manage_address.sender_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--@include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="get_manage_address('sender');" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>ایمیل</th>
                    <th>تاریخ</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($items as $item)
                    <tr id="tr_news_letter_{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->jalaliDate}}</td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">موردی یافت نشد</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('script')
    @parent
    @include('site.pages.blog-sections.global_modal')
    <script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
    <script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('site/map/js/mapbox.js')}}"></script>
    <script src="{{asset('site/map/js/leaflet.markercluster.js')}}"></script>
{{--    @include('pages.manage_address_script')--}}
    <script>
        let all_contacts = @json($items);
        function show_content(type,id){
            let contact='';
            $.each(all_contacts,function(key,item){
                if(item.id==id){
                    contact=item;
                }
            });

            global_modal_label.html('').append(` نمایش پیام: ${contact.full_name} `);
            //modal_header.css('display','none');
            global_modal_dialog.addClass('modal-lg');//.removeClass('mz-modal-width');
            global_modal_form.html('').append(contact.content);
            global_modal_footer.html('').append('<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>');
            global_modal_id.modal('show');


        }
    </script>
@endsection
