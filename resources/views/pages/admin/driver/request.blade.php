@extends('templates.datatable')
@section('style')
    @parent
    <script src="{{asset('metronic-assets/select2/css/select2.min.css')}}"></script>
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;

        }
        .mz_hidden{display:none !important;}
        .mz-btn-badge{
            padding:2px 4px;
            margin:2px;
            font-size: 1rem;
            border:1px solid transparent;
        }
        .alert-primary{}

    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                 درخواست بار جدید
                </h3>
            </div>


        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>نام راننده</th>
                    <th> خودرو</th>
                    <th> صاحب بار</th>
                    <th>  تاریخ درخواست</th>
                    <th>عملیات</th>

                </tr>
                </thead>
                <tbody>
                @forelse ($data as $key=>$item)
                    <tr id="tr_driver_{{$item->id.'*'.$item->pivot->order_id}}">
                        <td>{{$key+1}}</td>
                        <td>{{$item->user->name}}</td>
                        <td>{{$item->car_type->title}}</td>
                        <td>{{\App\Models\Order::find($item->pivot->order_id)->customer->user->name}}</td>
                        <td>{{edit_date($item->pivot->created_at) }}</td>
                        <td>
                            @if($item['status']==0)
                            <span onclick="accept_request(this,'{{$item->id}}' ,'{{$item->pivot->order_id}}' );" class="btn-small btn-primary">انتخاب راننده</span>
                                @else
                                <span  class="btn-small btn-{{$item['status']==1?'success':'danger'}}">{{$item['status']==1?'انتخاب شده':'رد شده'}}</span>
                                @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/driver.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->

        </div>
    </div>




@endsection
@section('script')
    @parent
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function accept_request(element,driver_id,order_id) {
            $.ajax({
                type:'post',
                url:`/admin/driver_accept/`,
                data:{
                    driver_id:driver_id,
                    order_id:order_id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success:function(response) {
                    if(response.status==='success') {
                        console.log(response.data);
                        return;
                        if(Number(response.data.active)===1)
                            $(element).removeClass('btn-danger').addClass('btn-success').attr('data-active',1).text('فعال');
                        else
                            $(element).removeClass('btn-success').addClass('btn-danger').attr('data-active',0).text('غیر فعال');
                    }
                }
            });
        }
    </script>
@endsection
