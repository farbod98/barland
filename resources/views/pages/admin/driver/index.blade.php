@extends('templates.datatable')
@section('style')
    @parent
    <script src="{{asset('metronic-assets/select2/css/select2.min.css')}}"></script>
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;

        }
        .mz_hidden{display:none !important;}
        .mz-btn-badge{
            padding:2px 4px;
            margin:2px;
            font-size: 1rem;
            border:1px solid transparent;
        }
        .alert-primary{}

    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/driver.page_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--                        @include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="driver_new()" data-toggle="modal" data-target="#add_new_row" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>@lang(__('pages/driver.index.id'))</th>
                    <th>@lang(__('pages/driver.index.name'))</th>
                    <th>خودرو</th>
                    <th>@lang(__('pages/driver.index.mobile'))</th>
                    <th>@lang(__('pages/driver.index.national_code'))</th>
                    <th>@lang(__('pages/driver.index.smart_code'))</th>
                    <th>@lang(__('pages/driver.index.tels'))</th>
                    <th>@lang(__('pages/driver.index.status'))</th>
                    <th>@lang(__('pages/driver.index.edit'))</th>


                </tr>
                </thead>
                <tbody>
                @forelse ($drivers as $driver)
                    <tr id="tr_driver_{{$driver->id}}">
                        <td>{{$driver->id}}</td>
                        <td>{{$driver->user->name}}</td>
                        <td>
                            <span onclick="car_edit(this,'{{$driver->car->id}}');" class="btn-small btn-primary">{{$driver->car_type->title}}</span>
                        </td>
                        <td>{{$driver->user->mobile}}</td>
                        <td>{{$driver->national_id}}</td>
                        <td>{{$driver->smart_code}}</td>
{{--                        <td>{{str_replace(',',' - ',str_replace(['[',']','"'],'',$driver->tels))}}</td>--}}
                        <td>
                            @foreach(my_json_decode($driver->tels) as $tel)
                            <span class="mz-btn-badge alert-primary">{{$tel}}</span>
                            @endforeach
                        </td>
                        <td><span onclick="status_change(this);" class="btn-small {{$driver->active==0?'btn-danger':'btn-success'}}" data-id="{{$driver->id}}" data-active="{{$driver->active}}">{{$driver->active==0?'غیر فعال':'فعال'}}</span></td>
                        <td><span onclick="driver_edit(this,'{{$driver->id}}');" class="btn-small btn-primary">ویرایش</span></td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/driver.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <div class="modal fade" id="driver_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang(__('pages/driver.index.edit'))</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="driver_form" action="javascript:driver_update(this);">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="first_name" class="form-control-label">نام</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="last_name" class="form-control-label">نام خانوادگی</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="mobile" class="form-control-label">@lang(__('pages/driver.index.mobile'))</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="national_code" class="form-control-label">@lang(__('pages/driver.index.national_code'))</label>
                                    <input type="text" class="form-control" id="national_code" name="national_code">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="smart_code" class="form-control-label">@lang(__('pages/driver.index.smart_code'))</label>
                                    <input type="text" class="form-control" id="smart_code" name="smart_code">
                                </div>
                            </div>


                        </div>
                        <div class="row" id="mz_repeater">
                            <div class="col-6">
                                <div class="kt-margin-b-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="add">
                            <div class="col-6">
                                <div id="phone_message_box" class="text-danger mz_hidden">
                                    لطفا ابتدا مقادیر قبلی را پر نموده سپس افزودن جدید را انتخاب کنید
                                </div>
                            </div>
                            <div class="col-6">
                                <a href="javascript:;" onclick="mz_repeater()" class="btn btn btn-warning pull-left">
                                    <i class="la la-plus"></i>
                                    <span>افزودن شماره تلفن</span>
                                </a>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="car_type" class="form-control-label">@lang(__('pages/driver.index.car_type'))</label>
                                    <select class="form-control" id="car_type" name="car_id"></select>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" form="driver_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="car_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">خودرو</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="car_form" action="javascript:car_update(this);">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name" class="form-control-label">پلاک</label>
                                    <input type="text" class="form-control" id="pelak" name="name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name" class="form-control-label">کد هوشمند</label>
                                    <input type="text" class="form-control" id="car_smart_code" name="name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="car_type" class="form-control-label">ویژگی</label>
                                    <select class="form-control" id="car_feature" name="car_id"></select>
                                </div>
                            </div>

                            </div>

                       </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" form="car_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    @parent
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}"></script>
    <script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        let drivers=@json($drivers);
        function car_edit(element,id){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`/car/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {

                        $("#pelak").val(response.data.car.pelak);
                        $("#car_smart_code").val(response.data.car.smart_code);
                        let data='';
                        $.each(response.data.car_features,function(index,prepend){
                            data+=`<option value="${index}" ${index==response.data.car.car_feature_id?'selected':''}>${prepend}</option>`;
                        });
                        $('#car_feature').html('').append(data);
                        data='';
                        $("#car_form").attr('action',`javascript:car_update(this,${id});`);
                        $("#car_edit_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

        function car_update(element,id) {
            let form_id='#car_form';
            if(!validation(form_id))
                return;
            let pelak=$("#pelak").val();
            let smart_code=$("#car_smart_code").val();
            let car_feature_id=$("#car_feature").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`/car/${id}`,
                data:{
                    pelak:pelak,
                    smart_code:smart_code,
                    car_feature_id:car_feature_id,
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let car_type=response.data;
                    if(response.status==='success') {
                        // let data=`<td>${car_type.id}</td>
                        //         <td>${car_type.title}</td>
                        //         <td><span onclick="status_change(this);" class="btn-small ${car_type.active===0?'btn-danger':'btn-success'}" data-id="${car_type.id}" data-active="${car_type.active}">${car_type.active===0?'غیر فعال':'فعال'}</span></td>
                        //         <td><span onclick="car_type_edit(this,'${car_type.id}');" class="btn-small btn-primary">ویرایش</span></td>`;
                        // $("#tr_car_type_"+car_type.id).html('').append(data);
                        $("#car_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            })}
        function mz_repeater(){
            let inputs = $('#driver_form input[name="tels"]');
            let flag=false;
            $.each(inputs,function(key,input){
                if($(input).val()==='')
                    flag=true;
            });

            if(flag) {
                $("#phone_message_box").removeClass('mz_hidden');
                return;
            }
            else
                $("#phone_message_box").addClass('mz_hidden');

            let col = `<div class="col-6 deleteable">
                            <div class="kt-margin-b-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    <div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="la la-close"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>`;

            $("#mz_repeater").append(col);
        }


        function mz_remove_repeater_item(element){
            $(element).closest('.deleteable').remove();
        }

        function driver_address(element,id) {
            $.each(customers,function(key,drivers){
                if(Number(drivers.id)===Number(id)){
                    let data='';
                    $.each(driver.driver_address,function(index,address){
                        data+=`<p>${address.address}</p>`;
                    });
                    $('#driver_modal_body').html('').append(data);
                    $('#driver_address_modal').modal('show');
                }
            });

        }

        function status_change(element) {
            let id=$(element).attr('data-id');
            let active=$(element).attr('data-active');
            if(Number(active)===1)
                active=0;
            else
                active=1;
            $.ajax({
                type:'put',
                url:`driver/${id}`,
                data:{
                    active:active,
                    id:id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success:function(response) {
                    if(response.status==='success') {
                        if(Number(response.data.active)===1)
                            $(element).removeClass('btn-danger').addClass('btn-success').attr('data-active',1).text('فعال');
                        else
                            $(element).removeClass('btn-success').addClass('btn-danger').attr('data-active',0).text('غیر فعال');
                    }
                }
            });
        }





        function driver_edit(element,id){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`driver/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {
                        $("#first_name").val(response.data.user.first_name);
                        $("#last_name").val(response.data.user.last_name);
                        $("#mobile").val(response.data.user.mobile);
                        $("#tels").val(response.data.tels);
                        $("#national_code").val(response.data.national_id);
                        $("#smart_code").val(response.data.smart_code);
                        $("#driver_form").attr('action',`javascript:driver_update(this,${id});`);
                        let data='';
                        $.each(response.car_types,function(index,car_type){
                            data+=`<option value="${car_type.id}" ${car_type.id==response.data.car_type.id?'selected':''}>${car_type.title}</option>`;
                        });
                        $('#car_type').html('').append(data);
                        data='';
                        // $.each(response.prepends,function(index,prepend){
                        //     data+=`<option value="${index}" ${index===response.data.prepend?'selected':''}>${prepend}</option>`;
                        // });
                        // $('#prepend').html('').append(data);
                        data='';
                        let tels=JSON.parse(response.data.tels);
                        $("#mz_repeater").html('');
                        let del=`<div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="la la-close"></i>
                                        </a>
                                    </div>`;
                        $.each(tels,function(key,tel){
                            let data=`<div class="col-6 deleteable">
                            <div class="kt-margin-b-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels" value="${tel}" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    ${key===0? '':del}
                                </div>
                            </div>
                        </div>`;
                            $("#mz_repeater").append(data);

                        });



                        $("#driver_edit_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }



        function validation(element) {
            let form=$(element);
            let all_rules={rules:{
                    name:{required:true},
                    mobile:{
                        required:true,
                        digits:true,
                    },
                    national_code:{
                        required:true,
                        digits:true,
                    },
                    tels:{digits:true},
                }
            };

            form.validate(all_rules);
            if(!form.valid())
                return false;
            return true;
        }

        function driver_update(element,id) {
            let form_id='#driver_form';
            if(!validation(form_id))
                return;

            let first_name=$("#first_name").val();
            let last_name=$("#last_name").val();
            let mobile=$("#mobile").val();
            let smart_code=$("#smart_code").val();
            let national_id=$("#national_code").val();
            let car_id=$("#car_type").val();
            let phone = $('#driver_form input[name="tels"]');
            let tels=[];
            $.each(phone,function(key,input){
                tels.push(input.value);
            });

            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`driver/${id}`,
                data:{
                    first_name:first_name,
                    last_name:last_name,
                    mobile:mobile,
                    national_id:national_id,
                    smart_code:smart_code,
                    tels:tels,
                    car_type_id:car_id,
                    tels:JSON.stringify(tels)
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let driver=response.data;
                    let tels = JSON.parse(driver.tels);
                    let all_tels='';
                    $.each(tels,function(ket ,tel){
                        all_tels+=`<span class="mz-btn-badge alert-primary">${tel}</span>`;
                    });
                    //let tels=driver.tels.replace(/\[|\]|"/g,'').replace(/,/g,' - ');
                    if(response.status==='success') {
                        let data=`<td>${driver.id}</td>
                                <td>${driver.user.name}</td>
                                 <td>  <span onclick="car_edit(this,${driver.car.id});" class="btn-small btn-primary">${driver.car_type.title}</span></td>
                                <td>${driver.user.mobile}</td>
                                <td>${driver.national_id}</td>
                                <td>${driver.smart_code}</td>
                                <td>${all_tels}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${driver.active === 0 ? 'btn-danger' : 'btn-success'}" data-id="${driver.id}" data-active="${driver.active}">${driver.active === 0 ? 'غیر فعال' : 'فعال'}</span></td>
                                <td><span onclick="driver_edit(this,'${driver.id}');" class="btn-small btn-primary">ویرایش</span></td>`;
                        $("#tr_driver_"+driver.id).html('').append(data);
                        $("#driver_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });


        }


        function driver_new() {
            $("#name").val('');
            $("#mobile").val('');
            $("#national_code").val('');
            $("#smart_code").val('');
            $("#tels").val('');
            $.ajax({
                type:'get',
                url:`driver/get/details`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    let data='';
                    $.each(response.data,function(index,type){
                        data+=`<option value="${type.id}">${type.title}</option>`;
                    });
                    $("#car_type").html('').append(data);
                    data='';
                    // $.each(response.prepends,function(index,prepend){
                    //     data+=`<option value="${index}">${prepend}</option>`;
                    // });
                    // $("#prepend").html('').append(data);

                }
            });
            $("#mz_repeater").remove();
            let data=`<div class="row" id="mz_repeater">
                            <div class="col-6">
                                <div class="kt-margin-b-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    </div>
                                </div>
                            </div>
                        </div>`;
            $(data).insertBefore('#add');

            $("#driver_edit_modal").modal('show');
            $("#driver_form").attr('action',`javascript:driver_store();`);
        }

        function driver_store() {
            let form_id='#driver_form';
            if(!validation(form_id))
                return;

            let first_name=$("#first_name").val();
            let last_name=$("#last_name").val();
            let mobile=$("#mobile").val();
            let smart_code=$("#smart_code").val();
            let national_id=$("#national_code").val();
            let car_type_id=$("#car_type").val();
            let phone = $('#driver_form input[name="tels"]');
            let tels=[];
            $.each(phone,function(key,input){
                tels.push(input.value);
            });
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'post',
                url:`driver`,
                data:{
                    first_name:first_name,
                    last_name:last_name,
                    mobile:mobile,
                    national_id:national_id,
                    tels:JSON.stringify(tels),
                    smart_code:smart_code,
                    car_type_id:car_type_id
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let driver=response.data;
                    let tels = JSON.parse(driver.tels);
                    let all_tels='';
                    $.each(tels,function(ket ,tel){
                        all_tels+=`<span class="mz-btn-badge alert-primary">${tel}</span>`;
                    });

                    //let tels=driver.tels.replace(/\[|\]|"/g,'').replace(/,/g,' - ');
                    if(response.status==='success') {
                        let new_row=[
                            driver.id,
                            driver.user.name,
                            driver.car_type.title,
                            driver.user.mobile,
                            driver.national_id,
                            smart_code,
                            all_tels,
                            `<span onclick="status_change(this);" class="btn-small ${driver.active===0?'btn-danger':'btn-success'}" data-id="${driver.id}" data-active="${driver.active}">${driver.active===0?'غیر فعال':'فعال'}</span>`,
                            `<span onclick="driver_edit(this,'${driver.id}');" class="btn-small btn-primary">ویرایش</span>`
                        ];

                        let data=`<tr id="tr_customer_${driver.id}">
                                <td>${driver.id}</td>
                                <td>${driver.user.name}</td>
                                <td>${driver.car_type.title}</td>
                                <td>${driver.user.mobile}</td>
                                <td>${driver.national_id}</td>
                                <td>${driver.smart_code}</td>
                                <td>${tels}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${driver.active===0?'btn-danger':'btn-success'}" data-id="${driver.id}" data-active="${driver.active}">${driver.active===0?'غیر فعال':'فعال'}</span></td>
                                <td><span onclick="customer_edit(this,'${driver.id}');" class="btn-small btn-primary">ویرایش</span></td>
                                </tr>`;
                        // $("tbody").append(data);
                        let table=$("#kt_table_1").DataTable();
                        table.row.add(new_row).node().id=`tr_driver_${driver.id}`;
                        table.draw(false);
                        $("#driver_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

    </script>
@endsection
