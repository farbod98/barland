@extends('templates.datatable')
@section('style')
    @parent
    <script src="{{asset('metronic-assets/select2/css/select2.min.css')}}"></script>
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }
        .mz_hidden{display:none !important;}
    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/customer.page_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--                        @include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="customer_new();" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>عنوان</th>
                    <th>تناژ</th>
                    <th>وضعیت</th>
                    <th>ویرایش</th>

                </tr>
                </thead>
                <tbody>
                @forelse ($car_types as $car_type)
                    <tr id="tr_car_type_{{$car_type->id}}">
                        <td>{{$car_type->id}}</td>
                        <td>{{$car_type->title}}</td>
                        <td>{{$car_type->tonage}}</td>
                        <td><span onclick="status_change(this);" class="btn-small {{$car_type->active==0?'btn-danger':'btn-success'}}" data-id="{{$car_type->id}}" data-active="{{$car_type->active}}">{{$car_type->active==0?'غیر فعال':'فعال'}}</span></td>
                        <td>
                            <span onclick="car_type_edit(this,'{{$car_type->id}}');" class="btn-small btn-primary">ویرایش</span>
                            <a href="{{url('admin/car_feature',$car_type->id)}}" class="btn-small btn-danger">ویژگی ها</a>
                            @if(count($car_type->prices)>0)
                            <a href="{{url('admin/car_price',$car_type->id)}}" class="btn-small btn-success">کرایه ها </a>
                                @endif

                        </td>

                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/application.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <div class="modal fade" id="car_type_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ویرایش خودرو</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="car_type_form" action="javascript:car_type_update(this);">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="first_name" class="form-control-label">عنوان</label>
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="last_name" class="form-control-label">تناژ</label>
                                    <input type="text" class="form-control" id="tonage" name="tonage">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" id="submit_btn" form="car_type_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="customer_address_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">آدرس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body" id="customer_modal_body">

                </div>
                <div class="modal-footer">
                    <a href="customer_address/create">افزودن</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>






@endsection
@section('script')
    @parent
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}"></script>
    <script>
        function mz_repeater(){
            let inputs = $('#customer_form input[name="tels"]');
            let flag=false;
            $.each(inputs,function(key,input){
                if($(input).val()==='')
                    flag=true;
            });

            if(flag) {
                $("#phone_message_box").removeClass('mz_hidden');
                return;
            }
            else
                $("#phone_message_box").addClass('mz_hidden');

            let col = `<div class="col-6 deleteable">
                            <div class="kt-margin-b-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    <div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="la la-close"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>`;

            $("#mz_repeater").append(col);
        }

        function mz_remove_repeater_item(element){
            $(element).closest('.deleteable').remove();
        }

        {{--let customers=@json($customers);--}}
        function customer_address(element,id) {
            $.each(customers,function(key,customer){
                if(Number(customer.id)===Number(id)){
                    let data='';
                    $.each(customer.customer_address,function(index,address){
                        data+=`<a href="customer/address/${address.id}">${address.address}</a>`;
                    });
                    $('#customer_modal_body').html('').append(data);
                    $('#customer_address_modal').modal('show');
                }
            });

        }

        function status_change(element) {
            let id=$(element).attr('data-id');
            let active=$(element).attr('data-active');
            if(Number(active)===1)
                active=0;
            else
                active=1;
            $.ajax({
                type:'put',
                url:`car_type/${id}`,
                data:{
                    active:active,
                    id:id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response) {
                    console.log(response);
                    if(response.status==='success') {
                        if(Number(response.data.active)===1)
                            $(element).removeClass('btn-danger').addClass('btn-success').attr('data-active',1).text('فعال');
                        else
                            $(element).removeClass('btn-success').addClass('btn-danger').attr('data-active',0).text('غیر فعال');
                    }
                }
            });
        }

        function car_type_edit(element,id){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`car_type/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    console.log(response);
                    if(response.status==='success') {
                        $("#title").val(response.data.title);
                        $("#tonage").val(response.data.tonage);
                        let data='';
                        $("#submit_btn").html('به روز رسانی');



                        $("#car_type_form").attr('action',`javascript:car_type_update(this,${id});`);
                        $("#car_type_edit_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

        function validation(element) {
            let form=$(element);
            let all_rules={rules:{
                    name:{required:true},
                    mobile:{
                        required:true,
                        digits:true,
                    },
                    national_code:{
                        required:true,
                        digits:true,
                    },
                }
            };

            form.validate(all_rules);
            if(!form.valid())
                return false;
            return true;
        }

        function car_type_update(element,id) {
            let form_id='#car_type_form';
            if(!validation(form_id))
                return;

            let title=$("#title").val();
            let tonage=$("#tonage").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`car_type/${id}`,
                data:{
                    title:title,
                    tonage:tonage,
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let car_type=response.data;
                    if(response.status==='success') {
                        let data=`<td>${car_type.id}</td>
                                <td>${car_type.title}</td>
                                <td>${car_type.tonage}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${car_type.active===0?'btn-danger':'btn-success'}" data-id="${car_type.id}" data-active="${car_type.active}">${car_type.active===0?'غیر فعال':'فعال'}</span></td>
                                <td><span onclick="car_type_edit(this,'${car_type.id}');" class="btn-small btn-primary">ویرایش</span></td>`;
                        $("#tr_car_type_"+car_type.id).html('').append(data);
                        $("#car_type_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            })}

        function customer_new() {
            $("#title").val('');
            $("#tonage").val('');
            $("#submit_btn").html('ثبت');
            $("#car_type_edit_modal").modal('show');
            $("#car_type_form").attr('action',`javascript:car_type_store();`);
        }

        function car_type_store() {
            let form_id='#car_type_form';
            if(!validation(form_id))
                return;

            let title=$("#title").val();
            let tonage=$("#tonage").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'post',
                url:`car_type`,
                data:{
                    title:title,
                    tonage:tonage,
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let car_type=response.data;
                    if(response.status==='success') {
                        let new_row=[
                            car_type.id,
                            car_type.title,
                            car_type.tonage,
                            `<span onclick="status_change(this);" class="btn-small ${car_type.active===0?'btn-danger':'btn-success'}" data-id="${car_type.id}" data-active="${car_type.active}">${car_type.active===0?'غیر فعال':'فعال'}</span>`,
                            `<span onclick="car_type_edit(this,'${car_type.id}');" class="btn-small btn-primary">ویرایش</span>`
                        ];
                        {{--let data=`<tr id="tr_customer_${customer.id}">--}}
                        {{--        <td>${customer.id}</td>--}}
                        {{--        <td>${customer.name}</td>--}}
                        {{--        <td>${customer.mobile}</td>--}}
                        {{--        <td>${customer.national_id}</td>--}}
                        {{--        <td>${tels}</td>--}}
                        {{--        <td><span onclick="status_change(this);" class="btn-small ${customer.active===0?'btn-danger':'btn-success'}" data-id="${customer.id}" data-active="${customer.active}">${customer.active===0?'غیر فعال':'فعال'}</span></td>--}}
                        {{--        <td><span onclick="customer_address(this,'${customer.id}');" class="btn-small btn-dark" data-id="${customer.id}">@lang(__('pages/customer.index.address'))</span></td>--}}
                        {{--        <td><span onclick="customer_edit(this,'${customer.id}');" class="btn-small btn-primary">ویرایش</span></td>--}}
                        {{--        </tr>`;--}}
                        let table=$("#kt_table_1").DataTable();
                        table.row.add(new_row).node().id=`tr_car_type_${car_type.id}`;
                        table.draw(false);
                        $("#car_type_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

    </script>
@endsection
