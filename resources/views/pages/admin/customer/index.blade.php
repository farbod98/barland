@extends('templates.datatable')
@section('style')
    @parent
    <script src="{{asset('metronic-assets/select2/css/select2.min.css')}}"></script>
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }
        .mz_hidden{display:none !important;}
    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/customer.page_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--@include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="customer_new();" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>@lang(__('pages/customer.index.id'))</th>
                    <th>@lang(__('pages/customer.index.name'))</th>
                    <th>@lang(__('pages/customer.index.mobile'))</th>
                    <th>@lang(__('pages/customer.index.national_code'))</th>
                    <th>@lang(__('pages/customer.index.tels'))</th>
                    <th>@lang(__('pages/customer.index.status'))</th>
                    <th>@lang(__('pages/customer.index.operation'))</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($customers as $customer)
                    <tr id="tr_customer_{{$customer->id}}">
                        <td>{{$customer->id}}</td>
                        <td>{{$customer->user->name}}</td>
                        <td>{{$customer->user->mobile}}</td>
                        <td>{{$customer->national_id}}</td>
                        <td>
                            @foreach(my_json_decode($customer->tels) as $tel)
                                <span class="mz-btn-badge alert-primary">{{$tel}}</span>
                            @endforeach
                        </td>
                        <td><span onclick="status_change(this);" class="btn-small {{$customer->active==0?'btn-danger':'btn-success'}}" data-id="{{$customer->id}}" data-active="{{$customer->active}}">{{$customer->active==0?'غیر فعال':'فعال'}}</span></td>
                        <td>
                            <span onclick="customer_edit(this,'{{$customer->id}}');" class="btn-small btn-primary">ویرایش</span>
                            <a href="{{asset('/admin/customer/manage_address/'.$customer->id)}}" class="btn-small btn-success"> مدیریت آدرس ها</a>
                        </td>
                        {{--<td><div class="color-box" style="background: {{$application->color}}"></div></td>--}}
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/customer.index.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <div class="modal fade" id="customer_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang(__('pages/customer.index.edit'))</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="customer_form" action="javascript:customer_update(this);">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="prepend" class="form-control-label">@lang(__('pages/customer.index.prepend'))</label>
                                    <select name="prepend" id="prepend" class="form-control"></select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="first_name" class="form-control-label">@lang(__('pages/customer.index.first_name'))</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="last_name" class="form-control-label">@lang(__('pages/customer.index.last_name'))</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="mobile" class="form-control-label">@lang(__('pages/customer.index.mobile'))</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="national_code" class="form-control-label">@lang(__('pages/customer.index.national_code'))</label>
                                    <input type="text" class="form-control" id="national_code" name="national_code">
                                </div>
                            </div>
                        </div>
                        <div class="row" id="mz_repeater">
                            <div class="col-6">
                                <div class="kt-margin-b-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="add">
                            <div class="col-6">
                                <div id="phone_message_box" class="text-danger mz_hidden">
                                    لطفا ابتدا مقادیر قبلی را پر نموده سپس افزودن جدید را انتخاب کنید
                                </div>
                            </div>
                            <div class="col-6">
                                <a href="javascript:;" onclick="mz_repeater()" class="btn btn btn-warning pull-left">
                                    <i class="la la-plus"></i>
                                    <span>افزودن شماره تلفن</span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" id="submit_btn" form="customer_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="customer_address_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">آدرس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body" id="customer_modal_body">

                </div>
                <div class="modal-footer">
                    <a href="customer_address/create">افزودن</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}"></script>
    <script>
        function mz_repeater(){
            let inputs = $('#customer_form input[name="tels"]');
            let flag=false;
            $.each(inputs,function(key,input){
                if($(input).val()==='')
                    flag=true;
            });

            if(flag) {
                $("#phone_message_box").removeClass('mz_hidden');
                return;
            }
            else
                $("#phone_message_box").addClass('mz_hidden');

            let col = `<div class="col-6 deleteable">
                            <div class="kt-margin-b-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    <div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="la la-close"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>`;

            $("#mz_repeater").append(col);
        }

        function mz_remove_repeater_item(element){
            $(element).closest('.deleteable').remove();
        }

        let customers=@json($customers);
        function customer_address(element,id) {
            $.each(customers,function(key,customer){
                if(Number(customer.id)===Number(id)){
                    let data='';
                    $.each(customer.customer_address,function(index,address){
                        data+=`<a href="customer/address/${address.id}">${address.address}</a>`;
                    });
                    $('#customer_modal_body').html('').append(data);
                    $('#customer_address_modal').modal('show');
                }
            });

        }

        function status_change(element) {
            let id=$(element).attr('data-id');
            let active=$(element).attr('data-active');
            if(Number(active)===1)
                active=0;
            else
                active=1;
            $.ajax({
                type:'put',
                url:`customer/${id}`,
                data:{
                  active:active,
                  id:id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success:function(response) {
                    if(response.status==='success') {
                        if(Number(response.data.active)===1)
                            $(element).removeClass('btn-danger').addClass('btn-success').attr('data-active',1).text('فعال');
                        else
                            $(element).removeClass('btn-success').addClass('btn-danger').attr('data-active',0).text('غیر فعال');
                    }
                }
            });
        }

        function customer_edit(element,id){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`customer/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {
                        $("#name").val(response.data.name);
                        $("#mobile").val(response.data.mobile);
                        $("#national_code").val(response.data.national_id);
                        $("#prepend").html('');
                        let data='';
                        $.each(response.prepends,function(index,prepend){
                            data+=`<option value="${index}" ${index===Number(response.data.prepend)?'selected':''}>${prepend}</option>`;
                        });
                        $("#submit_btn").html('به روز رسانی');


                        $("#prepend").append(data);



                        let tels=JSON.parse(response.data.tels);
                        $("#mz_repeater").html('');
                        let del=`<div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="la la-close"></i>
                                        </a>
                                    </div>`;

                        $.each(tels,function(key,tel){
                            let data=`<div class="col-6 deleteable">
                            <div class="kt-margin-b-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels" value="${tel}" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    ${key===0? '':del}
                                </div>
                            </div>
                        </div>`;
                            $("#mz_repeater").append(data);
                        });

                        $("#customer_form").attr('action',`javascript:customer_update(this,${id});`);
                        $("#customer_edit_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

        function validation(element) {
            let form=$(element);
            let all_rules={rules:{
                    name:{required:true},
                    mobile:{
                        required:true,
                        digits:true,
                    },
                    national_code:{
                        required:true,
                        digits:true,
                    },
                }
            };

            form.validate(all_rules);
            if(!form.valid())
                return false;
            return true;
        }

        function customer_update(element,id) {
            let form_id='#customer_form';
            if(!validation(form_id))
                return;

            let first_name=$("#first_name").val();
            let last_name=$("#last_name").val();
            let mobile=$("#mobile").val();
            let national_id=$("#national_code").val();
            let phone = $('#customer_form input[name="tels"]');
            let prepend=$("#prepend").val();
            let tels=[];
            $.each(phone,function(key,input){
                tels.push(input.value);
            });
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`customer/${id}`,
                data:{
                    first_name:first_name,
                    last_name:last_name,
                    mobile:mobile,
                    national_id:national_id,
                    tels:JSON.stringify(tels),
                    prepend:prepend
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let customer=response.data;

                    let tels=customer.tels.replace(/\[|\]|"/g,'').replace(/,/g,' - ');
                    if(response.status==='success') {
                        let data=`<td>${customer.id}</td>
                                <td>${customer.name}</td>
                                <td>${customer.mobile}</td>
                                <td>${customer.national_id}</td>
                                <td>${tels}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${customer.active===0?'btn-danger':'btn-success'}" data-id="${customer.id}" data-active="${customer.active}">${customer.active===0?'غیر فعال':'فعال'}</span></td>
                                <td><span onclick="customer_address(this,'${customer.id}');" class="btn-small btn-dark" data-id="${customer.id}">@lang(__('pages/customer.index.address'))</span></td>
                                <td><span onclick="customer_edit(this,'${customer.id}');" class="btn-small btn-primary">ویرایش</span></td>`;
                        $("#tr_customer_"+customer.id).html('').append(data);
                        $("#customer_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            })}

        function customer_new() {
            $("#first_name").val('');
            $("#last_name").val('');
            $("#mobile").val('');
            $("#national_code").val('');
            $("#mz_repeater").remove();
            $("#prepend").html('');
            $("#submit_btn").html('ثبت');
            let data=`<div class="row" id="mz_repeater">
                            <div class="col-6">
                                <div class="kt-margin-b-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    </div>
                                </div>
                            </div>
                        </div>`;
            $(data).insertBefore('#add');
            data='';
            $.ajax({
                type:'get',
                url:`customer/prepends/get`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    $.each(response.prepends,function(index,prepend){
                        data+=`<option value="${index}">${prepend}</option>`;
                    });
                    $("#prepend").append(data);
                }
            });

            $("#customer_edit_modal").modal('show');
            $("#customer_form").attr('action',`javascript:customer_store();`);
        }

        function customer_store() {
            let form_id='#customer_form';
            if(!validation(form_id))
                return;

            let fisrt_name=$("#first_name").val();
            let last_name=$("#last_name").val();
            let mobile=$("#mobile").val();
            let phone = $('#customer_form input[name="tels"]');
            let prepend=$("#prepend").val();
            let tels=[];
            $.each(phone,function(key,input){
                tels.push(input.value);
            });
            let national_id=$("#national_code").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'post',
                url:`customer`,
                data:{
                    first_name:first_name,
                    last_name:last_name,
                    mobile:mobile,
                    national_id:national_id,
                    tels:tels,
                    prepend:prepend
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let customer=response.data;
                    let tels=customer.tels.replace(/\[|\]|"/g,'').replace(/,/g,' - ');
                    if(response.status==='success') {
                        let new_row=[
                          customer.id,
                          customer.name,
                            customer.mobile,
                            customer.national_id,
                            tels,
                            `<span onclick="status_change(this);" class="btn-small ${customer.active===0?'btn-danger':'btn-success'}" data-id="${customer.id}" data-active="${customer.active}">${customer.active===0?'غیر فعال':'فعال'}</span>`,
                            `<span onclick="customer_address(this,'${customer.id}');" class="btn-small btn-dark" data-id="${customer.id}">@lang(__('pages/customer.index.address'))</span>`,
                            `<span onclick="customer_edit(this,'${customer.id}');" class="btn-small btn-primary">ویرایش</span>`
                        ];
                        let data=`<tr id="tr_customer_${customer.id}">
                                <td>${customer.id}</td>
                                <td>${customer.name}</td>
                                <td>${customer.mobile}</td>
                                <td>${customer.national_id}</td>
                                <td>${tels}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${customer.active===0?'btn-danger':'btn-success'}" data-id="${customer.id}" data-active="${customer.active}">${customer.active===0?'غیر فعال':'فعال'}</span></td>
                                <td><span onclick="customer_address(this,'${customer.id}');" class="btn-small btn-dark" data-id="${customer.id}">@lang(__('pages/customer.index.address'))</span></td>
                                <td><span onclick="customer_edit(this,'${customer.id}');" class="btn-small btn-primary">ویرایش</span></td>
                                </tr>`;
                        let table=$("#kt_table_1").DataTable();
                        table.row.add(new_row).node().id=`tr_customer_${customer.id}`;
                        table.draw(false);
                        // $("tbody").append(data);
                        $("#customer_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

    </script>
@endsection
