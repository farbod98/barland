@extends('templates.datatable')
@section('style')
    @parent
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('site/map/css/mapbox.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.Default.css')}}" rel="stylesheet" />
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }
        .mz_hidden{display:none !important;}
        .table-link-icon{font-size: 18px;display: inline-block;background: #e3e3e3;padding: 5px 8px 0px;border-radius: 5px;}

        .address_detail_box{}
        .address_detail_box .item{margin-bottom: 10px;padding: 5px 15px;}
        .address_detail_box .item:hover{background-color: #eee;}
        .address_detail_box span{}
        .address_detail_box .title{color:#0b57d5;margin-left: 15px;display: inline-block;width: 75px;}
        .address_detail_box .title:after{content: ':'}
        .address_detail_box .description{color: #333;display: inline;line-height: 35px;}
        .leaflet-touch .leaflet-control-attribution, .leaflet-touch .leaflet-control-layers, .leaflet-touch .leaflet-bar {display: none;}
        .select2{width: 100% !important;}
        .select2-container--default .select2-selection--single .select2-selection__rendered {line-height: 20px !important;}
        .select2-container .select2-selection--single {height: 38px !important;}
        .select2-container--default .select2-selection--single {border: 1px solid #ced4da !important;}
        .select2-container--default .select2-selection--single .select2-selection__arrow b {top: 67% !important;}
        .select2-container--default .select2-selection--single .select2-selection__arrow {top: 18px;}
        .modal.show .modal-dialog.mz-modal-width{
            margin-top: 100px;
        }
    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/customer.manage_address.sender_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--@include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="get_manage_address('sender');" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>@lang(__('pages/customer.manage_address.id'))</th>
                    <th>@lang(__('pages/customer.manage_address.title'))</th>
                    <th>@lang(__('pages/customer.manage_address.sender_province'))</th>
                    <th>@lang(__('pages/customer.manage_address.sender_city'))</th>
                    <th>@lang(__('pages/customer.index.operation'))</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($items['sender'] as $item)
                    <tr id="tr_sender_{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->city->province->title}}</td>
                        <td>{{$item->city->title}}</td>
                        <td>
                            <a href="javascript:;" class="table-link-icon" title="جزئیات" onclick="get_details_of_address('sender','{{$item->id}}');"><i class="fas fa-eye"></i></a>
                            <a href="javascript:;" class="table-link-icon" title="ویرایش" onclick="get_manage_address('sender','{{$item->id}}');"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/customer.index.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/customer.manage_address.receiver_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--@include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="get_manage_address('receiver');" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>@lang(__('pages/customer.manage_address.id'))</th>
                    <th>@lang(__('pages/customer.manage_address.name'))</th>
                    <th>@lang(__('pages/customer.manage_address.receiver_province'))</th>
                    <th>@lang(__('pages/customer.manage_address.receiver_city'))</th>
                    <th>@lang(__('pages/customer.index.operation'))</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($items['receiver'] as $item)
                    <tr id="tr_receiver_{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->city->province->title}}</td>
                        <td>{{$item->city->title}}</td>
                        <td>
                            <a href="javascript:;" class="table-link-icon" title="جزئیات" onclick="get_details_of_address('receiver','{{$item->id}}');"><i class="fas fa-eye"></i></a>
                            <a href="javascript:;" class="table-link-icon" title="ویرایش" onclick="get_manage_address('receiver','{{$item->id}}');"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/customer.index.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('script')
    @parent
    @include('site.pages.blog-sections.global_modal')
    <script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
    <script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('site/map/js/mapbox.js')}}"></script>
    <script src="{{asset('site/map/js/leaflet.markercluster.js')}}"></script>
    @include('pages.manage_address_script')
    <script>
        function get_details_of_address(type,id){
            mz_global_async_ajax('{{asset('order/get/address')}}','post',{id:id,type:type}).then( (response) => {
                swal.close();
                if(response.status==='success'){
                    let address = response.data;
                    let receiver_data='';
                    if(type==='receiver') {
                        let phones='';
                        let tels=JSON.parse(address.tels);
                        $.each(tels, function (key, tel) {
                            phones += `${tel} ${key+1 === tels.length ?'':' ، '}`;
                        });
                        receiver_data = `
                        <div class="col-12 item"><span class="title">تلفن همراه</span><span class="description">${address.mobile != null ? address.mobile : ''}</span></div>
                        <div class="col-12 item"><span class="title">تلفن</span><span class="description">${phones}</span></div>`;
                    }
                    let content=`<div class="row address_detail_box">
                                <div class="col-12 item"><span class="title">استان </span><span class="description">${address.city.province.title}</span></div>
                                <div class="col-12 item"><span class="title">شهر </span><span class="description">${address.city.title}</span></div>
                                <div class="col-12 item"><span class="title">آدرس </span><span class="description">${address.address}</span></div>
                                <div class="col-12 item"><span class="title">پلاک </span><span class="description">${address.pelak != null ? address.pelak :''}</span></div>
                                <div class="col-12 item"><span class="title">واحد </span><span class="description">${address.vahed != null ? address.vahed :''}</span></div>
                                ${receiver_data}
                            </div>`;
                    global_modal_label.html('').append(` نمایش جزئیات آدرس: ${type==='sender'?address.title:address.name} `);
                    //modal_header.css('display','none');
                    global_modal_dialog.addClass('modal-lg').removeClass('mz-modal-width');
                    global_modal_form.html('').append(content);
                    global_modal_footer.html('').append('<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>');
                    global_modal_id.modal('show');
                }
            });
        }
    </script>
@endsection
