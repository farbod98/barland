@extends('templates.datatable')
@section('style')
    @parent
    <style>
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }

        .mz_hidden{display:none !important;}
        .mz-btn-badge{
            padding:2px 4px;
            margin:2px;
            font-size: 1rem;
            border:1px solid transparent;
        }
        .alert-primary{}
    </style>
@endsection
@section('datatable')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @lang(__('pages/order.page_title'))
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--                        @include('pages.tools.export')--}}
                        <a href="javascript:;" onclick="new_order();" data-toggle="modal" data-target="#add_new_row" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            @lang(__('general.new_record'))
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>@lang(__('pages/order.index.id'))</th>
                    <th>@lang(__('pages/order.index.sender'))</th>
                    <th>@lang(__('pages/order.index.receiver'))</th>
                    <th>@lang(__('pages/order.index.product'))</th>
                    <th>@lang(__('pages/order.index.tonage'))</th>
                    <th>@lang(__('pages/order.index.tarefe'))</th>
                    <th>@lang(__('pages/order.index.driver'))</th>
                    <th>@lang(__('pages/order.index.price'))</th>
                    {{--<th>@lang(__('pages/order.index.status'))</th>--}}
                    <th>@lang(__('pages/order.index.status'))</th>
                    <th>@lang(__('pages/order.index.operation'))</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($orders as $order)
                    <tr id="tr_order_{{$order->id}}">
                        <td>{{$order->id}}</td>
                        <td>{{$order->customer->title}}</td>
                        <td>
                            {{$order->receiver->name}}
                        </td>
                        <td>{{$order->product->title}}</td>
                        <td>{{$order->tonage}}</td>
                        <td>{{$order->tarefe}}</td>
                        <td>{{$order->driver->user->name}}</td>
                        <td>{{isset($order->price)?$order->price:'نامعلوم'}}</td>
                        <td><span onclick="status_change(this);" class="btn-small {{$order->active==0?'btn-danger':'btn-success'}}" data-id="{{$order->id}}" data-active="{{$order->active}}">{{$order->active==0?'غیر فعال':'فعال'}}</span></td>
                        <td>
                            <span onclick="order_edit(this,'{{$order->id}}');" class="btn-small btn-primary">ویرایش</span>

                            <span class="btn btn-small btn-danger" onclick="request_edit(this,'{{$order->id}}' );"> {{count($order->driver_requests)}}  </span>

                        </td>
                    </tr>
                @empty
                    <tr>
                        <td nowrap colspan="5" class="text-center">
                            <span class="kt-font-bold kt-font-danger">@lang(__('pages/order.not_found'))</span>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <div class="modal fade" id="order_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang(__('pages/order.index.edit'))</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="order_form" action="javascript:order_update(this);">
                        <div class="form-group">
                            <label for="product" class="form-control-label">@lang(__('pages/order.index.product'))</label>
                            <select name="product" id="product" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="tonage" class="form-control-label">@lang(__('pages/order.index.tonage'))</label>
                            <input type="text" class="form-control" id="tonage" name="tonage">
                        </div>
                        <div class="form-group">
                            <label for="tarefe" class="form-control-label">@lang(__('pages/order.index.tarefe'))</label>
                            <input type="text" class="form-control" id="tarefe" name="tarefe">
                        </div>
                        <div class="form-group">
                            <label for="price" class="form-control-label">@lang(__('pages/order.index.price'))</label>
                            <input type="text" class="form-control" id="price" name="price">
                        </div>
                        <div class="form-group">
                            <label for="driver" class="form-control-label">@lang(__('pages/order.index.driver'))</label>
                            <select name="driver" id="driver" class="form-control"></select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" form="order_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="order_address_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">آدرس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body" id="order_modal_body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="receiver_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang(__('pages/order.index.edit'))</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="receiver_form" action="javascript:receiver_update(this);">
                        <div class="form-group">
                            <label for="prepend" class="form-control-label">@lang(__('pages/receiver.index.prepend'))</label>
                            <select name="prepend" id="prepend" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="name" class="form-control-label">@lang(__('pages/receiver.index.name'))</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="form-control-label">@lang(__('pages/receiver.index.mobile'))</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                        <div class="form-group">
                            <label for="lat" class="form-control-label">@lang(__('pages/receiver.index.lat'))</label>
                            <input type="text" class="form-control" id="lat" name="lat">
                        </div>
                        <div class="form-group">
                            <label for="long" class="form-control-label">@lang(__('pages/receiver.index.long'))</label>
                            <input type="text" class="form-control" id="long" name="long">
                        </div>
                        <div class="form-group">
                            <label for="address" class="form-control-label">@lang(__('pages/receiver.index.address'))</label>
                            <input name="address" id="address" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" form="receiver_form" class="btn btn-primary">به روز رسانی</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="new_order_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang(__('pages/order.index.new'))</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="new_order_form" action="javascript:order_store();" >
                        <div class="form-group" id="customer_div">
                            <label for="customer" class="form-control-label">@lang(__('pages/order.index.customer'))</label>
                            <select name="customer" id="customer_new" class="form-control" onchange="get_customer_address(this);"></select>
                        </div>
                        <div class="form-group" id="address_div">
                            <label for="customer_address" class="form-control-label">@lang(__('pages/order.index.address'))</label>
                            <select name="customer_address" id="customer_address" class="form-control" ></select>
                        </div>
                        <div class="form-group">
                            <label for="receiver" class="form-control-label">@lang(__('pages/order.index.receiver'))</label>
                            <span class="btn btn-success" id="add_receiver" onclick="new_receiver();">گیرنده جدید</span>
                        </div>
                        <div class="form-group">
                            <label for="product" class="form-control-label">@lang(__('pages/order.index.product'))</label>
                            <select name="product" id="product_new" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="driver" class="form-control-label">@lang(__('pages/order.index.driver'))</label>
                            <select name="driver" id="driver_new" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="tonage" class="form-control-label">@lang(__('pages/order.index.tonage'))</label>
                            <input type="text" class="form-control" id="tonage_new" name="tonage">
                        </div>
                        <div class="form-group">
                            <label for="tarefe" class="form-control-label">@lang(__('pages/order.index.tarefe'))</label>
                            <input type="text" class="form-control" id="tarefe_new" name="tarefe">
                        </div>
                        <div class="form-group">
                            <label for="price" class="form-control-label">@lang(__('pages/order.index.price'))</label>
                            <input type="text" class="form-control" id="price_new" name="price">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" form="new_order_form" class="btn btn-primary">ثبت</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="new_receiver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang(__('pages/order.index.new'))</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="new_receiver_form" action="javascript:receiver_store(this);">


                        <div class="modal-body">
                            <form id="new_receiver_form" action="javascript:receiver_store(this);">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="receiver" class="form-control-label">@lang(__('pages/receiver.index.prepend'))</label>
                                            <select class="form-control" type="text" id="prepend_new" name="prepend"></select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label">@lang(__('pages/receiver.index.name'))</label>
                                            <input type="text" class="form-control" id="name_new" name="name">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="mobile" class="form-control-label">@lang(__('pages/receiver.index.mobile'))</label>
                                            <input type="text" class="form-control" id="mobile_new" name="mobile">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="city" class="form-control-label">@lang(__('pages/receiver.index.city'))</label>
                                            <select type="text" class="form-control" id="city_new" name="city"></select>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="lat" class="form-control-label">@lang(__('pages/receiver.index.lat'))</label>
                                            <input type="text" class="form-control" id="lat_new" name="lat">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="long" class="form-control-label">@lang(__('pages/receiver.index.long'))</label>
                                            <input type="text" class="form-control" id="long_new" name="long">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="address" class="form-control-label">@lang(__('pages/driver.index.address'))</label>
                                            <input type="text" class="form-control" id="address_new" name="address">
                                        </div>
                                    </div>

                                </div>
                                <div class="row" id="mz_repeater">
                                    <div class="col-6">
                                        <div class="kt-margin-b-10">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                                </div>
                                                <input type="text" name="tels_new" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="add">
                                    <div class="col-6">
                                        <div id="phone_message_box" class="text-danger mz_hidden">
                                            لطفا ابتدا مقادیر قبلی را پر نموده سپس افزودن جدید را انتخاب کنید
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:;" onclick="mz_repeater()" class="btn btn btn-warning pull-left">
                                            <i class="la la-plus"></i>
                                            <span>افزودن شماره تلفن</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>



                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button type="submit" form="new_receiver_form" class="btn btn-primary">ثبت</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let orders=@json($orders);


        function mz_repeater(){
            let inputs = $('#new_receiver_form input[name="tels_new"]');
            let flag=false;
            $.each(inputs,function(key,input){
                if($(input).val()==='')
                    flag=true;
            });

            if(flag) {
                $("#phone_message_box").removeClass('mz_hidden');
                return;
            }
            else
                $("#phone_message_box").addClass('mz_hidden');

            let col = `<div class="col-6 deleteable">
                            <div class="kt-margin-b-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels_new" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    <div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="la la-close"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>`;

            $("#mz_repeater").append(col);
        }

        function mz_remove_repeater_item(element){
            $(element).closest('.deleteable').remove();
        }

        function order_address(element,id) {
            $.each(customers,function(key,orders){
                if(Number(orders.id)===Number(id)){
                    let data='';
                    $.each(order.order_address,function(index,address){
                        data+=`<p>${address.address}</p>`;
                    });
                    $('#order_modal_body').html('').append(data);
                    $('#order_address_modal').modal('show');
                }
            });

        }

        function status_change(element) {
            let id=$(element).attr('data-id');
            let active=$(element).attr('data-active');
            if(Number(active)===1)
                active=0;
            else
                active=1;
            $.ajax({
                type:'put',
                url:`order/${id}`,
                data:{
                    active:active,
                    id:id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success:function(response) {
                    if(response.status==='success') {
                        if(Number(response.data.active)===1)
                            $(element).removeClass('btn-danger').addClass('btn-success').attr('data-active',1).text('فعال');
                        else
                            $(element).removeClass('btn-success').addClass('btn-danger').attr('data-active',0).text('غیر فعال');
                    }
                }
            });
        }

        function order_edit(element,id){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`order/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {
                        let data='';
                        $("#product").val(response.data.product.content);
                        $("#price").val(response.data.price);
                        $("#tarefe").val(response.data.tarefe);
                        $("#tonage").val(response.data.tonage);
                        $.each(response.products,function(index,product){
                            data+=`<option value="${product.id}" ${product.id===response.data.product_id?'selected':''}>${product.content}</option>`;
                        });
                        $('#product').html('').append(data);
                        data='';
                        $.each(response.drivers,function(index,driver){
                            data+=`<option value="${driver.id}" ${driver.id===response.data.driver_id?'selected':''}>${driver.name}</option>`;
                        });
                        $('#driver').html('').append(data);

                        $("#order_form").attr('action',`javascript:order_update(this,${id});`);
                        $("#order_edit_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

        function receiver_edit(element,id,order){
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'get',
                url:`receiver/${id}/edit`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {
                        $("#name").val(response.data.name);
                        $("#mobile").val(response.data.mobile);
                        $("#lat").val(response.data.lat);
                        $("#long").val(response.data.long);
                        $("#address").val(response.data.address);
                        let data='';
                        $.each(response.prepends,function (index,prepend) {
                            data+=`<option value="${index}" ${index==response.data.prepend?'selected':''}>${prepend}</option>`;
                        });
                        $("#prepend").html('').append(data);
                        $("#receiver_form").attr('action',`javascript:receiver_update(this,${id},${order});`);
                        $("#receiver_edit").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });
        }

        function order_validation(element) {
            let form=$(element);
            let all_rules={rules:{
                    tonage:{
                        required:true,
                        number:true
                    },
                    tarefe:{
                        required:true,
                        number:true
                    },
                    price:{
                        required:true,
                        number:true
                    },
                }
            };

            form.validate(all_rules);
            if(!form.valid())
                return false;
            return true;
        }

        function receiver_validation(element) {
            let form=$(element);
            let all_rules={rules:{
                    name:{required:true,},
                    mobile:{
                        required:true,
                        digits:true
                    },
                    lat:{
                        required:true,
                        number:true,
                    },
                    long:{
                        number:true,
                        required:true,
                    },
                    tels:{
                        digits:true
                    },
                    address:{
                        required:true,
                    }
                }
            };

            form.validate(all_rules);
            if(!form.valid())
                return false;
            return true;
        }

        function order_update(element,id) {
            let form_id='#order_form';
            if(!validation(form_id))
                return;

            let product=$("#product").val();
            let tonage=$("#tonage").val();
            let tarefe=$("#tarefe").val();
            let price=$("#price").val();
            let driver=$("#driver").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`order/${id}`,
                data:{
                    product_id:product,
                    tonage:tonage,
                    tarefe:tarefe,
                    price:Number(price),
                    driver_id:driver
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let order=response.data;
                    if(response.status==='success') {
                        price=order.price?order.price:'نامعلوم';
                        driver=order.driver?order.driver.name:'نامعلوم';
                        let data=`<td>${order.id}</td>
                                <td>${order.customer.name}</td>
                                <td>
                                ${order.receiver.name}
                                </td>
                                <td>${order.product.content}</td>
                                <td>${order.tonage}</td>
                                <td>${order.tarefe}</td>
                                <td>${driver}</td>
                                <td>${price}</td>
                                <td><span onclick="status_change(this);" class="btn-small ${order.active === 0 ? 'btn-danger' : 'btn-success'}" data-id="${order.id}" data-active="${order.active}">${order.active === 0 ? 'غیر فعال' : 'فعال'}</span></td>
                                <td>
                                <span onclick="order_edit(this,'${order.id}');" class="btn-small btn-primary">ویرایش</span>
                                <span class="btn btn-small btn-outline-info" onclick="receiver_edit(this,'${order.receiver.id}','${order.id}' );"> ویرایش گیرنده</span>
                                </td>`;
                        $("#tr_order_"+order.id).html('').append(data);
                        $("#order_edit_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });


        }

        function receiver_update(element,id,order) {

            let form_id='#order_form';
            if(!receiver_validation(form_id))
                return;

            let prepend=$("#prepend").val();
            let name=$("#name").val();
            let mobile=$("#mobile").val();
            let lat=$("#lat").val();
            let long=$("#long").val();
            let address=$("#address").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'put',
                url:`receiver/${id}`,
                data:{
                    prepend:prepend,
                    name:name,
                    mobile:mobile,
                    lat:lat,
                    long:long,
                    address:address
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let receiver=response.data;
                    if(response.status==='success') {
                        data=`${response.data.name}`;
                        // data=`<td>علی<span onclick="receiver_edit(this,1,1);">edit</span></td>`;
                        $("#tr_order_"+order+" > td:nth-child(3)").html('').append(data);
                        $("#receiver_edit").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });


        }

        function new_receiver_update(id) {


                let form_id='#order_form';
                if(!receiver_validation(form_id))
                    return;

                let prepend=$("#prepend_new").val();
                let name=$("#name_new").val();
                let mobile=$("#mobile_new").val();
                let lat=$("#lat_new").val();
                let long=$("#long_new").val();
                let address=$("#address_new").val();
                let phone = $('#new_receiver_form input[name="tels_new"]');
                let tels = [];
                $.each(phone, function (key, input) {
                    tels.push(input.value);
                });


                swal.fire({
                    title:'در حال دریافت اطلاعات',
                    text:'لطفا منتظر بمانید',
                    allowOutsideClick:false,
                    onOpen:function () {
                        swal.showLoading();
                    }
                });
                $.ajax({
                    type:'put',
                    url:`receiver/${id}`,
                    data:{
                        prepend:prepend,
                        name:name,
                        mobile:mobile,
                        lat:lat,
                        long:long,
                        address:address,
                        tels:JSON.stringify(tels)
                    },
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response) {
                        swal.close();
                        let receiver=response.data;
                        if(response.status==='success') {
                            $("#new_receiver_modal").modal('hide');
                            $( "#receiver_created" ).replaceWith(`<div id="receiver_created"><p id="new_receiver_id" data-id="${receiver.id}">${receiver.name}</p><span onclick="edit_new_receiver(${receiver.id});" class="btn btn-small btn-outline-primary">ویرایش</span></div>`);
                            $("#new_order_modal").modal('show');

                        }
                    },
                    error:function(){
                        swal.close();
                    }
                });




        }

        function new_order() {
            $("#customer_address").html('');
            $("#tonage_new").val('');
            $("#tarefe_new").val('');
            $("#price_new").val('');
            $( "#receiver_created" ).replaceWith(`<span class="btn btn-success" id="add_receiver" onclick="new_receiver();">گیرنده جدید</span>`);


            $("#new_order_modal").modal('show');
            $.ajax({
                type:'get',
                url:`order/all/get`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    let data='';
                    $.each(response.drivers,function(index,driver){
                        data+=`<option value="${driver.id}">${driver.name}</option>`;
                    });
                    $("#new_order_modal #driver_new").html('').append(data);

                    data='';
                    $.each(response.customers,function(index,customer){
                        data+=`<option value="${customer.id}">${customer.name}</option>`;
                    });
                    $("#new_order_modal #customer_new").html('').append(data);
                    data='';
                    $.each(response.products,function(index,product){
                        data+=`<option value="${product.id}">${product.content}</option>`;
                    });
                    $("#new_order_modal #product_new").html('').append(data);
                }
            });



        }

        function new_receiver() {
            $("#new_order_modal").modal('hide');
            $("#name_new").val('');
            $("#mobile_new").val('');
            $("#city_new").val('');
            $("#lat_new").val('');
            $("#long_new").val('');
            $("#address_new").val('');
            $("#mz_repeater").remove();
            $( "#new_receiver_form" ).attr('action','javascript:receiver_store(this)');


            let data=`<div class="row" id="mz_repeater">
                            <div class="col-6">
                                <div class="kt-margin-b-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="tels_new" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    </div>
                                </div>
                            </div>
                        </div>`;
            $(data).insertBefore('#add');



            $.ajax({
                type:'get',
                url:`receiver/get/details`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {
                        let data='';
                        $.each(response.prepends,function (index,prepend) {
                            data+=`<option value="${index}">${prepend}</option>`;
                        });
                        $("#prepend_new").html('').append(data);
                        data='';
                        $.each(response.cities,function (index,city) {
                            data+=`<option value="${index+1}">${city.slug}</option>`;
                        });
                        $("#city_new").html('').append(data);
                        $("#new_receiver_modal").modal('show');
                    }
                },
                error:function(){
                    swal.close();
                }
            });






        }

        function receiver_store(element) {


            let form_id = '#new_receiver_form';
            if (!receiver_validation(form_id))
                return;

            let prepend = $("#prepend_new").val();
            let name = $("#name_new").val();
            let mobile = $("#mobile_new").val();
            let lat = $("#lat_new").val();
            let long = $("#long_new").val();
            let address = $("#address_new").val();
            let city_id = $("#city_new").val();
            let phone = $('#new_receiver_form input[name="tels_new"]');
            let tels = [];
            $.each(phone, function (key, input) {
                tels.push(input.value);
            });
            swal.fire({
                title: 'در حال دریافت اطلاعات',
                text: 'لطفا منتظر بمانید',
                allowOutsideClick: false,
                onOpen: function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type: 'post',
                url: `receiver/`,
                data: {
                    prepend: prepend,
                    name: name,
                    mobile: mobile,
                    lat: lat,
                    long: long,
                    address: address,
                    tels: JSON.stringify(tels),
                    city_id: city_id
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {
                    swal.close();
                    let receiver = response.data;
                    if (response.status === 'success') {
                        $( "#add_receiver" ).replaceWith(`<div id="receiver_created"><p id="new_receiver_id" data-id="${receiver.id}">${receiver.name}</p><span onclick="edit_new_receiver(${receiver.id});" class="btn btn-small btn-outline-primary">ویرایش</span></div>`);
                        $("#new_receiver_modal").modal('hide');
                        $("#new_order_modal").modal('show');
                    }
                },
                error: function () {
                    swal.close();
                }
            });
        }

        function order_store() {

            let form_id='#new_order_form';
            if(!order_validation(form_id))
                return;
            let receiver_id=$("#new_receiver_id").attr('data-id');
            let product=$("#product_new").val();
            let tonage=$("#tonage_new").val();
            let tarefe=$("#tarefe_new").val();
            let price=$("#price_new").val();
            let driver=$("#driver_new").val();
            let address=$("#customer_address").val();
            swal.fire({
                title:'در حال دریافت اطلاعات',
                text:'لطفا منتظر بمانید',
                allowOutsideClick:false,
                onOpen:function () {
                    swal.showLoading();
                }
            });
            $.ajax({
                type:'post',
                url:`order`,
                data:{
                    product_id:product,
                    tonage:tonage,
                    tarefe:tarefe,
                    price:Number(price),
                    driver_id:driver,
                    receiver_id:receiver_id,
                    customer_address_id:address
                },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    let order=response.data;
                    if(response.status==='success') {
                        let order=response.data;
                        price=order.price?order.price:'نامعلوم';
                        driver=order.driver?order.driver.name:'نامعلوم';
                        let new_row=[
                            order.id,
                            order.customer.name,
                            order.receiver.name,
                            order.product.content,
                            order.tonage,
                            order.tarefe,
                            driver,
                            price,
                            `<span onclick="status_change(this);" class="btn-small ${order.active===0?'btn-danger':'btn-success'}" data-id="${order.id}" data-active="${order.active}">${order.active===0?'غیر فعال':'فعال'}</span>`,
                            `<span onclick="order_edit(this,'${order.id}');" class="btn-small btn-primary">ویرایش</span>`+`<span class="btn btn-small btn-outline-info" onclick="receiver_edit(this,'${order.receiver.id}','${order.id}' );"> ویرایش گیرنده</span>`
                        ];
                        let table=$("#kt_table_1").DataTable();
                        table.row.add(new_row).node().id=`tr_order_${order.id}`;
                        table.draw(false);
                        $("#new_order_modal").modal('hide');
                    }
                },
                error:function(){
                    swal.close();
                }
            });


        }

        function get_customer_address(element) {
            $("#customer_address").html('');
            let id=$(element).val();



            $.ajax({
                type:'get',
                url:`customer/${id}/address`,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response) {
                    swal.close();
                    if(response.status==='success') {
                        let address=response.data;
                        let data='';
                        $.each(address,function (index,addr) {
                            data+=`<option value="${addr.id}">${addr.address}</option>`
                        });
                        $("#customer_address").html('').append(data);
                    }
                },
                error:function(){
                    swal.close();
                }
            });






        }

        function edit_new_receiver(id) {
            $("#new_order_modal").modal('hide');
            $("#new_receiver_modal").modal('show');
            $("#new_receiver_form").attr('action',`javascript:new_receiver_update(${id});`);

        }


    </script>
@endsection
