<link rel="stylesheet" href="{{asset('site/css/fontawesome.css')}}">
<link rel="stylesheet" href="{{asset('site/css/slick.css')}}">
<link rel="stylesheet" href="{{asset('site/css/slick-theme.css')}}">
<link rel="stylesheet" href="{{asset('site/css/bootstrap-rtl.min.css')}}">
<link rel="stylesheet" href="{{asset('site/css/style.css')}}">
<link rel="stylesheet" href="{{asset('site/css/responsive.css')}}">
<link rel="stylesheet" href="{{asset('metronic-assets/sweetalert2/css/sweetalert2.min.rtl.css')}}"/>
<link rel="stylesheet" href="{{asset('metronic-assets/toastr/css/toastr.min.rtl.css')}}"/>
