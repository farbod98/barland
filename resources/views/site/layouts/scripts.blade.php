<script src="{{asset('site/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{asset('site/js/slick.js')}}"></script>
<script src="{{asset('metronic-assets/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/toastr/js/toastr.min.js')}}" type="text/javascript"></script>
<script>
    function openNav() {
        document.getElementById("sidebar-nav-toggle").style.width = "300px";
        document.getElementById("nav-overlay").style.display = "block";
        document.getElementById("nav-overlay").style.height = document.body.offsetHeight+'px';
    }

    function closeNav() {
        let overlay = document.getElementById("nav-overlay");
        document.getElementById("sidebar-nav-toggle").style.width = "0";
        // overlay.style.width = overlay.style.width+300;
        setTimeout(function(){ $(overlay).fadeOut() }, 250);
        //overlay.fadeOut()();
    }

    function scrollToTop(){
        $("html, body").animate({scrollTop: 0}, 1000);
    }

    async function mz_global_async_ajax(url,type,data){
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });
        data['token']="{{csrf_token()}}";
        let result;
        try {
            result = await $.ajax({
                url: url,
                type: type,
                data: data,
                headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
                dataType: 'json',
            });
            if(result.status==='error' || result.status=='error'){
                //let status = 'error';
                let myTitle = 'عملیات ناموفق';
                //ToastrMessage(myTitle, result.message, status);
                swal.fire({
                    title: myTitle,
                    text: result.message,
                    type: "error",
                    confirmButtonText: 'بسیار خب',
                    confirmButtonClass: "btn btn-brand"
                });
            }
            return result;
        } catch (error) {
            swal.close();
            let message='';
            if(error.responseJSON) {
                $.each(error.responseJSON.errors, function (key, error) {
                    message = error;
                });
            }
            let status = 'error';
            let myTitle = 'عملیات ناموفق';
            ToastrMessage(myTitle, message, status);
            return error;
            //console.error(error);
        }
    }

    function dropdown_menu_item(element){
        $(element).find('i.has-sub-menu').toggleClass('fa-chevron-left').toggleClass('fa-chevron-down');
        $(element).parent().find('> ul').toggleClass('active');
    }
</script>
