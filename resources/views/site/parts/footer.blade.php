<section class="all-sections driver-page-footer">
    <footer>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 all-columns first-child">
                <div class="item-head">
                    <h3>دسترسی سریع</h3>
                </div>
                <div class="item-content">
                    <ul>
                        <li><a href="#">سوالات متداول</a></li>
                        <li><a href="#">تماس با ما</a></li>
                        <li><a href="#">خبرنامه</a></li>
                        <li><a href="#">درباره ما</a></li>
                        <li><a href="#">فرصت های شغلی</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 all-columns contract">
                <div class="item-head">
                    <h3>اطلاعات تماس</h3>
                </div>
                <div class="item-content">
                    <ul>
                        <li>
                            <span>آدرس:</span>
                            <span class="text">استان تهران، خیابان ستارخان، خیابان پاتریس، کوچه سیامکی، خیابان ستارخان، خیابان پاتریس، کوچه سیامکی، پلاک 57</span>
                        </li>
                        <li>
                            <span>شماره تلفن:</span>
                            <a href="tel:09353322681">09353322681</a>
                        </li>
                        <li>
                            <span>ایمیل:</span>
                            <a href="mailto:support@barland.ir">support@barland.ir</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 all-columns newsletter">
                <div class="item-head">
                    <h3>عضویت در خبرنامه</h3>
                </div>
                <div class="item-content">
                    <form action="" id="news_letter" >
                        <input type="text" name="email" class="form-control" placeholder="ایمیل خود را وارد کنید">
                        <a href="javascript:;" onclick="submit(this);">ثبت</a>
                    </form>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 all-columns">
                <img src="{{asset('site/images/e-nemad.png')}}" alt="نماد الکترونیک" />
            </div>
        </div>
    </footer>
</section>

<section class="driver-page-copy-right">
    <div class="container">
        <p>تمام حقوق مادی و معنوی سایت مربوط به بارلند میباشد</p>
    </div>
</section>

<script>
    function submit() {
        let form_id = '#news_letter';
        if (!validation(form_id))
            return;
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال ثبت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });
        let form=$(form_id);

        let formData=new FormData(form[0]);
        let url ="{{asset('news_letter')}}";
        $.ajax(url, {
            data: formData,
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    swal.fire({
                        title: 'عملیات موفق',
                        text: response.message,
                        type: "success",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                    form.find('.form-control').val('');
                }
                if(response.status==='error') {
                    swal.fire({
                        title: 'عملیات ناموفق',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });


    }
    function validation(element) {
        let form=$(element);
        let all_rules={rules:{
                email:{
                    required:true,
                    email: true
                },
            }
        };

        form.validate(all_rules);
        if(!form.valid())
            return false;
        return true;
    }




</script>
