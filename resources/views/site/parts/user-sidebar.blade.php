<aside class="sidebar">

    <nav>
        <ul>
            <li class="{{request()->path()=='user'?'active':''}}"><a href="{{url('/user')}}"><i class="fas fa-user"></i> اطلاعات کاربری </a></li>
            @if(get_role()=='customer')
            <li><a href="javascript:;" onclick="dropdown_menu_item(this);"><i class="fas fa-truck"></i> مدیریت بارهای من <i class="fas fa-chevron-left has-sub-menu"></i></a>
                <ul class="dropdown_menu">
                    <li class="{{request()->path()=='user/bars/finished'?'active':''}}"><a href="{{route('user.finished_bars')}}"><i class="fas fa-circle"></i> تحویل داده شده </a></li>
                    <li class="{{request()->path()=='user/bars/in_way'?'active':''}}"><a href="{{route('user.in_way_bars')}}"><i class="fas fa-circle"></i> درحال انجام </a></li>
                    <li class="{{request()->path()=='user/bars/request'?'active':''}}"><a href="{{route('user.request_bars')}}"><i class="fas fa-circle"></i> در صف تایید </a></li>
                </ul>
            </li>
                <li class="{{request()->path()=='user/manage/address'?'active':''}}"><a href="{{url('user/manage/address')}}"><i class="fas fa-user"></i>  مدیریت آدرس ها </a></li>
            @endif
            @if(get_role()=='driver')
                <li><a href="javascript:;" onclick="dropdown_menu_item(this);"><i class="fas fa-truck"></i> سفرهای من <i class="fas fa-chevron-left has-sub-menu"></i></a>
                    <ul class="dropdown_menu">
                        <li class="{{request()->path()=='user/trips/old'?'active':''}}"><a href="{{route('user.old_trips')}}"><i class="fas fa-circle"></i> گذشته </a></li>
                        <li class="{{request()->path()=='user/trips/request'?'active':''}}"><a href="{{route('user.request_trips')}}"><i class="fas fa-circle"></i> در صف تایید </a></li>
                    </ul>
                </li>
            @endif
            <li class="{{request()->path()=='user/manage/password'?'active':''}}"><a href="{{route('user.change_password')}}"><i class="fas fa-unlock-alt"></i> تغییر رمز عبور </a></li>
            <li><a href="#"><i class="fas fa-envelope"></i> پیام ها </a></li>
            <li><a href="#"><i class="fas fa-gift"></i> پاداش ها </a></li>
        </ul>
    </nav>
</aside>
