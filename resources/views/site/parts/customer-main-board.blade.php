<section class="main-board customer">
    <div class="box-inputs">
        <form id="get_price_and_order_registration_form">
            <div class="form-group">
                <label for="form_order_from">مبدا را انتخاب کنید</label>
                <select name="form_order_from" id="form_order_from" class="form-control select2">
                    <option value=""></option>
                    @foreach(\App\Models\Province::where('accepted',1)->get() as $province)
                        @foreach($province->cities()->where('accepted',1)->get() as $city)
                            <option value="{{$city->id}}" >{{$city->title}} ({{$province->title}})</option>
                        @endforeach
                    @endforeach
                </select>
                <!--<small id="formHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
            </div>
            <div class="form-group">
                <label for="form_order_to">مقصد را انتخاب کنید</label>
                <select name="form_order_to" id="form_order_to" class="form-control select2">
                    <option value=""></option>
                    @foreach(\App\Models\City::all()  as $city)
                        <option value="{{$city->id}}" >{{$city->title}} ({{$city->province->title}})</option>
                    @endforeach
                </select>
                <!--<small id="toHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
            </div>

            <hr>

            <div class="form-group">
                <label>نوع خودرو را انتخاب کنید</label>
                <div class="box-inputs-toggle">
                    @foreach($car_types as $car_type)
                        {{--<input type="radio" onchange="get_car_feature(this);" {{$loop->index==0?'checked':''}} name="form_order_car_type" value="{{$car_type->id}}" id="car_type_{{$car_type->id}}" />--}}
                        <input type="radio" {{$loop->index==0?'checked':''}} name="form_order_car_type" value="{{$car_type->id}}" id="car_type_{{$car_type->id}}" />
                        <label for="car_type_{{$car_type->id}}">{{$car_type->title}}</label>
                    @endforeach

                </div>
            </div>

            <hr>

            <div class="register-new text-center">
                <a href="javascript:;" class="btn" onclick="get_price_and_order_registration();">
                    <span> استعلام قیمت و ثبت سفارش</span>
                    <i class="fas fa-long-arrow-alt-left"></i>
                </a>
            </div>
        </form>


        <div id="customer_order_registration" class="mz-hidden">
            <div class="text-center order_registration">
                <p>میانگین قیمت 5 سفر آخر در این مسیر</p>
                <h2><strong id="average_request">1,000</strong> تومان </h2>
            </div>
            <hr>
            <div class="text-center order_registration">
                <p>تعرفه باربری برای مسیر درخواستی شما</p>
                <h2><strong id="tariff_request">500</strong> تومان </h2>
            </div>
            <hr>
            <div class="register-newf text-center">
                <a href="javascript:;" class="btn" onclick="go_to_order_page_form();">
                    <span> ثبت سفارش</span>
                    <i class="fas fa-long-arrow-alt-left"></i>
                </a>
                <form action="{{Route('order.index.post')}}" method="post" id="go_to_order_page_form"></form>
            </div>
        </div>
    </div>
</section>
