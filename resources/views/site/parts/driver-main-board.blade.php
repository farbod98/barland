<section class="main-board driver">
    <div class="box-inputs">
        @auth
            <form action="">
                <div class="form-group">
                    <label for="filter_from_state">استان مبدا</label>
                    <select class="form-control select2" name="filter_from_state" id="filter_from_state" onchange="change_cities(this,'from');">
                        <option value=""></option>
                        @foreach(\App\Models\Province::get_accepted_provinces() as $province)
                            <option value="{{$province->id}}">{{$province->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="filter_from_city">شهر مبدا</label>
                    <select class="form-control select2" name="filter_from_city" id="filter_from_city">
                        <option value=""></option>
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <label for="filter_to_state">استان مقصد</label>
                    <select class="form-control select2" name="filter_to_state" id="filter_to_state" onchange="change_cities(this,'to');">
                        <option value=""></option>
                        @foreach(\App\Models\Province::get_provinces()->get() as $province)
                            <option value="{{$province->id}}">{{$province->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="filter_to_city">شهر مقصد</label>
                    <select class="form-control select2" name="filter_to_city" id="filter_to_city">
                        <option value=""></option>
                    </select>
                </div>

                <div class="register-new text-center">
                    <a href="javascript:;" onclick="get_bars();" class="btn">جستجوی بار</a>
                </div>
            </form>
        @endauth
        @guest
            <form action="{{asset('driver')}}" method="post" id="driver_form_id">
                <div class="form-group">
                    <label for="first_name">نام</label>
                    <input type="text" class="form-control" id="first_name" aria-describedby="formHelp" name="first_name">
                </div>
                <div class="form-group">
                    <label for="last_name">نام خانوادگی</label>
                    <input type="text" class="form-control" id="last_name" name="last_name">
                </div>
                <div class="form-group">
                    <label for="mobile">شماره همراه</label>
                    <input type="text" class="form-control" id="mobile" name="mobile">
                </div>

                <hr>

                <div class="form-group">
                    <label>نوع خودروی خود را انتخاب کنید</label>
                    <div class="box-inputs-toggle">
                        @foreach($car_types as $car_type)
                            <input type="radio" {{$loop->index==0?'checked':''}} onchange="get_car_feature(this);" name="car_type" value="{{$car_type->id}}" id="car_type_{{$car_type->id}}" />
                            <label for="car_type_{{$car_type->id}}">{{$car_type->title}}</label>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <label for="car_feature">ویژگی خودروی خود را انتخاب کنید</label>
                    <select class="form-control" id="car_feature" name="car_feature_id">
                        <option value=""></option>
                        @foreach($car_features as $type)
                            @if($type['car_type_id']==$car_types[0]['id'])
                                <option value="{{$type->id}}">{{$type->title}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <hr>
                <div class="form-group form-check">
                    <label class="form-check-label" for="accept-rules">
                        <input type="checkbox" class="form-check-input" id="accept-rules" name="accept_rules">
                        <a href="#" class="accept-rules">شرایط و قوانین </a>سایت را میپذیرم
                    </label>

                </div>

                <div class="register-new text-center">
                    <a href="javascript:;" onclick="store_new_driver();" class="btn">ثبت نام</a>
                </div>
            </form>
        @endguest
    </div>
</section>
