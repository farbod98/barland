<section class="all-sections driver-page-support">
    <div class="top-section container mb-5">
        <h2 class="title">
            نحوه پشتیبانی
        </h2>
        <p class="description">
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
        </p>
    </div>

    <div class="section-content">
        <div class="flex-content-box" id="driver_page_support">
            <div class="item active">
                <div class="item-title">
                    <a href="javascript:;" onclick="set_new_support_item(this);"><img src="{{asset('site/images/white-car.png')}}" alt="car" /></a>
                    <span class="active">مرحله اول</span>
                </div>
                <div class="item-content active">
                    <h3>پشتیبانی در مسیر</h3>
                    <p>1
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="item-title">
                    <a href="javascript:;" onclick="set_new_support_item(this);"><img src="{{asset('site/images/black-car.png')}}" alt="car" /></a>
                    <span>مرحله دوم</span>
                </div>
                <div class="item-content">
                    <h3>پشتیبانی در مسیر</h3>
                    <p>2
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="item-title">
                    <a href="javascript:;" onclick="set_new_support_item(this);"><img src="{{asset('site/images/black-car.png')}}" alt="car" /></a>
                    <span>مرحله سوم</span>
                </div>
                <div class="item-content">
                    <h3>پشتیبانی در مسیر</h3>
                    <p>3
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="item-title">
                    <a href="javascript:;" onclick="set_new_support_item(this);"><img src="{{asset('site/images/black-car.png')}}" alt="car" /></a>
                    <span>مرحله چهارم</span>
                </div>
                <div class="item-content">
                    <h3>پشتیبانی در مسیر</h3>
                    <p>4
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="item-title">
                    <a href="javascript:;" onclick="set_new_support_item(this);"><img src="{{asset('site/images/black-car.png')}}" alt="car" /></a>
                    <span>مرحله پنجم</span>
                </div>
                <div class="item-content">
                    <h3>پشتیبانی در مسیر</h3>
                    <p>5
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="item-title">
                    <a href="javascript:;" onclick="set_new_support_item(this);"><img src="{{asset('site/images/black-car.png')}}" alt="car" /></a>
                    <span>مرحله ششم</span>
                </div>
                <div class="item-content">
                    <h3>پشتیبانی در مسیر</h3>
                    <p>6
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                        درمسیر از شما پشتیبانی خواهیم کرد
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
