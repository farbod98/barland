
<div id="sidebar-nav-toggle" class="sidebar-nav-toggle">
    <div class="top-close-button">
        <a href="javascript:void(0)" onclick="closeNav()"><i class="fas fa-times"></i></a>
    </div>

    <ul class="menu-items">
        <li><a href="{{asset('/')}}" onclick="closeNav()">صفحه اصلی</a></li>
        <li><a href="{{asset('/login')}}">ورود/عضویت</a></li>
        <li><a href="{{asset('/about')}}" onclick="closeNav()">درباره ما</a></li>
        <li><a href="{{asset('/faq')}}" onclick="closeNav()">سوالات متداول</a></li>
        @if(get_role()=='customer')
            <li><a href="{{asset('/order')}}">درخواست بار جدید</a></li>
        @endif
        <li><a href="{{asset('/contact')}}" onclick="closeNav()">تماس با ما</a></li>
        <li><a href="{{asset('/news-latter')}}" onclick="closeNav()">خبرنامه</a></li>
        @if(get_role()=='customer')
            <li><a href="{{asset('/partners')}}" onclick="closeNav()">همکاران ما</a></li>
        @endif
    </ul>

    <div class="socials">
        <ul class="social-items">
            <li><a href="#" onclick="closeNav()"><i class="fab fa-twitter"></i></a></li>
            <li><a href="#" onclick="closeNav()"><i class="fab fa-instagram"></i></a></li>
            <li><a href="#" onclick="closeNav()"><i class="fab fa-linkedin-in"></i></a></li>
        </ul>
    </div>
</div>

<div id="nav-overlay" onclick="closeNav()">
    <div class="close-button">
        <a href="javascript:void(0)" onclick="closeNav()">بستن</a>
    </div>
</div>



