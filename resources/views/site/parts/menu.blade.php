<nav class="top-navigation {{top_navigation_class()}}">
    <ul class="menu hidden-xs">
        @auth
            <li><a href="javascript:;" onclick="dropdown_menu_item(this);">{{auth()->user()->full_name}} <i class="fas fa-chevron-left has-sub-menu"></i></a>
                <ul>
                    <li><a href="{{route('user.index')}}"><i class="fas fa-home"></i> پروفایل </a></li>
                    <li><a href="javascript:;" onclick="document.getElementById('logout_user_form').submit();"><i class="fas fa-sign-out-alt"></i> خروج </a>
                        <form action="{{route('logout')}}" method="post" id="logout_user_form">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
        @endauth
        @guest
            <li><a href="{{asset('/login/'.get_type_of_login())}}">{{get_type_of_login()=='driver'?'ورود':'ورود/عضویت'}}</a></li>
        @endguest
        <li><a href="{{asset('/')}}">صفحه اصلی</a></li>
        @if(get_role()=='driver')
            <li class="{{request()->path()=='bars'?'active':''}}"><a href="{{asset('/bars')}}"> بار ها</a></li>
        @endif
        @if(get_role()=='customer')
            <li class="{{request()->path()=='order'?'active':''}}"><a href="{{asset('/order')}}">درخواست بار جدید</a></li>
        @endif
        <li class="{{request()->path()=='about'?'active':''}}"><a href="{{asset('/about')}}">درباره ما</a></li>
        <li class="{{request()->path()=='faq'?'active':''}}"><a href="{{asset('/faq')}}">سوالات متداول</a></li>
        {{--@dd($page_type)--}}
        <li class="{{request()->path()=='contact'?'active':''}}"><a href="{{asset('/contact')}}">تماس با ما</a></li>
        <li class="{{request()->path()=='blog'?'active':''}}"><a href="{{asset('/blog')}}">وبلاگ</a></li>
    </ul>
    <div class="logo pull-left">
        <a href="{{asset('/')}}"><img src="{{asset('site/images/logo-white.png')}}" alt="logo" id="site-main-logo"></a>
    </div>

    <nav class="top-right menu navbar-light block-xs">
        <button class="navbar-toggler" type="button" onclick="openNav();">
            <span class="navbar-toggler-icon"></span>
            <span class="title">منو</span>
        </button>
    </nav>
</nav>
