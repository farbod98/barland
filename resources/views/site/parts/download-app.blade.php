<section class="all-sections driver-page-download-app">
    <div class="section-content">
        <div class="row">
            <div class="col-4 right-box">
                <div class="img"><img src="{{asset('site/images/download-app.png')}}" alt="download-app"/></div>
            </div>
            <div class="col-8 left-box text-center">
                <h3>در این بخش میتوانید اپلیکیشن بارلند را دانلود کنید</h3>
                <div class="images">

                    <img src="{{asset('site/images/app-google-play.png')}}" alt="work-car" />
                    <img src="{{asset('site/images/app-bazar.png')}}" alt="work-car" />
                </div>
                <div class="download-box">
                    <input type="text" placeholder="شماره تلفن خود را وارد کنید">
                    <a href="#">دریافت اپلیکیشن</a>
                </div>
            </div>
        </div>
    </div>
</section>
