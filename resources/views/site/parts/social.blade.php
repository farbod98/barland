<section class="driver-page-social">
    <div class="container">
        <div class="pull-right">
            <h6> مارا در شبکه های اجتماعی دنبال کنید</h6>
        </div>
        <div class="pull-left">
            <ul class="social-items">
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
    </div>
</section>
