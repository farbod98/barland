<section class="all-sections driver-page-testimonial">
    <div class="top-section container mb-5">
        <h2 class="title">
            نظرات کاربران
        </h2>
        <p class="description">
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
        </p>
    </div>
    <!--<div class="regular container testimonials">-->
    <div class="regular testimonials">
        <div class="item">
            <div class="content">
                <div class="item-image">
                    <img src="{{asset('site/images/majid.jpg')}}" alt="majid">
                </div>
                <div class="item-content">
                    <h3>مجید ذوالفقاری</h3>
                    <p>
                        خیلی خوب بود دستتون درد نکنه، دیگه خیلی راحت تر از قبل میتونم بار بگیرم و درد سرهای گذشته رو هم ندارم
                    </p>
                    <i class="fas fa-quote-left"></i>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="content">
                <div class="item-image">
                    <img src="{{asset('site/images/majid.jpg')}}" alt="majid">
                </div>
                <div class="item-content">
                    <h3>مجید ذوالفقاری</h3>
                    <p>
                        خیلی خوب بود دستتون درد نکنه، دیگه خیلی راحت تر از قبل میتونم بار بگیرم و درد سرهای گذشته رو هم ندارم
                    </p>
                    <i class="fas fa-quote-left"></i>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="content">
                <div class="item-image">
                    <img src="{{asset('site/images/majid.jpg')}}" alt="majid">
                </div>
                <div class="item-content">
                    <h3>مجید ذوالفقاری</h3>
                    <p>
                        خیلی خوب بود دستتون درد نکنه، دیگه خیلی راحت تر از قبل میتونم بار بگیرم و درد سرهای گذشته رو هم ندارم
                    </p>
                    <i class="fas fa-quote-left"></i>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="content">
                <div class="item-image">
                    <img src="{{asset('site/images/majid.jpg')}}" alt="majid">
                </div>
                <div class="item-content">
                    <h3>مجید ذوالفقاری</h3>
                    <p>
                        خیلی خوب بود دستتون درد نکنه، دیگه خیلی راحت تر از قبل میتونم بار بگیرم و درد سرهای گذشته رو هم ندارم
                    </p>
                    <i class="fas fa-quote-left"></i>
                </div>
            </div>
        </div>
    </div>
</section>
