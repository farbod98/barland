<section class="all-sections driver-page-work" id="driver-page-work">
    <div class="top-section container mb-5">
        <h2 class="title">
            نحوه کار با بارلند
        </h2>
        <p class="description">
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
        </p>
    </div>

    <div class="section-content">
        <div class="row">
            <div class="col-7 all-columns">
                <div class="steps step-1 active" id="driver-page-work-step-1">
                    <div class="counter"><span>1</span></div>
                    <div class="content">
                        در مرحله اول در سایت ما ثبت نام کرده و نام خود را وارد سایت ما
                        که در آن ثبت نام کرده اید وارد کنید که در سایت ما ثبت نام شوید
                    </div>
                </div>
                <div class="steps step-2" id="driver-page-work-step-2">
                    <div class="counter"><span>2</span></div>
                    <div class="content">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                    </div>
                </div>
                <div class="steps step-3" id="driver-page-work-step-3">
                    <div class="counter"><span>3</span></div>
                    <div class="content">
                        در مرحله سوم در سایت ما ثبت نام کرده و نام خود را وارد سایت ما
                        که در آن ثبت نام کرده اید وارد کنید که در سایت ما ثبت نام شوید
                        در مرحله سوم در سایت ما ثبت نام کرده و نام خود را وارد سایت ما
                        که در آن ثبت نام کرده اید وارد کنید که در سایت ما ثبت نام شوید
                    </div>
                </div>
            </div>
            <div class="col-5 all-columns">
                <img src="{{asset('site/images/work-car.png')}}" alt="work-car" class="pull-left"/>
            </div>
        </div>
    </div>
</section>
