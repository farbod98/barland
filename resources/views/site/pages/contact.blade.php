<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="about-page clear-pt-150">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="top-section container mb-5">
                    <h2 class="title">
                        تماس با بارلند
                    </h2>
                </div>
            </div>
        </div>

        <div class="row mt-5 mb-5 contact">
            <div class="col-md-4">
                <div class="item">
                    <div class="icon"><i class="fas fa-home"></i></div>
                    <div class="content">
                        <div class="heading">آدرس:</div>
                        <address>تهران، پاسداران، دشتستان دوم، پلاک ۱۴، واحد ۴</address>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="icon"><i class="fas fa-phone-square"></i></div>
                    <div class="content">
                        <div class="heading">تلفن:</div>
                        <a href="tel:02188202561">02188202561</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="icon"><i class="fas fa-envelope"></i></div>
                    <div class="content">
                        <div class="heading">ایمیل:</div>
                        <a href="mailto:support@barland.com">support@barland.com</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pt-5 mb-5">
            <div class="col-md-6 contact-form">
                <h3>با ما در تماس باشید</h3>
                <p>در صورت داشتن هرگونه سوال و یا انتقادی میتوانید از طریق فرم زیر با ما در میان بگذارید</p>
                <form action="" id="contact_form">
                    <input type="text" class="form-control" name="full_name" aria-label="نام" placeholder="نام و نام خانوادگی">
                    <input type="email" class="form-control" name="email" aria-label="ایمیل" placeholder="ایمیل">
                    <input type="text" class="form-control" name="subject" aria-label="موضوع" placeholder="موضوع">
                    <textarea class="form-control" name="content" cols="30" rows="5" aria-label="content" placeholder="متن پیام"></textarea>

                    <div class="all-button"><a href="javascript:;" onclick="contact_submit(this);" class="submit">ارسال پیام</a></div>
                </form>
            </div>
            <div class="col-md-6">
                <div class="img"><img src="{{asset('site/images/main-truck.png')}}" alt="main-truck"></div>
            </div>
        </div>
    </div>
</section>
{{--@include('site.parts.customer-main-board')--}}

{{--@include('site.parts.support')--}}

{{--@include('site.parts.work_with_us')--}}

{{--@include('site.parts.download-app')--}}

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')
<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script>
    function validation(element) {
        let form=$(element);
        let all_rules={rules:{
                full_name:{required:true},
                email:{
                    required:true,
                },
                subject:{
                    required:true,
                },
                content:{
                    required:true,
                },
            }
        };

        form.validate(all_rules);
        if(!form.valid())
            return false;
        return true;
    }

    function contact_submit(element){
        let form_id='#contact_form';
        if(!validation(form_id))
            return;


        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال ثبت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });
        let form=$(form_id);

        let formData=new FormData(form[0]);
        let url ="{{asset('contact')}}";
        $.ajax(url, {
            data: formData,
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    swal.fire({
                        title: 'عملیات موفق',
                        text: response.message,
                        type: "success",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                    form.find('.form-control').val('');
                }
                if(response.status==='error') {
                    swal.fire({
                        title: 'عملیات ناموفق',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });

    }

</script>
</body>
</html>
