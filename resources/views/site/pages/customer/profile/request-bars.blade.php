<table class="table table-bordered table-hover mz-multi-data-table">
    <thead>
    <tr>
        <th>شناسه</th>
        <th>عنوان بار</th>
        <th>مبداء</th>
        <th>مقصد</th>
        <th>تاریخ بارگیری</th>
        <th>کرایه</th>
        <th>جزئیات</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($items as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->product->title}}</td>
            <td><a href="javascript:;" title="جزئیات" onclick="get_details_of_address('sender','{{$item->customer_address_id}}');">{{$item->customer_address->title}}</a></td>
            <td><a href="javascript:;" title="جزئیات" onclick="get_details_of_address('receiver','{{$item->receiver_id}}');">{{$item->receiver->name}}</a></td>
            <td>{{$item->jalaliDate}}</td>
            <td>{{$item->price}}</td>
            <td><a href="javascript:;" onclick="show_detail_item_modal(this,'{{$item->id}}','request');"><i class="fas fa-chevron-left"></i></a></td>
        </tr>
    @empty
        <tr>
            <td colspan="7">سفارش در صف تایید وجود ندارد</td>
        </tr>
    @endforelse
    </tbody>
</table>
