<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
    <link href="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .address_detail_box{}
        .address_detail_box .item{margin-bottom: 10px;padding: 5px 15px;}
        .address_detail_box .item:hover{background-color: #eee;}
        .address_detail_box span{}
        .address_detail_box .title{color:#0b57d5;margin-left: 15px;display: inline-block;width: 75px;}
        .address_detail_box .title:after{content: ':'}
        .address_detail_box .description{color: #333;display: inline;line-height: 35px;}
    </style>
</head>
<body>

@include('site.parts.menu')

<section class="user_page">
    @include('site.parts.user-sidebar')
    <div class="main-content user_info">
        <div class="padding-around user-info-input">
            @include('site.pages.customer.profile.'.$page_type)
        </div>
    </div>
</section>

{{--@include('site.parts.social')--}}

{{--@include('site.parts.footer')--}}

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('site/js/price/mask.js')}}" type="text/javascript"></script>
<script src="{{asset('site/js/price/numeral.js')}}" type="text/javascript"></script>
<script src="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
@include('site.pages.blog-sections.global_modal')

{{--@include('site.pages.customer.profile.scripts')--}}

<script>

    $(document).ready(function(){
        dataTablesBasicScrollable();
        let li_s = $('.user_page > aside.sidebar nav li');
        $.each(li_s,function(key,li){
            if($(li).hasClass('active'))
                if($(li).parent().hasClass('dropdown_menu'))
                    $(li).parent().parent().find('> a')[0].click();
        });
    });

    function dataTablesBasicScrollable() {
        let data_tables = $(".mz-multi-data-table");
        $.each(data_tables,function(key,table){
            $(table).DataTable({
                scrollY: '50vh',
                scrollX: true,
                scrollCollapse: true,
            });
        });
    }

    function change_sortable_filter(element,filter){
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
    }

    function show_detail_item_modal(element,id,type){
        let url=`{{asset('order')}}/${id}/edit`;
        mz_global_async_ajax(url,'get',{id:id}).then( (response) => {
            swal.close();
            if(response.status==='success'){
                let order=response.data;
                let content='';
                if(type==='request')
                    content=`<div class="row">
                        <table class="table bar-detail-table">
                            <tr>
                                <td colspan="2"><span class="label">عنوان بار</span><span class="content">${order.product.title}</span></td>
                                <td></td>
                                <td colspan="2"><span class="label">صاحب بار</span><span class="content">${order.owner}</span></td>
                            </tr>
                            <tr>
                                <td colspan="2"><span class="label">مبداء</span><span class="content">${order.start_city.title}</span></td>
                                <td></td>
                                <td colspan="2"><span class="label">مقصد</span><span class="content">${order.dest_city.title}</span></td>
                            </tr>
                             <tr>
                                <td><span class="label">قیمت پیشنهادی</span><span class="content">${number_price_format(order.price)}</span></td>
                                <td></td>
                                <td><span class="label">تعرفه</span><span class="content">${number_price_format(order.tarefe)}</span></td>
                                <td></td>
                                <td><span class="label">میانگین 5 سفر آخر</span><span class="content">${number_price_format(order.avg)} تومان</span></td>
                            </tr>

                            <tr>
                                <td><span class="label">نوع خودرو</span><span class="content">${order.car_type}</span></td>
                                <td></td>
                                <td><span class="label">ویژگی خودرو</span><span class="content">${order.car_feature.title}</span></td>
                                <td></td>
                                <td><span class="label">نوع بسته بندی</span><span class="content">${order.packing.title}</span></td>
                            </tr>
                            <tr>
                                <td><span class="label">تاریخ بارگیری</span><span class="content">${order.jalaliDate}</span></td>
                                <td></td>
                                <td><span class="label">زمان شروع</span><span class="content">${order.time_from}</span></td>
                                <td></td>
                                <td><span class="label">زمان پایان</span><span class="content">${order.time_to}</span></td>
                            </tr>
                            <tr>
                                <td><span class="label">وزن بار</span><span class="content">${order.tonage} تن</span></td>
                                <td><span class="label">ارزش محموله</span><span class="content">${number_price_format(order.cargo)} تومان</span></td>
                                <td><span class="label">نرخ باسکول</span><span class="content">${number_price_format(order.bascule)} تومان</span></td>
                                <td><span class="label">نرخ بارگیری</span><span class="content">${number_price_format(order.lading)} تومان</span></td>
                                <td><span class="label">نرخ تخلیه</span><span class="content">${number_price_format(order.vacate)} تومان</span></td>
                            </tr>
                        </table>
                    </div>`;
                else
                    content=`<div class="row">
                        <table class="table bar-detail-table">
                            <tr>
                                <td colspan="2"><span class="label">عنوان بار</span><span class="content">${order.product.title}</span></td>
                                <td></td>
                                <td colspan="2"><span class="label">صاحب بار</span><span class="content">${order.owner}</span></td>
                            </tr>
                            <tr>
                                <td><span class="label">مبداء</span><span class="content">${order.start_city.title}</span></td>
                                <td></td>
                                <td><span class="label">مقصد</span><span class="content">${order.dest_city.title}</span></td>
                                <td></td>
                                <td><span class="label">قیمت ثبت شده</span><span class="content">${number_price_format(order.price)} تومان</span></td>
                            </tr>
                            <tr>
                                <td><span class="label">نوع خودرو</span><span class="content">${order.car_type}</span></td>
                                <td></td>
                                <td><span class="label">ویژگی خودرو</span><span class="content">${order.car_feature.title}</span></td>
                                <td></td>
                                <td><span class="label">نوع بسته بندی</span><span class="content">${order.packing.title}</span></td>
                            </tr>
                            <tr>
                                <td><span class="label">تاریخ بارگیری</span><span class="content">${order.jalaliDate}</span></td>
                                <td></td>
                                <td><span class="label">زمان شروع</span><span class="content">${order.time_from}</span></td>
                                <td></td>
                                <td><span class="label">زمان پایان</span><span class="content">${order.time_to}</span></td>
                            </tr>
                            <tr>
                                <td><span class="label">وزن بار</span><span class="content">${order.tonage} تن</span></td>
                                <td><span class="label">ارزش محموله</span><span class="content">${number_price_format(order.cargo)} تومان</span></td>
                                <td><span class="label">نرخ باسکول</span><span class="content">${number_price_format(order.bascule)} تومان</span></td>
                                <td><span class="label">نرخ بارگیری</span><span class="content">${number_price_format(order.lading)} تومان</span></td>
                                <td><span class="label">نرخ تخلیه</span><span class="content">${number_price_format(order.vacate)} تومان</span></td>
                            </tr>
                        </table>
                    </div>`;
                let footer_close_btn = `<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>`;
                //let footer_submit = `<button type="submit" form="global_base_page_modal_form" class="btn btn-primary">مشاهده جزئیات</button>`;
                global_modal_label.html('').append(`جزئیات بار`);
                global_modal_form.attr('action',`javascript:store_comment(${id});`);
                global_modal_footer.html('').append(`${footer_close_btn}`);
                // global_modal_header.css('display','none');
                global_modal_form.html('').append(content);
                global_modal_dialog.addClass('modal-lg');
                global_modal_id.modal('show');
            }
        });
    }

    function get_details_of_address(type,id){
        mz_global_async_ajax('{{asset('order/get/address')}}','post',{id:id,type:type}).then( (response) => {
            swal.close();
            if(response.status==='success'){
                let address = response.data;
                let receiver_data='';
                if(type==='receiver') {
                    let phones='';
                    let tels='';
                    $.each(tels, function (key, tel) {
                        phones += `${tel} ${key+1 === tels.length ?'':' ، '}`;
                    });
                    receiver_data = `
                        <div class="col-12 item"><span class="title">تلفن همراه</span><span class="description">${address.mobile != null ? address.mobile : ''}</span></div>
                        <div class="col-12 item"><span class="title">تلفن</span><span class="description">${phones}</span></div>`;
                }
                let content=`<div class="row address_detail_box">
                                <div class="col-12 item"><span class="title">استان </span><span class="description">${address.city.province.title}</span></div>
                                <div class="col-12 item"><span class="title">شهر </span><span class="description">${address.city.title}</span></div>
                                <div class="col-12 item"><span class="title">آدرس </span><span class="description">${address.address}</span></div>
                                <div class="col-12 item"><span class="title">پلاک </span><span class="description">${address.pelak != null ? address.pelak :''}</span></div>
                                <div class="col-12 item"><span class="title">واحد </span><span class="description">${address.vahed != null ? address.vahed :''}</span></div>
                                ${receiver_data}
                            </div>`;
                global_modal_label.html('').append(` نمایش جزئیات آدرس: ${type==='sender'?address.title:address.name} `);
                //modal_header.css('display','none');
                global_modal_dialog.addClass('modal-lg');
                global_modal_form.html('').append(content);
                global_modal_footer.html('').append('<button type="button" class="btn btn-secondary" data-dismiss="modal">تایید</button>');
                global_modal_id.modal('show');
            }
        });
    }

    function number_price_format(price){
        return numeral(price).format('0,0');
    }
</script>
</body>
</html>
