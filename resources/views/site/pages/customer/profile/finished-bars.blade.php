<table class="table table-bordered table-hover mz-multi-data-table">
    <thead>
    <tr>
        <th>ردیف</th>
        <th>عنوان بار</th>
        <th>صاحب بار</th>
        <th>مبداء</th>
        <th>مقصد</th>
        <th>تاریخ بارگیری</th>
        <th>تاریخ تحویل</th>
        <th>کرایه</th>
        <th>جزئیات</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($items as $item)
        <tr>
            <td>{{$loop->index+1}}</td>
            <td>{{$item->product->title}}</td>
            {{--                        <td>{{$item->customer->user->name}}</td>--}}
            <td><a href="javascript:;" title="جزئیات" onclick="get_details_of_address('sender','{{$item->customer_address_id}}');">{{$item->customer_address->title}}</a></td>
            <td><a href="javascript:;" title="جزئیات" onclick="get_details_of_address('receiver','{{$item->receiver_id}}');">{{$item->receiver->name}}</a></td>
            <td>{{$item->driver->name}}</td>
            <td>{{$item->driver->mobile}}</td>
            <td>{{$item->jalaliDate}}</td>
            {{--                        <td>1398-10-09</td>--}}
            <td>{{$item->price}}</td>
            <td><a href="javascript:;" onclick="show_detail_item_modal(this,'{{$item->id}}','finished');"><i class="fas fa-chevron-left"></i></a></td>
        </tr>
    @empty
        <tr>
            <td colspan="9">سفارش تحویل داده شده وجود ندارد</td>
        </tr>
    @endforelse
    </tbody>
</table>
