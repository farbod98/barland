<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('site/map/css/mapbox.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.Default.css')}}" rel="stylesheet" />
    <link href="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    <title>{{$page_title}}</title>
    <style>
        .address_detail_box{}
        .address_detail_box .item{margin-bottom: 10px;padding: 5px 15px;}
        .address_detail_box .item:hover{background-color: #eee;}
        .address_detail_box span{}
        .address_detail_box .title{color:#0b57d5;margin-left: 15px;display: inline-block;width: 75px;}
        .address_detail_box .title:after{content: ':'}
        .address_detail_box .description{color: #333;display: inline;line-height: 35px;}
        .leaflet-touch .leaflet-control-attribution, .leaflet-touch .leaflet-control-layers, .leaflet-touch .leaflet-bar {display: none;}
        .modal.show .modal-dialog.mz-modal-width{
            margin-top: 100px;
        }
    </style>
</head>
<body>

@include('site.parts.menu')

<section class="user_page">
    @include('site.parts.user-sidebar')
    <div class="main-content user_info">

        <div class="padding-around user-info-input">
            <div class="text-center list-title-item">
                لیست آدرس های مبداء
                <a href="javascript:;" class="btn btn-info pull-left" onclick="get_manage_address('sender');">افزودن آدرس جدید</a>
            </div>
            <table class="table table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>عنوان آدرس </th>
                    <th>استان مبداء</th>
                    <th>شهر مبداء</th>
                    <th>جزئیات</th>
                </tr>
                </thead>
                <tbody>
                @forelse($items['sender'] as $item)
                    <tr id="tr_sender_{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->city->province->title}}</td>
                        <td>{{$item->city->title}}</td>
                        <td class="multi-operand">
                            <a href="javascript:;" title="جزئیات" onclick="get_details_of_address('sender','{{$item->id}}');"><i class="fas fa-eye"></i></a>
                            <a href="javascript:;" title="ویرایش" onclick="get_manage_address('sender','{{$item->id}}');"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5"><div class="alert alert-danger">آدرسی یافت نشد</div></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="padding-around user-info-input mt-120">
            <div class="text-center list-title-item">
                لیست آدرس های مقصد
                <a href="javascript:;" class="btn btn-info pull-left" onclick="get_manage_address('receiver');">افزودن آدرس جدید</a>
            </div>
            <table class="table table-bordered table-hover mz-multi-data-table">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام گیرنده</th>
                    <th>استان مقصد</th>
                    <th>شهر مقصد</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @forelse($items['receiver'] as $item)
                    <tr id="tr_receiver_{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->city->province->title}}</td>
                        <td>{{$item->city->title}}</td>
                        <td class="multi-operand">
                            <a href="javascript:;" title="جزئیات" onclick="get_details_of_address('receiver','{{$item->id}}');"><i class="fas fa-eye"></i></a>
                            <a href="javascript:;" title="ویرایش" onclick="get_manage_address('receiver','{{$item->id}}');"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5"><div class="alert alert-danger">آدرسی یافت نشد</div></td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            {{--<nav aria-label="Page navigation" class="mt-5">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item active"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>--}}
        </div>
    </div>
</section>

{{--@include('site.parts.social')--}}

{{--@include('site.parts.footer')--}}

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('site/map/js/mapbox.js')}}"></script>
<script src="{{asset('site/map/js/leaflet.markercluster.js')}}"></script>
{{--<script src="{{asset('site/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

@include('site.pages.blog-sections.global_modal')
@include('pages.manage_address_script')
<script>
    $(document).ready(function(){
        dataTablesBasicScrollable();
    });

    function dataTablesBasicScrollable() {
        let data_tables = $(".mz-multi-data-table");
        $.each(data_tables,function(key,table){
            $(table).DataTable({
                scrollY: '50vh',
                scrollX: true,
                scrollCollapse: true,
            });
        });
    }

    $(document).ready(function(){
        let li_s = $('.user_page > aside.sidebar nav li');
        $.each(li_s,function(key,li){
            if($(li).hasClass('active'))
                if($(li).parent().hasClass('dropdown_menu'))
                    $(li).parent().parent().find('> a')[0].click();
        });
    });

    function change_sortable_filter(element,filter){
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
    }
</script>
</body>
</html>
