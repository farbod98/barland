<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <title>{{$page_title}}</title>

    {{--<link href='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' />--}}

    <link href="{{asset('site/map/css/mapbox.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.css')}}" rel="stylesheet" />
    <link href="{{asset('site/map/css/MarkerCluster.Default.css')}}" rel="stylesheet" />

    <link href="{{asset('metronic-assets/bootstrap/css/bootstrap-glyphicons.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('metronic-assets/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" rel="stylesheet" type="text/css"/>
    {{--<link href="{{asset('metronic-assets/bootstrap-timepicker/css/bootstrap-timepicker.min.rtl.css')}}" rel="stylesheet" type="text/css"/>--}}
    <style>
        /*Style Section*/
        .btn-small{
            font-size: 12px;padding: 4px 8px;border-radius: 5px;cursor: pointer;
        }
        .bootstrap-timepicker-widget table td input {width: 35px;}
        .bootstrap-timepicker-widget table {direction: ltr;}
        .mz_hidden{display:none !important;}
        .leaflet-touch .leaflet-control-attribution, .leaflet-touch .leaflet-control-layers, .leaflet-touch .leaflet-bar {display: none;}
        #new_bar_request_content_step_1{position: relative;}
        .modal.show .modal-dialog{margin-top: 150px;}
        .address_detail_box{}
        .address_detail_box .item{margin-bottom: 10px;padding: 5px 15px;}
        .address_detail_box .item:hover{background-color: #eee;}
        .address_detail_box span{}
        .address_detail_box .title{color:#0b57d5;margin-left: 15px;display: inline-block;width: 50px;}
        .address_detail_box .title:after{content: ':'}
        .address_detail_box .description{color: #333;display: inline;}
    </style>
</head>
<body>

@include('site.parts.menu')

<section class="new_bar_request">
    <div class="section-top">
        <div class="container">
            <div class="flex-content-box">
                <div id="new_bar_request_head_step_1" class="steps step-1 active">
                    <div class="counter"><span>1</span></div>
                    <div class="description">
                        مشخصات بار و خودروی درخواستی
                    </div>
                </div>
                <div id="new_bar_request_head_step_2" class="steps step-2">
                    <div class="counter"><span>2</span></div>
                    <div class="description">ثبت آدرس</div>
                </div>
                <div id="new_bar_request_head_step_3" class="steps step-3">
                    <div class="counter"><span>3</span></div>
                    <div class="description">بررسی نهایی</div>
                </div>
                <div id="new_bar_request_head_step_4" class="steps step-4">
                    <div class="counter"><span>4</span></div>
                    <div class="description">قیمت گذاری و ثبت سفارش</div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content">
        <form id="bar_request_form">
            <div id="new_bar_request_content_step_1" class="steps step-1 current">

                <div class="container">
                    <div class="title-section">لطفا مشخصات خودروی درخواستی خود را وارد کنید</div>

                    <div class="row">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>نوع خودرو را انتخاب کنید <span class="text-danger">*</span></label>
                                        <div class="box-inputs-toggle">
                                            @foreach($car_types as $car_type)
                                                <input type="radio" onchange="get_car_feature(this);" {{$data['car_type']==$car_type->id?'checked':''}} name="step_one_bar_car_type" value="{{$car_type->id}}" id="car_type_{{$car_type->id}}" />
                                                <label for="car_type_{{$car_type->id}}">{{$car_type->title}}</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_one_bar_car_details">ویژگی خودرو<span class="text-danger">*</span></label>
                                        <select class="form-control multi-select" id="step_one_bar_car_details" name="step_one_bar_car_details[]" aria-describedby="step_one_bar_car_details_help" multiple></select>
                                        <small id="step_one_bar_car_details_help" class="form-text text-muted">ویژگی خودروی مورد نظر را مشخص کنید</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container">
                    <div class="title-section">لطفا مشخصات بار خود را وارد کنید</div>
                    <div class="row">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_one_bar_title">عنوان بار <span class="text-danger">*</span></label>
                                        <select name="step_one_bar_title" id="step_one_bar_title" class="form-control select2">
                                            @foreach($products as $product)
                                                <option value="{{$product->id}}">{{$product->title}}</option>
                                            @endforeach
                                        </select>
                                        <small id="step_one_bar_title_help" class="form-text text-muted">نوع بار را مشخص کنید</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_one_bar_box_type">نوع بسته بندی <span class="text-danger">*</span></label>
                                        <select class="form-control" id="step_one_bar_box_type" name="step_one_bar_box_type" aria-describedby="step_one_bar_box_type_help">
                                            @foreach($packings as $packing)
                                                <option value="{{$packing->id}}">{{$packing->title}}</option>
                                            @endforeach
                                        </select>
                                        <small id="step_one_bar_box_type_help" class="form-text text-muted">در این بخش میتوانید نوع بسته بندی را مشخص کنید</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_one_bar_weight">وزن بار (برحسب تن)<span class="text-danger">*</span></label>
                                        <input type="number" onblur="change_step_one_bar_weight(this);" class="form-control" id="step_one_bar_weight" name="step_one_bar_weight" aria-describedby="step_one_bar_weight_help" placeholder="مثال:10 " min="0">
                                        <small id="step_one_bar_weight_help" class="form-text text-muted">در این بخش وزن بار را برحسب تناژ مشخص کنید</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-7"></div>
                    </div>
                </div>

                <hr>
                <div class="container">
                    <div class="title-section">اطلاعات تکمیلی بار</div>

                    <div class="row">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                       {{--<div class="col-6">
                                            <div class="form-group">
                                                <label for="step_one_bar_car_count">تعداد خودرو</label>
                                                <input type="number" class="form-control" id="step_one_bar_car_count" name="step_one_bar_car_count" aria-describedby="step_one_bar_car_count_help" placeholder="مثال:10 ">
                                                <small id="step_one_bar_car_count_help" class="form-text text-muted">تعداد خودروی مورد نظر را مشخص کنید</small>
                                            </div>
                                        </div>--}}
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_one_bar_cargo_value">ارزش محموله <span class="text-danger">*</span></label>
                                                <input type="text" onkeyup="number_price_format(this);" class="form-control" id="step_one_bar_cargo_value" name="step_one_bar_cargo_value" aria-describedby="step_one_bar_cargo_value_help" placeholder="مثال:10 ">
                                                <small id="step_one_bar_cargo_value_help" class="form-text text-muted"> ارزش محموله را برحسب تومان مشخص کنید</small>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_one_bar_bascule_price">نرخ باسکول</label>
                                                <input type="text" onkeyup="number_price_format(this);" class="form-control" id="step_one_bar_bascule_price" name="step_one_bar_bascule_price" aria-describedby="step_one_bar_bascule_price_help" placeholder="مثال:1000 " value="0">
                                                <small id="step_one_bar_bascule_price_help" class="form-text text-muted"> نرخ باسکول را برحسب تومان مشخص کنید</small>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_one_bar_lading_price">نرخ بارگیری</label>
                                                <input type="text" onkeyup="number_price_format(this);" class="form-control" id="step_one_bar_lading_price" name="step_one_bar_lading_price" aria-describedby="step_one_bar_lading_price_help" placeholder="مثال:1000 " value="0">
                                                <small id="step_one_bar_lading_price_help" class="form-text text-muted"> نرخ بارگیری را برحسب تومان مشخص کنید</small>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_one_bar_vacate_price">نرخ تخلیه</label>
                                                <input type="text" onkeyup="number_price_format(this);" class="form-control" id="step_one_bar_vacate_price" name="step_one_bar_vacate_price" aria-describedby="step_one_bar_vacate_price_help" placeholder="مثال:1000 " value="0">
                                                <small id="step_one_bar_vacate_price_help" class="form-text text-muted"> نرخ تخلیه را برحسب تومان مشخص کنید</small>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_one_bar_description">توضیحات بیشتر</label>
                                        <textarea class="form-control" id="step_one_bar_description" name="step_one_bar_description" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-7"></div>
                    </div>
                </div>
            </div>

            <div id="new_bar_request_content_step_2" class="steps step-2">
                <div class="container">
                    <div class="title-section">لطفا آدرس مبدا و مقصد بار را وارد کنید</div>

                    <div class="row">
                        <div class="col-5">
                            <div class="alert alert-warning">
                                <strong>توجه:</strong>
                                <span>
                                    در هر دو بخش محل بارگیری و محل تخلیه بار، میتوانید از بین آدرس هایی که قبلا وارد کرده اید یک آدرس را انتخاب کنید و یا از طریق دکمه ی ثبت آدرس جدید اقدام به ثبت آدرس جدید نمایید.
                                </span>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_two_bar_sender_place">محل بارگیری<span class="text-danger">*</span></label>
                                        <select class="form-control select2" onchange="get_details_of_address(this,'sender');" id="step_two_bar_sender_place" name="step_two_bar_sender_place" aria-describedby="step_two_bar_sender_place_help">
                                            <option value=""></option>
                                            @foreach($customer->address as $customer_address)
                                                <option value="{{$customer_address->id}}">{{$customer_address->title}}</option>
                                            @endforeach
                                        </select>
                                        <small id="step_two_bar_sender_place_help" class="form-text text-muted">شهر محل بارگیری را مشخص کنید</small>
                                    </div>
                                </div>
                            </div>

                            <div class="add_new_address">
                                <div class="add_new_address_inputs">

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="step_two_bar_sender_title">عنوان آدرس مبداء<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="step_two_bar_sender_title" name="step_two_bar_sender_title" aria-describedby="step_two_bar_sender_title_help" placeholder="">
                                                <small id="step_two_bar_sender_title_help" class="form-text text-muted">عنوان آدرس مبدا را مشخص کنید</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_sender_province">استان مبدا<span class="text-danger">*</span></label>
                                                        <select class="form-control select2" onchange="change_province(this,'sender');" id="step_two_bar_sender_province" name="step_two_bar_sender_province" aria-describedby="step_two_bar_sender_province_help">
                                                            <option value=""></option>
                                                            @foreach($provinces as $province)
                                                                <option value="{{$province->id}}">{{$province->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        <small id="step_two_bar_sender_province_help" class="form-text text-muted">استان مبدا را انتخاب کنید</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_sender_city">شهر مبدا<span class="text-danger">*</span></label>
                                                        <select class="form-control select2" onchange="change_city(this,'sender');" id="step_two_bar_sender_city" name="step_two_bar_sender_city" aria-describedby="step_two_bar_sender_city_help"></select>
                                                        <small id="step_two_bar_sender_city_help" class="form-text text-muted">شهر مبدا را انتخاب کنید</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="step_two_bar_sender_address">جزئیات آدرس مبداء<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="step_two_bar_sender_address" name="step_two_bar_sender_address" aria-describedby="step_two_bar_sender_address_help" placeholder="" readonly>
                                                <small id="step_two_bar_sender_address_help" class="form-text text-muted">آدرس مبدا را روی نقشه مشخص کنید</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_sender_no">پلاک</label>
                                                        <input type="number" step="0.1" class="form-control" id="step_two_bar_sender_no" name="step_two_bar_sender_no" aria-describedby="step_two_bar_sender_no_help" placeholder="">
                                                        <small id="step_two_bar_sender_no_help" class="form-text text-muted">پلاک شرکت را مشخص کنید</small>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_sender_unit">واحد</label>
                                                        <input type="number" class="form-control" id="step_two_bar_sender_unit" name="step_two_bar_sender_unit" aria-describedby="step_two_bar_sender_unit_help" placeholder="">
                                                        <small id="step_two_bar_sender_unit_help" class="form-text text-muted">واحد شرکت را مشخص کنید</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" aria-label="zoom" id="step_two_bar_sender_zoom" name="step_two_bar_sender_zoom">
                                <div class="add_new_address_button">
                                    <a href="javascript:;" onclick="request_add_new_address(this,'sender','open');" class="btn btn-sm btn-info">افزودن آدرس جدید</a>
                                    <a href="javascript:;" onclick="request_add_new_address(this,'sender','close');" class="btn btn-sm btn-danger mz-hidden">انصراف</a>
                                </div>
                            </div>

                            <div class="space-between"></div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_two_bar_receiver_place">محل تخلیه<span class="text-danger">*</span></label>
                                        <select class="form-control select2" onchange="get_details_of_address(this,'receiver');" id="step_two_bar_receiver_place" name="step_two_bar_receiver_place" aria-describedby="step_two_bar_receiver_place_help">
                                            <option value=""></option>
                                            @foreach($receivers as $receiver)
                                                <option value="{{$receiver->id}}">{{$receiver->name}}</option>
                                            @endforeach
                                        </select>
                                        <small id="step_two_bar_receiver_place_help" class="form-text text-muted">شهر محل تخلیه را مشخص کنید</small>
                                    </div>
                                </div>
                            </div>

                            <div class="add_new_address">
                                <div class="add_new_address_inputs">

                                    {{--<div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="step_two_bar_receiver_title">عنوان آدرس مقصد<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="step_two_bar_receiver_title" name="step_two_bar_receiver_title" aria-describedby="step_two_bar_receiver_title_help" placeholder="">
                                                <small id="step_two_bar_receiver_title_help" class="form-text text-muted">عنوان آدرس مقصد را مشخص کنید</small>
                                            </div>
                                        </div>
                                    </div>--}}

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_receiver_province">استان مقصد<span class="text-danger">*</span></label>
                                                        <select class="form-control select2" onchange="change_province(this,'receiver');" id="step_two_bar_receiver_province" name="step_two_bar_receiver_province" aria-describedby="step_two_bar_receiver_province_help">
                                                            <option value=""></option>
                                                            @foreach(\App\Models\Province::get_provinces()->get() as $province)
                                                                <option value="{{$province->id}}">{{$province->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        <small id="step_two_bar_receiver_province_help" class="form-text text-muted">استان مقصد را انتخاب کنید</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_receiver_city">شهر مقصد<span class="text-danger">*</span></label>
                                                        <select class="form-control select2" onchange="change_city(this,'receiver');" id="step_two_bar_receiver_city" name="step_two_bar_receiver_city" aria-describedby="step_two_bar_receiver_city_help"></select>
                                                        <small id="step_two_bar_receiver_city_help" class="form-text text-muted">شهر مقصد را انتخاب کنید</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="step_two_bar_receiver_name">نام گیرنده<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="step_two_bar_receiver_name" name="step_two_bar_receiver_name" aria-describedby="step_two_bar_receiver_name_help" placeholder="">
                                                <small id="step_two_bar_receiver_name_help" class="form-text text-muted">نام گیرنده را بنویسید</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_receiver_mobile">تلفن همراه<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="step_two_bar_receiver_mobile" name="step_two_bar_receiver_mobile" aria-describedby="step_two_bar_receiver_mobile_help" placeholder="">
                                                        <small id="step_two_bar_receiver_mobile_help" class="form-text text-muted">تلفن همراه گیرنده را وارد کنید</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_receiver_phone">تلفن<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="step_two_bar_receiver_phone" name="step_two_bar_receiver_phone" aria-describedby="step_two_bar_receiver_phone_help" placeholder="">
                                                        <small id="step_two_bar_receiver_phone_help" class="form-text text-muted">تلفن را وارد کنید</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="step_two_bar_receiver_address">جزئیات آدرس مقصد<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="step_two_bar_receiver_address" name="step_two_bar_receiver_address" aria-describedby="step_two_bar_receiver_address_help" placeholder="" readonly>
                                                <small id="step_two_bar_receiver_address_help" class="form-text text-muted">آدرس مقصد را روی نقشه مشخص کنید</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_receiver_no">پلاک</label>
                                                        <input type="number" step="0.1" class="form-control" id="step_two_bar_receiver_no" name="step_two_bar_receiver_no" aria-describedby="step_two_bar_receiver_no_help" placeholder="">
                                                        <small id="step_two_bar_receiver_no_help" class="form-text text-muted">پلاک شرکت را مشخص کنید</small>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="step_two_bar_receiver_unit">واحد</label>
                                                        <input type="number" class="form-control" id="step_two_bar_receiver_unit" name="step_two_bar_receiver_unit" aria-describedby="step_two_bar_receiver_unit_help" placeholder="">
                                                        <small id="step_two_bar_receiver_unit_help" class="form-text text-muted">واحد شرکت را مشخص کنید</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" aria-label="zoom" id="step_two_bar_receiver_zoom" name="step_two_bar_receiver_zoom">
                                <div class="add_new_address_button">
                                    <a href="javascript:;" onclick="request_add_new_address(this,'receiver','open');" class="btn btn-sm btn-info">افزودن آدرس جدید</a>
                                    <a href="javascript:;" onclick="request_add_new_address(this,'receiver','close');" class="btn btn-sm btn-danger mz-hidden">انصراف</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-7" id="container_map">
                            <div id="map_id" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container">
                    <div class="title-section">لطفا زمان دقیق بارگیری را وارد کنید</div>

                    <div class="row">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_two_bar_date">تاریخ</label>
                                                <select class="form-control" id="step_two_bar_date" name="step_two_bar_date" aria-describedby="step_two_bar_date_help">
                                                    <option value="0" selected>امروز</option>
                                                    <option value="1">فردا</option>
                                                    <option value="2">پس فردا</option>
                                                </select>
                                                <small id="step_two_bar_date_help" class="form-text text-muted">تاریخ بارگیری را وارد کنید</small>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_two_bar_hour_from">از ساعت</label>
                                                <input type="text" class="form-control timepicker" id="step_two_bar_hour_from" name="step_two_bar_hour_from" aria-describedby="step_two_bar_hour_help" value="{{date('H:i')}}" readonly>
                                                <small id="step_two_bar_hour_from_help" class="form-text text-muted">ساعت بارگیری را وارد کنید</small>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="step_two_bar_hour_to">تا ساعت</label>
                                                <input type="text" class="form-control timepicker" id="step_two_bar_hour_to" name="step_two_bar_hour_to" aria-describedby="step_two_bar_hour_help" value="{{date('H:i')}}" readonly>
                                                <small id="step_two_bar_hour_to_help" class="form-text text-muted">ساعت بارگیری را وارد کنید</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-7"></div>
                    </div>
                </div>
            </div>

            <div id="new_bar_request_content_step_3" class="steps step-3">
                <div class="container">
                    <div class="title-section">لطفا از صحت اطلاعات وارد شده اطمینان حاصل کنید</div>
                    <div class="row">
                        <div class="col-6">
                            <div class="content-box">
                                <ul>
                                    <li><span class="label">عنوان بار</span><strong id="step_one_bar_title_result"></strong></li>
                                    <li><span class="label">نوع بسته بندی</span><strong id="step_one_bar_box_type_result"></strong></li>
                                    <li><span class="label">وزن بار</span><strong id="step_one_bar_weight_result"></strong></li>
                                    <li><span class="label">نوع خودرو</span><strong id="step_one_bar_car_type_result"></strong></li>
                                    <li><span class="label">ویژگی خودرو</span><strong id="step_one_bar_car_details_result"></strong></li>
{{--                                <li><span class="label">تعداد خودرو</span><strong id="step_one_bar_car_count_result"></strong></li>--}}
                                    <li><span class="label">ارزش محموله</span><strong id="step_one_bar_cargo_value_result"></strong></li>
                                    <li><span class="label">نرخ باسکول</span><strong id="step_one_bar_bascule_price_result"></strong></li>
                                    <li><span class="label">نرخ بارگیری</span><strong id="step_one_bar_lading_price_result"></strong></li>
                                    <li><span class="label">نرخ تخلیه</span><strong id="step_one_bar_vacate_price_result"></strong></li>
{{--                                <li><span class="label">سند حمل</span><strong id="step_one_bar_document_result"></strong></li>--}}
                                    <li><span class="label">توضیحات بیشتر</span><strong id="step_one_bar_description_result"></strong></li>
                                    <li class="edit"><a href="javascript:;" onclick="wizard_request_go_to_step(this,'edit');" data-step="1"><i class="fas fa-pencil-alt"></i>ویرایش</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="content-box">
                                <ul>
                                    <li><span class="label">محل بارگیری</span><strong id="sender_address_result"></strong></li>
                                    <li><span class="label">محل تخلیه بار</span><strong id="receiver_address_result"></strong></li>
                                    <li><span class="label">تاریخ</span><strong id="step_two_bar_date_result"></strong></li>
                                    <li><span class="label">از ساعت</span><strong id="step_two_bar_hour_from_result"></strong></li>
                                    <li><span class="label">تا ساعت</span><strong id="step_two_bar_hour_to_result"></strong></li>
                                    <li class="edit"><a href="javascript:;" onclick="wizard_request_go_to_step(this,'edit');" data-step="2"><i class="fas fa-pencil-alt"></i>ویرایش</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="new_bar_request_content_step_4" class="steps step-4">
                <div class="container">
                    <div class="title-section"></div>
                    <div class="row mb-5">
                        <div class="col-6">
                            <div class="content-box">
                                <table>
                                    <tr>
                                        <td><span class="label">میانگین قیمت 5 سفر آخر در این مسیر</span></td>
                                        <td><strong id="step_four_average_five_price">10,000,000 </strong></td>
                                        <td><span class="price"> تومان </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">تعرفه باربری برای این مسیر</span></td>
                                        <td><strong id="step_four_price_this_way">150,000 </strong></td>
                                        <td><span class="price"> تومان </span></td>
                                    </tr>
                                </table>
                                {{--<ul>
                                    <li><span class="label">میانگین قیمت 5 سفر آخر در این مسیر</span><strong id="step_four_average_five_price">10,000,000 </strong><span> تومان </span></li>
                                    <li><span class="label">تعرفه باربری برای این مسیر</span><strong id="step_four_price_this_way">150,000 </strong><span> تومان </span></li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container">
                    <div class="title-section"><h3>لطفا قیمت پیشنهای خود را برای این سفر وارد کنید</h3></div>
                    <div class="row">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="step_four_bar_price"> قیمت پیشنهادی <span class="text-danger">*</span></label>
                                        <input type="text" onkeyup="number_price_format(this);" class="form-control" id="step_four_bar_price" name="step_four_bar_price" aria-describedby="step_four_bar_price_help" placeholder="مثال: 100,000">
                                        <small id="step_four_bar_price_help" class="form-text text-muted">قیمت پیشنهادی بار را در این قسمت بنویسید</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-7"></div>
                    </div>
                </div>
            </div>
        </form>
        <div class="container text-center">
            <div class="row">
                <div class="col-6 text-right">
                    <a href="javascript:;" onclick="wizard_request_go_to_step(this,'prev');" id="prev_step_button" data-step="1" class="btn prev-step mz-hidden">
                        بازگشت
                    </a>
                </div>
                <div class="col-6 text-left">
                    <a href="javascript:;" onclick="wizard_request_go_to_step(this,'next');" id="next_step_button" data-step="1" class="btn next-step">
                        <span>مرحله بعد</span>
                        <i class="fas fa-long-arrow-alt-left"></i>
                    </a>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 text-right">
                    <a href="javascript:;" onclick="wizard_submit_request();" id="submit_step_button" class="btn save-step mz-hidden">
                        <span>ثبت درخواست</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<div class="modal fade" id="global_base_page_modal" role="dialog" aria-labelledby="global_base_page_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="global_base_page_modal_header">
                <h5 class="modal-title" id="global_base_page_modal_label"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="modal-max-scroll">
                    <form action="javascript:global_base_page_modal_form_store();" id="global_base_page_modal_form">
                        <div class="row" id="global_base_page_modal_append_place_holder"></div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" id="global_base_page_modal_footer"></div>
        </div>
    </div>
</div>


{{--<script src="{{asset('metronic-assets/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('metronic-assets/bootstrap-timepicker/js/bootstrap-timepicker.js')}}" type="text/javascript"></script>

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

{{--<script src='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js'></script>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>--}}

<script src="{{asset('site/map/js/mapbox.js')}}"></script>
<script src="{{asset('site/map/js/leaflet.markercluster.js')}}"></script>


<script src="{{asset('site/js/price/mask.js')}}" type="text/javascript"></script>
<script src="{{asset('site/js/price/numeral.js')}}" type="text/javascript"></script>

{{--@include('site.pages.customer.script_data')--}}
<script>
    let sender_marker='';
    let receiver_marker='';
    let customer =@json($customer);
    let features=@json($car_features);
    let cities=@json($cities);
    let provinces=@json($provinces);
    let all_provinces=@json(\App\Models\Province::all());
    let $car_types = @json($car_types);
    let marker_owner='';

    $(document).ready(function(){
        $(`.timepicker`).timepicker({showMeridian: false});
        $('.select2').select2({placeholder:'انتخاب کنید'});
        $('.multi-select').select2({placeholder:'انتخاب کنید'});

        let car_type = "{{$data['car_type']}}";
        if(car_type!=='')
            get_car_feature(`#car_type_${car_type}`);
    });

    let index_counter=0;

    let global_modal = $("#global_base_page_modal");
    let modal_label = $("#global_base_page_modal_label");
    let modal_header = $("#global_base_page_modal_header");
    let modal_form = $("#global_base_page_modal_form");
    let modal_footer = $("#global_base_page_modal_footer");
    let close_button_modal = `<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>`;

    function number_price_format(element){
        $(element).val(numeral($(element).val()).format('0,0'));
    }

    function wizard_submit_request(){
        //ajax goes here
        if (!validate_wizard(4)) {
            scrollToTop();
            return;
        }
        let form = $("#bar_request_form");
        let url = "{{route('order_request.store')}}";
        let formData = new FormData(form[0]);
        formData.append('token', "{{csrf_token()}}");
        formData.append('tariff_request',`${$("#step_four_price_this_way").text()}`);
        formData.append('average_request',`${$("#step_four_average_five_price").text()}`);

        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        $.ajax(url, {
            data: formData,
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    swal.fire({
                        title: 'ثبت درخواست',
                        text: 'عملیات ثبت درخواست با موفقیت انجام شد',
                        type: "success",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand",
                        //showCancelButton: true,
                        //cancelButtonText: 'لغو'
                    }).then(function(result) {
                        if(result.value)
                            window.location = "{{url('/user/bars/request')}}";
                    });
                }else{
                    swal.fire({
                        title: 'ثبت درخواست',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function wizard_request_go_to_step(element,type){
        let head_step='new_bar_request_head_step';
        let content_step='new_bar_request_content_step';
        let current_step=Number($(element).attr('data-step'));

        if(type!=='prev')
            if (!validate_wizard(current_step)) {
                scrollToTop();
                return;
            }
        if(current_step===0)
            if (! change_step_one_bar_weight())
                return;


        let next_step;
        let prev_button=$(`#prev_step_button`);
        let next_button=$(`#next_step_button`);
        let submit_button=$(`#submit_step_button`);
        ///////////////////////////////////////////
        switch (type) {
            case 'next':
                next_step = current_step+1;
                $(`#${head_step}_${current_step}`).removeClass('active').addClass('passed');
                $(`#${content_step}_${current_step}`).removeClass('current');

                $(`#${head_step}_${next_step}`).addClass('active');
                $(`#${content_step}_${next_step}`).addClass('current');
                break;
            case 'prev':
                next_step = current_step-1;
                $(`#${head_step}_${current_step}`).removeClass('active');
                $(`#${content_step}_${current_step}`).removeClass('current');

                $(`#${head_step}_${next_step}`).removeClass('passed').addClass('active');
                $(`#${content_step}_${next_step}`).addClass('current');
                break;
            case 'edit':
                next_step = current_step;
                $(`#${head_step}_3`).removeClass('active');
                $(`#${content_step}_3`).removeClass('current');
                $(`#${head_step}_${current_step}`).removeClass('passed').addClass('active');
                $(`#${content_step}_${current_step}`).addClass('current');
                for(let i=current_step+1;i<=3;i++){
                    $(`#${head_step}_${i}`).removeClass('passed').removeClass('active');
                    $(`#${content_step}_${i}`).removeClass('current');
                }
                break;
        }
        ///////////////////////////////////////////
        if(next_step<=1)
            prev_button.addClass('mz-hidden');
        else
            prev_button.removeClass('mz-hidden');
        ///////////////////////////////////////////
        if(next_step>3)
            next_button.addClass('mz-hidden');
        else
            next_button.removeClass('mz-hidden');
        ///////////////////////////////////////////
        prev_button.attr('data-step',next_step);
        next_button.attr('data-step',next_step);
        ///////////////////////////////////////////
        if(next_step===2)
            get_city_map();
        ///////////////////////////////////////////
        if(next_step===3)
            show_wizard_result_content();
        ///////////////////////////////////////////
        if(next_step===4) {
            calculate_way_price();
            prev_button.addClass('mz-hidden');
            next_button.addClass('mz-hidden');
            submit_button.removeClass('mz-hidden');
        }else{
            submit_button.addClass('mz-hidden');
        }

        scrollToTop();
    }

    function validate_wizard(current_step){
        let form = $("#bar_request_form");
        let all_rules={rules:{}};
        switch(current_step){
            case 1:
                all_rules.rules={
                    step_one_bar_title: {required: true},
                    step_one_bar_box_type: {required: true},
                    step_one_bar_weight: {required: true},
                    step_one_bar_car_type: {required: true},
                    step_one_bar_car_details: {required: true},
                    // step_one_bar_document: {required: true},
                    // step_one_bar_car_count: {required: true},
                    step_one_bar_cargo_value: {required: true},
                    //step_one_bar_description: {required: true},

                    step_two_bar_sender_place: {required: true},
                    step_two_bar_sender_address: {required: true},
                    step_two_bar_sender_no: {required: true},
                    step_two_bar_sender_unit: {required: true},
                    step_two_bar_receiver_place: {required: true},
                    step_two_bar_receiver_address: {required: true},
                    step_two_bar_receiver_no: {required: true},
                    step_two_bar_receiver_unit: {required: true},
                    step_two_bar_date: {required: true},
                    step_two_bar_hour: {required: true},
                    step_four_bar_price: {required: true},
                };
                break;
            case 2:
                all_rules.rules={
                    step_two_bar_sender_place: {required: true},
                    step_two_bar_sender_address: {required: true},
                    step_two_bar_sender_no: {required: true},
                    step_two_bar_sender_unit: {required: true},
                    step_two_bar_receiver_place: {required: true},
                    step_two_bar_receiver_address: {required: true},
                    step_two_bar_receiver_no: {required: true},
                    step_two_bar_receiver_unit: {required: true},
                    step_two_bar_date: {required: true},
                    step_two_bar_hour: {required: true},
                };
                break;
            case 4:
                all_rules.rules={
                    step_four_bar_price: {required: true},
                };
                break;
        }
        form.validate(all_rules);
        console.log(all_rules);
        console.log(form.valid());
        if (!form.valid()) {
            console.log('not valid');
            return false;
        }

        console.log('valid');
        return true;
    }

    function calculate_way_price(){
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });
        let sender_place = $("#step_two_bar_sender_place").val();
        let receiver_place = $("#step_two_bar_receiver_place").val();
        let car_type = $('input[name=step_one_bar_car_type]:checked').val();

        let cargo_value = $("#step_one_bar_cargo_value").val();
        let bascule_price = $("#step_one_bar_bascule_price").val();
        let lading_price = $("#step_one_bar_lading_price").val();
        let vacate_price = $("#step_one_bar_vacate_price").val();
        let weight = $("#step_one_bar_weight").val();

        let url="{{route('calculate_way_price')}}";
        let data={
            sender:sender_place,
            receiver:receiver_place,
            car_type:car_type,
            weight:weight,
            cargo_value:cargo_value,
            bascule:bascule_price,
            lading_price:lading_price,
            vacate_price:vacate_price
        };
        $.ajax(url, {
            data: data,
            type: 'post',
            async:false,
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    $("#step_four_average_five_price").html(response.data.average_request);
                    $("#step_four_price_this_way").html(response.data.tariff_request);
                }else{
                    console.log(response);
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function show_wizard_result_content(){
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let sender_place = $("#step_two_bar_sender_place");
        let receiver_place = $("#step_two_bar_receiver_place");

        $('#step_one_bar_title_result').html($( "#step_one_bar_title option:selected" ).text());
        $('#step_one_bar_box_type_result').html($( "#step_one_bar_box_type option:selected" ).text());
        $('#step_one_bar_weight_result').html(`${$("#step_one_bar_weight").val()} تن `);

        // $('#step_one_bar_car_type_result').html($('input[name=step_one_bar_car_type]:checked').val());//step_one_bar_car_type =>val
        let label = $('input[name=step_one_bar_car_type]:checked').attr('id');
        $('#step_one_bar_car_type_result').html($(`label[for=${label}]`).text());//step_one_bar_car_type =>label

        // $('#step_one_bar_car_details_result').html($("#step_one_bar_car_details").val()); //value
        let details = $("#step_one_bar_car_details").find('option:selected');
        let details_text='';
        $.each(details,function(key,detail){details_text+=`${$(detail).text()}${key+1===details.length?'':'، '}`;});
        $('#step_one_bar_car_details_result').html(details_text);

        // $('#step_one_bar_document_result').html($('input[name=step_one_bar_document]:checked').val());//step_one_bar_document =>val
        label = $('input[name=step_one_bar_document]:checked').attr('id');
        $('#step_one_bar_document_result').html($(`label[for=${label}]`).text());//step_one_bar_car_type =>label

        // $('#step_one_bar_car_count_result').html(`${$("#step_one_bar_car_count").val()} دستگاه `);
        $('#step_one_bar_cargo_value_result').html(`${$("#step_one_bar_cargo_value").val()} تومان `);
        $('#step_one_bar_bascule_price_result').html(`${$("#step_one_bar_bascule_price").val()} تومان `);
        $('#step_one_bar_lading_price_result').html(`${$("#step_one_bar_lading_price").val()} تومان `);
        $('#step_one_bar_vacate_price_result').html(`${$("#step_one_bar_vacate_price").val()} تومان `);
        $('#step_one_bar_description_result').html($("#step_one_bar_description").val());

        // let place = sender_place.find('option:selected');
        // let sender_address=`${$(place).text()} - ${$("#step_two_bar_sender_address").val()} - پلاک ${$("#step_two_bar_sender_no").val()} - واحد ${$("#step_two_bar_sender_unit").val()}`;
        // $("#sender_address_result").html(sender_address);
        //
        // place = receiver_place.find('option:selected');
        // let receiver_address=`${$(place).text()} - ${$("#step_two_bar_receiver_address").val()} - پلاک ${$("#step_two_bar_receiver_no").val()} - واحد ${$("#step_two_bar_receiver_unit").val()}`;
        // $("#receiver_address_result").html(receiver_address);

        $("#step_two_bar_date_result").html($( "#step_two_bar_date option:selected" ).text());
        $("#step_two_bar_hour_from_result").html($("#step_two_bar_hour_from").val());
        $("#step_two_bar_hour_to_result").html($("#step_two_bar_hour_to").val());

        let url="{{route('get_all_address_request')}}";
        let data={sender:sender_place.val(),receiver:receiver_place.val()};
        $.ajax(url, {
            data: data,
            type: 'post',
            async:false,
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    let sender={no:response.data.sender.pelak !=null ? response.data.sender.pelak :'',unit:response.data.sender.vahed!=null?response.data.sender.vahed:''};
                    let receiver={no:response.data.receiver.pelak !=null ? response.data.receiver.pelak :'',unit:response.data.receiver.vahed!=null?response.data.receiver.vahed:''};
                    let sender_address=`${response.data.sender.address} - پلاک ${sender.no} - واحد ${sender.unit}`;
                    let receiver_address=`${response.data.receiver.address} - پلاک ${receiver.no} - واحد ${receiver.unit}`;
                    $("#sender_address_result").html(sender_address);
                    $("#receiver_address_result").html(receiver_address);
                }else{
                    console.log(response);
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function get_car_feature(element) {
        let car_id = Number($(element).val());
        let car_features = [];
        let options = '<option value=""></option>';
        $.each(features, function (key, feature) {
            if (feature['car_type_id'] === car_id)
                options += `<option value="${feature.id}">${feature.title}</option>`;
        });
        console.log(car_features);
        let car_feature = $("#step_one_bar_car_details");
        car_feature.html('').append(options);

        $.each($car_types,function(key,car_type){
            if(Number(car_type.id)===car_id)
                $("#step_one_bar_weight").attr('min',0.1).attr('max',car_type.tonage);
        });
        // car_feature.select2({placeholder:'انتخاب کنید'});
    }

    function request_add_new_address(element,type,kind){
        if(type==='sender' && kind==='open')
            marker_owner='sender';
        else if(type==='receiver' && kind==='open')
            marker_owner='receiver';
        else{
            marker_owner='';
        }
        $(element).closest('.add_new_address').find('.add_new_address_inputs').slideToggle();
        let a = $(element).parent().find('a');
        let address_place = $(`#step_two_bar_${type}_place`);
        let address_place_parent = address_place.closest('.row');
        if(kind==='close') {
            $(a[1]).addClass('mz-hidden');
            address_place.removeClass('mz-hidden');
            address_place_parent.removeClass('mz-hidden');
            $(a[0]).text('افزودن آدرس جدید').attr('onclick',`request_add_new_address(this,'${type}','open');`);
            return;
        }
        address_place.addClass('mz-hidden');
        address_place_parent.addClass('mz-hidden');
        $(a[1]).removeClass('mz-hidden');
        $(a[0]).text('ثبت آدرس').attr('onclick',`store_new_address_request(this,'${type}');`);
    }

    function store_new_address_request(element,type){
        if (!validate_new_address(element,type)) {
            return;
        }

        let data = {};
        let url;
        data['address'] = $(`#step_two_bar_${type}_address`).val();
        data['zoom'] = $(`#step_two_bar_${type}_zoom`).val();
        data['pelak'] = $(`#step_two_bar_${type}_no`).val();
        data['vahed'] = $(`#step_two_bar_${type}_unit`).val();
        data['customer_id'] = customer.id;
        if(type==='sender') {
            data['title'] = $(`#step_two_bar_${type}_title`).val();
            data['long'] = sender_marker.getLatLng().lng;
            data['lat'] = sender_marker.getLatLng().lat;
            data['city_id']=$("#step_two_bar_sender_city").val();
            url="{{asset('/customer_address')}}";
        }
        else if(type==='receiver'){
            data['name']=$("#step_two_bar_receiver_name").val();
            data['mobile']=$("#step_two_bar_receiver_mobile").val();
            data['tels']=[];
            data['tels']=$("#step_two_bar_receiver_phone").val();
            data['city_id'] = $("#step_two_bar_receiver_city").val();
            data['long'] = receiver_marker.getLatLng().lng;
            data['lat'] = receiver_marker.getLatLng().lat;
            url="{{asset('/receiver')}}";
        }

        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        $.ajax(url, {
            data: data,
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    let data=`<option value="${response.address.id}" selected>${type==='sender'?response.address.title:response.address.name}</option>`;
                    $(`#step_two_bar_${type}_place`).append(data).trigger('change');
                    let status = 'success';
                    let myTitle = 'عملیات موفق';
                    let message = 'عملیات ثبت آدرس با موفقیت انجام شد.';
                    ToastrMessage(myTitle, message, status);
                    request_add_new_address(element,type,'close');
                }else{
                    swal.fire({
                        title: 'ثبت آدرس',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function validate_new_address(element,type){
        let flag=false;
        let error_message='لطفا ابتدا فیلد های الزامی را پر کرده سپس اقدام به ثبت کنید';
        let fields = {};
        fields['province'] = $(`#step_two_bar_${type}_province`).val();
        fields['city'] = $(`#step_two_bar_${type}_city`).val();
        fields['address'] = $(`#step_two_bar_${type}_address`).val();
        if(type==='sender'){
            fields['title'] = $("#step_two_bar_sender_title").val();
        }else{
            fields['name'] = $("#step_two_bar_receiver_name").val();
            fields['mobile'] = $("#step_two_bar_receiver_mobile").val();
            fields['phone'] = $("#step_two_bar_receiver_phone").val();
        }
        $.each(fields,function(key,field){
            if(field==='')
                flag=true;
            if(key==='mobile'){
                let numbers = '0123456789';
                let num = persian_to_latin_num(field);
                let st = num.substr(0,2);
                if(num.length!==11){
                    flag=true;
                    error_message = 'شماره تلفن همراه باید 11 رقمی باشد';
                }
                for(let i=0;i<11;i++){
                    let char = num.substr(i,1);
                    if(numbers.indexOf(char)===-1){
                        flag=true;
                        error_message = 'فرمت تلفن همراه صحیح وارد نشده است';
                    }
                }
                if(st !=='09'){
                    flag=true;
                    error_message = 'شماره تلفن همراه با 09 شروع میشود';
                }
                if(field==='')
                    error_message = 'شماره تلفن همراه نمیتواند خالی باشد';
            }
        });


        if(flag) {
            swal.fire({
                title: 'ثبت آدرس',
                text: error_message,
                type: "error",
                confirmButtonText: 'بسیار خب',
                confirmButtonClass: "btn btn-brand"
            });
            return false;
        }
        return true;
    }

    function persian_to_latin_num(st){
        let ret = '';
        if(st !== '') {
            st  = st+'';
            ret = st.split('۰').join('0');
            ret = ret.split('۱').join('1');
            ret = ret.split('۲').join('2');
            ret = ret.split('۳').join('3');
            ret = ret.split('۴').join('4');
            ret = ret.split('۵').join('5');
            ret = ret.split('۶').join('6');
            ret = ret.split('۷').join('7');
            ret = ret.split('۸').join('8');
            ret = ret.split('۹').join('9');
        }
        return ret;
    }

    function get_details_of_address(element,type){
        let id = $(element).val();
        mz_global_async_ajax('order/get/address','post',{id:id,type:type}).then( (response) => {
            swal.close();
            if(response.status==='success'){
                let address = response.data;
                let content=`<div class="row address_detail_box">
                                <div class="col-12 item"><span class="title">استان</span><span class="description">${address.city.province.title}</span></div>
                                <div class="col-12 item"><span class="title">شهر</span><span class="description">${address.city.title}</span></div>
                                <div class="col-12 item"><span class="title">آدرس</span><span class="description">${address.address}</span></div>
                                <div class="col-12 item"><span class="title">پلاک</span><span class="description">${address.pelak != null ? address.pelak :''}</span></div>
                                <div class="col-12 item"><span class="title">واحد</span><span class="description">${address.vahed != null ? address.vahed :''}</span></div>
                             </div>`;
                modal_label.html('').append(` نمایش جزئیات آدرس: ${type==='sender'?address.title:address.name} `);
                //modal_header.css('display','none');
                modal_form.html('').append(content);
                modal_footer.html('').append('<button type="button" class="btn btn-secondary" data-dismiss="modal">تایید</button>');
                global_modal.modal('show');
            }
        });
    }

    //let current_map = L.map('map_id').setView([35.6914789,51.3384505], 11);//تهران
    //let current_map = L.map('map_id').setView([31.6413517,54.4177819], 7.25);//یزد
    //let current_map = L.map('map_id').setView([36.315591,59.567964], 7.25);//مشهد
    // let current_map = L.map('map_id').setView([34.301579,48.821656], 12);//ملایر
    //let current_map = L.map('map_id').setView([32.1155095,54.4595693], 5);//ایران
    get_city_map();

    function change_province(element,type) {
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let province=$(element).val();
        let data='<option value=""></option>';
        if(type==='sender') {
            $.each(cities, function (key, city) {
                if (Number(city.province_id) === Number(province) && Number(city.accepted) === 1)
                    data += `<option value="${city.id}">${city.title}</option>`;
            });
        }else{
            $.each(cities, function (key, city) {
                if (Number(city.province_id) === Number(province) )
                    data += `<option value="${city.id}">${city.title}</option>`;
            });
        }

        $(`#step_two_bar_${type}_city`).html('').append(data);
        let map_data = {latitude:'31.6413517',longitude:'54.4177819',zoom:'7.25'};
        $.each(all_provinces,function(key,item){
            if(Number(item.id)===Number(province))
                map_data=item;
        });
        get_city_map(map_data);
       swal.close();
    }

    function change_city(element,type){
        let city=$(element).val();
        let map_data = {latitude:'31.6413517',longitude:'54.4177819',zoom:'7.25'};
        $.each(cities,function(key,item){
            if(Number(item.id)===Number(city))
                map_data=item;
        });
        get_city_map(map_data);
    }

    function get_city_map(data=null){
        $("#container_map").html('').append(`<div id="map_id" style="height: 500px;"></div>`);
        let container = L.DomUtil.get('map_id');if(container != null){ container._leaflet_id = null; }

        let map_data={latitude:'32.1155095',longitude:'54.4595693',zoom:'5'};
        if(data!==null)
            map_data={latitude:data.latitude,longitude:data.longitude,zoom:data.zoom};
        /////////////////////////////////////////////////////////////////////
        L.mapbox.accessToken = 'pk.eyJ1IjoibXJtYWppZHoiLCJhIjoiY2s2N2Y0eW54MDRmdzNscXQ3a3ZqdW85NCJ9.Tj2XpAzzlVVRu7d380SK0A';
        let current_map = L.mapbox.map('map_id')
            .setView([map_data.latitude, map_data.longitude], map_data.zoom)
            .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
        // current_map.addControl(new MapboxBrowserLanguage({  defaultLanguage: 'fa'}));
        let markers = new L.MarkerClusterGroup();
        current_map.addLayer(markers);
        /////////////////////////////////////////////////////////////////////

        sender_marker = L.marker([1, 1], {draggable: 'true'});
        receiver_marker = L.marker([10, 10], {draggable: 'true'});
        let lng,lat;
        current_map.on('click', function (e) {
            $(`#step_two_bar_${marker_owner}_zoom`).val(current_map.getZoom());
            console.log(current_map.getZoom());
            if(marker_owner==='sender'){
                sender_marker.setLatLng([e.latlng.lat, e.latlng.lng]);
                sender_marker.addTo(current_map);
                lng = sender_marker.getLatLng().lng;
                lat = sender_marker.getLatLng().lat;
            }
            else if(marker_owner==='receiver'){
                receiver_marker.setLatLng([e.latlng.lat, e.latlng.lng]);
                receiver_marker.addTo(current_map);
                lng = receiver_marker.getLatLng().lng;
                lat =receiver_marker.getLatLng().lat;
            }
            $.ajax({
                url: 'https://nominatim.openstreetmap.org/reverse?accept-language=fa',
                data: {
                    format: 'jsonv2',
                    lat: lat,
                    lon: lng,
                },
                success: function (response) {
                    console.log(response);
                    $(`#step_two_bar_${marker_owner}_address`).val(response.display_name);
                    console.log(response);
                }
            });
        });
    }

    function change_step_one_bar_weight(element){
        let flag=false;
        element = $(element);
        let max = Number(element.attr('max'));
        let min = Number(element.attr('min'));
        let value = Number(element.val());
        if(value > max) {
            flag=true;
            //element.val(max);
        }
        if(value < min)
            element.val(min);

        if(flag) {
            swal.fire({
                title: 'تعیین وزن بار',
                text: ` وزن درخواستی شما بیش از حد مجاز برای این خودرو میباشد، حداکثر وزن برای این خودرو ${max} تن میباشد،لطفا خودروی درخواستی خود را تعویض و یا وزن بار درخواستی را کاهش دهید `,
                type: "error",
                confirmButtonText: 'بسیار خب',
                confirmButtonClass: "btn btn-brand"
            });
            return false;
        }
        return true;
    }

</script>
</body>
</html>
