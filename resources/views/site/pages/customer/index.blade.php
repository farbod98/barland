<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

@include('site.parts.customer-main-board')

@include('site.parts.support')

@include('site.parts.work_with_us')

@include('site.parts.download-app')

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')
<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}"></script>
<script>
    let asset_url='{{asset('/site/')}}';
    let driver_page_work=$("#driver-page-work");
    $(window).scroll(function () {

        if ($(window).scrollTop() >= 70) {
            $('nav.top-navigation').addClass('has_scroll');
            $('#site-main-logo').attr('src',`${asset_url}/images/logo-black.png`);
        } else {
            $('nav.top-navigation').removeClass('has_scroll');
            $('#site-main-logo').attr('src',`${asset_url}/images/logo-white.png`);
        }

        let winHeight = $(this).height();
        let scrollTop = $(this).scrollTop();
        let elemHeight = driver_page_work.height();
        let elementTop = driver_page_work.position().top;
        if (elementTop < scrollTop + winHeight && scrollTop < elementTop + elemHeight){
            setTimeout(function () {$("#driver-page-work-step-2").addClass('active');},1500);
            setTimeout(function () {$("#driver-page-work-step-3").addClass('active');},3000);
        }

    });

    let setIntervalHandler;
    $(document).ready(function(){
        $('.select2').select2({placeholder:'انتخاب کنید'});
        /*let box = $(".main-board .box-inputs")[0];
        let box_height = $(box).height();
        let window_height = $(window).height();
        let margin_top = ((window_height - box_height)/2)-70;
        console.log(box_height);
        console.log(window_height);
        console.log(margin_top);
        $(box).css('margin-top',margin_top);*/

        $('.regular').slick({
            speed: 2000,
            // autoplaySpeed: 2000,
            arrows:true,
            autoplay: false,
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: false,
            infinite: true,
            cssEase: 'linear',
            rtl: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        setIntervalHandler = setInterval(function(){ driver_page_support();}, 5000);
    });

    function driver_page_support(element=null){
        let support = $("#driver_page_support");
        let items = support.find('.item');
        let active_item = support.find('.item.active');
        let next_item = active_item.next();

        if(element!==null)
            next_item = $(element);
        else {
            $.each(items, function (key, item) {
                if ($(item).hasClass('active')) {
                    if (key + 1 === items.length) {
                        next_item = items[0];
                        next_item = $(next_item);
                    }
                }
            });
        }

        active_item.removeClass('active');
        $(active_item.find('img')).attr('src',`${asset_url}/images/black-car.png`);
        $(active_item.find('.item-content.active')).removeClass('active');
        $(active_item.find('span')).removeClass('active');

        $(next_item).addClass('active');
        $(next_item.find('img')).attr('src',`${asset_url}/images/white-car.png`);
        $(next_item.find('.item-content')).addClass('active');
        $(next_item.find('span')).addClass('active');
    }

    function set_new_support_item(element){
        // When you want to cancel it:
        element=$(element).closest('.item');
        clearInterval(setIntervalHandler);
        setIntervalHandler = 0; // I just do this so I know I've cleared the interval
        driver_page_support(element);
        //setInterval
        setIntervalHandler = setInterval(function(){ driver_page_support();}, 5000);
    }

    function get_price_and_order_registration(){
        // validate goes here
        if (!validate_wizard())
            return;
        let form_order_from = $("#form_order_from").val();
        let form_order_to = $("#form_order_to").val();
        let form_order_car_type = $("input[name=form_order_car_type]:checked").val();
        let data = `<input type="hidden" name="send_form_order_from" value="${form_order_from}">
                    <input type="hidden" name="send_form_order_to" value="${form_order_to}">
                    <input type="hidden" name="send_form_order_car_type" value="${form_order_car_type}">`;

        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let url = '{{route('get_ten_last_request')}}';
        $.ajax(url, {
            data:{
                from:form_order_from,
                to:form_order_to,
                car_type:form_order_car_type
            },
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    if(response.status==='success') {
                        let form = $("#go_to_order_page_form");
                        form.html('').append(data);
                        $("#average_request").text(response.data['average_request']);
                        $("#tariff_request").text(response.data['tariff_request']);
                        $("#get_price_and_order_registration_form").addClass('mz-hidden');
                        $("#customer_order_registration").removeClass('mz-hidden');
                    }
                    /*swal.fire({
                        title: 'ثبت دیدگاه',
                        text: 'عملیات ثبت دیدگاه با موفقیت انجام شد',
                        type: "success",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                    global_modal_id.modal('hide');*/
                }else{
                    swal.fire({
                        title: 'استعلام قیمت',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با ‍پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function validate_wizard(){
        let form = $("#get_price_and_order_registration_form");
        let all_rules={rules:{}};
        all_rules.rules={
            form_order_from: {required: true},
            form_order_to: {required: true},
            form_order_car_type: {required: true},
        };
        form.validate(all_rules);
        console.log(all_rules);
        console.log(form.valid());
        if (!form.valid()) {
            console.log('not valid');
            return false;
        }
        console.log('valid');
        return true;
    }

    function go_to_order_page_form() {
        $("#go_to_order_page_form").submit();
    }

    /*function dropdown_menu_item(element){
        $(element).find('i.has-sub-menu').toggleClass('fa-chevron-left').toggleClass('fa-chevron-down');
        $(element).parent().find('> ul').toggleClass('active');
    }*/
</script>
</body>
</html>
