<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="about-page faq-page clear-pt-150">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="top-section container mb-5">
                    <h2 class="title">
                        سوالات متداول
                    </h2>
                </div>
            </div>
        </div>

        <div class="row pt-5 mb-5">
            <div class="col-md-6">
                <div class="item">
                    <header>
                        <h4>جابجایی کالا چگونه صورت میگیرد؟</h4>
                        <div class="icon"><i class="fas fa-question-circle"></i></div>
                    </header>
                    <div class="content">
                        جابجایی به صورت درون شهری ، برون شهری انجام میپذیرد.
                    </div>
                </div>

                <div class="item">
                    <header>
                        <h4>جابجایی کالا چگونه صورت میگیرد؟</h4>
                        <div class="icon"><i class="fas fa-question-circle"></i></div>
                    </header>
                    <div class="content">
                        جابجایی به صورت درون شهری ، برون شهری انجام میپذیرد.
                    </div>
                </div>

                <div class="item">
                    <header>
                        <h4>جابجایی کالا چگونه صورت میگیرد؟</h4>
                        <div class="icon"><i class="fas fa-question-circle"></i></div>
                    </header>
                    <div class="content">
                        جابجایی به صورت درون شهری ، برون شهری انجام میپذیرد.
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="item">
                    <header>
                        <h4>جابجایی کالا چگونه صورت میگیرد؟</h4>
                        <div class="icon"><i class="fas fa-question-circle"></i></div>
                    </header>
                    <div class="content">
                        جابجایی به صورت درون شهری ، برون شهری انجام میپذیرد.
                    </div>
                </div>

                <div class="item">
                    <header>
                        <h4>جابجایی کالا چگونه صورت میگیرد؟</h4>
                        <div class="icon"><i class="fas fa-question-circle"></i></div>
                    </header>
                    <div class="content">
                        جابجایی به صورت درون شهری ، برون شهری انجام میپذیرد.
                    </div>
                </div>

                <div class="item">
                    <header>
                        <h4>جابجایی کالا چگونه صورت میگیرد؟</h4>
                        <div class="icon"><i class="fas fa-question-circle"></i></div>
                    </header>
                    <div class="content">
                        جابجایی به صورت درون شهری ، برون شهری انجام میپذیرد.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--@include('site.parts.customer-main-board')--}}

{{--@include('site.parts.support')--}}

{{--@include('site.parts.work_with_us')--}}

{{--@include('site.parts.download-app')--}}

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')
<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script>
</script>
</body>
</html>
