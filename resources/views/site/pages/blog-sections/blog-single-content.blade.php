<article class="mb-4">
    <a href="{{url('blog-single',$post->slug)}}" class="block-link"> <img class="img-responsive" src="{{$post->image}}" alt=""> </a>
    <div class="news-detail">
        <div class="row">
            <div class="col-md-3 text-center">
                <div class="avatar"> <img class="img-circle" src="{{$post->user->image}}" alt=""></div>
                <p>{{$post->shamsi}}</p>
                <p><i class="fa fa-comment"></i>{{count($post->comments)}} </p>
            </div>
            <div class="col-md-9">
                <a href="#"></a>
                <div class="description">
                    {!! $post->body !!}
                </div>
            </div>
        </div>
    </div>
</article>
