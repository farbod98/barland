<aside class="col-md-4 col-sm-4 col-xs-12 sidebar-blog">
    <section class="widget widget_search">
        <form role="search" method="get" class="search-form" id="sidebar_search_form" action="{{url('search')}}">
            <div class="search-box">
                <input type="search" class="form-control" aria-label="جستجو" placeholder="جستجو …" value="" name="s">
                <div class="icon" onclick="document.getElementById('sidebar_search_form').submit()"><i class="fas fa-search"></i></div>
            </div>
        </form>
    </section>

    <section class="widget widget_last_news">
        <div class="top-section">آخرین اخبار</div>
        <div class="content-box">
            <ul>
                @foreach(\App\Models\Post::orderBy('created_at','desc')->limit(5)->get() as $post)
                <li><a href="{{url('blog-single',$post->slug)}}" class="block-link"><i class="fas fa-angle-double-left"></i>{{$post->title}}</a></li>
                @endforeach
            </ul>
        </div>
    </section>

    <section class="widget widget_last_news">
        <div class="top-section">پربازدیدترین ها</div>
        <div class="content-box">
            <ul>
                @foreach(\App\Models\Post::orderBy('viewed','desc')->limit(5)->get() as $post)
                    <li><a href="{{url('blog-single',$post->slug)}}" class="block-link"><i class="fas fa-angle-double-left"></i>{{$post->title}}</a></li>
                @endforeach
            </ul>
        </div>
    </section>

    <section class="widget widget_last_news">
        <div class="top-section">دسته بندی ها</div>
        <div class="content-box">
            <ul>
                @foreach(\App\Models\Category::all() as $category)
                    <li><a href="{{url('blog/category='.$category->slug)}}" class="block-link"><span class="title">{{$category->title}}</span><span class="count">{{count($category->posts)}}</span></a></li>
                @endforeach
            </ul>
        </div>
    </section>

    <section class="widget widget_tags">
        <div class="top-section">تگ ها</div>
        <div class="content-box">
            <ul>
                @foreach(\App\Models\Tag::all() as $tag)
                    <li><a href="{{url('blog/tag='.$tag->slug)}}">{{$tag->title}}</a></li>
                @endforeach
            </ul>
        </div>
    </section>

</aside>
