<div class="modal fade" id="global_base_page_modal" role="dialog" aria-labelledby="global_base_page_modal" aria-hidden="true">
    <div class="modal-dialog" role="document" id="global_base_page_modal_dialog">
        <div class="modal-content">
            <div class="modal-header" id="global_base_page_modal_header">
                <h5 class="modal-title" id="global_base_page_modal_label"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="global_base_page_modal_body">
                <div class="modal-max-scroll">
                    <form action="javascript:global_base_page_modal_form_store();" id="global_base_page_modal_form">
                        <div class="row" id="global_base_page_modal_append_place_holder"></div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" id="global_base_page_modal_footer"></div>
        </div>
    </div>
</div>

<script>
    let global_modal_id = $("#global_base_page_modal");
    let global_modal_dialog = $("#global_base_page_modal_dialog");
    let global_modal_label = $("#global_base_page_modal_label");
    let global_modal_header = $("#global_base_page_modal_header");
    let global_modal_form = $("#global_base_page_modal_form");
    let global_modal_body = $("#global_base_page_modal_body");
    let global_modal_footer = $("#global_base_page_modal_footer");

    /*  let content=`<div class="row"></div>`;
        let footer_close_btn = `<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>`;
        let footer_submit = `<button type="submit" form="global_base_page_modal_form" class="btn btn-primary">مشاهده جزئیات</button>`;
        global_modal_label.html('').append(`جزئیات بار`);
        global_modal_form.attr('action',`javascript:store_comment(${id});`);
        global_modal_footer.html('').append(`${footer_submit} ${footer_close_btn}`);
        // global_modal_header.css('display','none');
        global_modal_form.html('').append(content);
        global_modal_dialog.addClass('modal-lg');
        global_modal_id.modal('show');
    */
    /*
    let global_modal_id = document.getElementById('global_base_page_modal');
    let global_modal_label = document.getElementById('global_base_page_modal_label');
    let global_modal_header = document.getElementById('global_base_page_modal_header');
    let global_modal_form = document.getElementById('global_base_page_modal_form');
    let global_modal_footer = document.getElementById('global_base_page_modal_footer');
    */
</script>

