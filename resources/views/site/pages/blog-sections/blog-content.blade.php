<div class="col-md-8 col-sm-8 col-xs-12">
    @forelse($posts as $post)
        @include('site.pages.blog-sections.blog-item' ,compact($post))
        @empty
        <div class="row text-center">
            <div class="col-12">
                <div class="alert alert-danger">جستجوی شما نتیجه ای در بر نداشت</div>
            </div>
            <div class="col-12">
                <div class="mt-5 redirect">
                    <a href="{{asset('/')}}">
                        <i class="fas fa-home"></i>
                        <span> بازگشت به صفحه اصلی</span>
                    </a>
                </div>
            </div>

            <div class="col-12">
                <div class="mt-5 redirect">
                    <a href="{{asset('/blog')}}">
                        <i class="fas fa-blog"></i>
                        <span> بازگشت به وبلاگ</span>
                    </a>
                </div>
            </div>
        </div>
    @endforelse

    {!! $posts->render() !!}
</div>
