<!--=======  COMMENTS =========-->
<div class="line-separator">* * *</div>

<div class="comments">

    <!--=======  LEAVE COMMENTS =========-->
    <h4 class="font-alegreya">دیدگاه شما</h4>
    <form id="blog_comment_form" action="javascript:store_comment();">
        <ul class="row">
           {{-- <li class="col-sm-12">
                <label> عنوان
                    <input type="text" class="form-control" name="title" placeholder="">
                </label>
            </li>--}}
            <li class="col-sm-12">
                <label> متن دیدگاه
                    <textarea class="form-control" name="content" id="content"></textarea>
                </label>
            </li>
            <li class="col-sm-12">
                <div class="row captcha">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="captcha">حروف تصویر را وارد کنید</label>
                        <input id="captcha" name="captcha" maxlength="5" minlength="5" type="text" class="">
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 captcha-p-t">
                    <span onclick="refreshCaptcha()">
                        <img src="{{asset('site/images/refresh.png')}}" alt="refresh">
                    </span>
                        <img id="captcha_image" src="{{captcha_src()}}" alt="captcha">
                    </div>
                </div>
            </li>

            <li class="col-sm-12">
                <button type="submit" class="btn btn-submit">ثبت دیدگاه</button>
            </li>
        </ul>
    </form>

    <div class="line-separator">* * *</div>

    <h4 class="top-section-text">دیدگاه ها</h4>
    <ul class="media-list">
        @if(count($post->comments)>0)
            @foreach($post->comments as $comment)
            <li class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="{{$comment->user->image}}" alt="">
                    </a>
                </div>
                <div class="media-body-back">
                    <div class="media-body light-gray-bg">
                        <h6 class="media-heading"><span class="user_name">{{$comment->user->name}}</span> <span class="date">{{$comment->shamsi}}</span></h6>
                        <p>
                            {{strip_tags($comment->comment)}}
                        </p>
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="javascript:;" onclick="show_modal_reply(this,'{{$comment->id}}')" class="reply"><i class="fas fa-reply"></i>پاسخ</a>
                        @endif
                    </div>
                </div>
            </li>
                @foreach($comment->comments as $cmt)
                    <li class="media margin-right-80">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="{{$cmt->user->image}}" alt="">
                            </a>
                        </div>
                        <div class="media-body-back">
                            <div class="media-body light-gray-bg">
                                <h6 class="media-heading"><span class="user_name">{{$cmt->user->name}}</span> <span class="date">{{$cmt->shamsi}}</span></h6>
                                <p>
                                    {{strip_tags($cmt->comment)}}
                                </p>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                    <a href="javascript:;" onclick="show_modal_reply(this,'{{$comment->id}}')" class="reply"><i class="fas fa-reply"></i>پاسخ</a>
                                @endif
                            </div>
                        </div>
                    </li>
                @endforeach
            @endforeach
        @endif
    </ul>
</div>
