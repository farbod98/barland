<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('site/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('metronic-assets/toastr/css/toastr.min.rtl.css')}}">
    <link rel="stylesheet" href="{{asset('metronic-assets/sweetalert2/css/sweetalert2.min.rtl.css')}}">
    <title>Bar land</title>
</head>
<body>

<section class="main-page login-page">
    <div class="login-box">
        <h1>{{$status=='2'?'پیامک تایید شماره تلفن':'رمز عبور'}}</h1>
        @if($status=='2')
            <h3>لطفا <strong>کد پنج رقمی</strong> ارسال شده به {{$mobile}} را در قسمت زیر وارد کنید</h3>
        @else
            <h3>لطفا رمز عبور خود را وارد کنید </h3>
        @endif

        <form action="{{asset('confirm')}}" method="post" id="confirm">
            <div class="form-group">
                <label for="code">{{$status=='1'?'رمز عبور':'کد تایید'}}</label>
                <input type="text" class="form-control" id="code" aria-describedby="formHelp" placeholder="مثال: 12345" name="code">
            </div>
            <div class="form-group">
                <div class="next-step text-center pull-left">
                    <a href="javascript:;" onclick="send_data();" class="btn">
                        <span>مرحله بعد</span>
                        <i class="fas fa-long-arrow-alt-left"></i>
                    </a>
                </div>
                <div class="prev-step text-center pull-right">
                    <a href="{{asset('/login/customer')}}" class="btn">
                        بازگشت
                    </a>
                </div>
            </div>
            @csrf
        </form>
    </div>

    <div class="top-left logo"><a href="{{asset('/')}}"><img src="{{asset('site/images/logo-black.png')}}" alt="logo"></a></div>

</section>

<script src="{{asset('site/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('site/js/bootstrap.min.js')}}"></script>

<script src="{{asset('metronic-assets/sweetalert2/js/sweetalert2.min.js')}}"></script>
<script src="{{asset('metronic-assets/toastr/js/toastr.min.js')}}"></script>

<script>
    function openNav() {
        document.getElementById("sidebar-nav-toggle").style.width = "300px";
        document.getElementById("nav-overlay").style.display = "block";
        document.getElementById("nav-overlay").style.height = document.body.offsetHeight+'px';
    }

    function closeNav() {
        let overlay = document.getElementById("nav-overlay");
        document.getElementById("sidebar-nav-toggle").style.width = "0";
        // overlay.style.width = overlay.style.width+300;
        setTimeout(function(){ $(overlay).fadeOut() }, 250);
        //overlay.fadeOut()();
    }

    function scrollToTop(){
        $("html, body").animate({scrollTop: 0}, 1000);
    }

    function send_data(){
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });
        let code = $("#code").val();
        $.ajax("{{asset('confirm')}}", {
                type: 'post',
                data: {code:code},
                headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
                dataType: 'json',
                success: function (response) {
                    swal.close();
                    if (response.status === 'success') {
                        window.location = response.route;
                    } else {
                        let status = 'error';
                        let myTitle = 'عملیات ناموفق';
                        let message = response.message;
                        ToastrMessage(myTitle, message, status);
                    }
                },
                error: function (data) {
                    swal.close();
                    $("#waiting_image_ajax").css('visibility', 'hidden');
                    let status = 'error';
                    let myTitle = 'عملیات ناموفق';
                    let message = 'عملیات با مشکل مواجه شد. لطفا با ‍پشتیبانی تماس حاصل فرمایید.';
                    ToastrMessage(myTitle, message, status);
                }
            });
    }

</script>
</body>
</html>
