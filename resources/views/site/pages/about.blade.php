<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="about-page clear-pt-150">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="top-section container mb-5">
                    <h2 class="title">
                        درباره بارلند
                    </h2>
                    <div class="description">
                        <p>
                            آسان‌بار تخصصی‌ترین و محبوب‌ترین سامانه اینترنتی ارائه دهنده خدمات حمل‌و‌نقل کالا در سراسر ایران به رانندگان پایه یک و شرکت‌های حمل‌ونقل (باربری) است، به عبارت دیگر آسان‌بار پل ارتباطی رانندگان و شرکت‌های حمل‌ونقل در بستر اینترنت است. شرکت‌های حمل‌ونقل از طریق اپلیکیشن، بارهای خود را با تمامی جزئیات اعلام می کنند.
                        </p>
                        <p>
                            رانندگان بدون نیاز به حضور در پایانه‌های حمل‌ونقل، از طریق اپلیکیشن آسان‌بار، بارهای اعلام شده توسط شرکت‌های حمل‌و‌نقل را مشاهده، ارزیابی و انتخاب می‌کنند و بدین ترتیب می‌توانند بیشتر از گذشته در کنار خانواده‌ی خود باشند و برنامه‌ریزی دقیق تری را انجام دهند.
                        </p>
                        <p>
                            از سمتی شرکت‌های حمل‌ونقل در مدت زمان کمتری راننده مناسب و تایید شده توسط آسان‌بار را برای حمل بار خود انتخاب می کنند و می توانند از طریق اپلیکیشن بار خود را از مبدا تا مقصد پی‌گیری کنند.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5 mb-5 contact">
            <div class="col-md-4">
                <div class="item">
                    <div class="icon"><i class="fas fa-home"></i></div>
                    <div class="content">
                        <div class="heading">آدرس:</div>
                        <address>تهران، پاسداران، دشتستان دوم، پلاک ۱۴، واحد ۴</address>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="icon"><i class="fas fa-phone-square"></i></div>
                    <div class="content">
                        <div class="heading">تلفن:</div>
                        <a href="tel:02188202561">02188202561</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="icon"><i class="fas fa-envelope"></i></div>
                    <div class="content">
                        <div class="heading">ایمیل:</div>
                        <a href="mailto:support@barland.com">support@barland.com</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pt-5 mb-5">
            <div class="col-md-6 section-2">
                <p>
                اتوبار بی نظیر سهم زیادی از حمل و نقل های بین شهری و درون شهری را به خود اختصاص داده و بدست آوردن این افتخار حاصل سال ها تجربه و اعتماد شما گرامیان می باشد. اتوبار ظریف بار در حمل اثاثیه، بسته بندی، انبار داری متخصص بوده و به جزئیات مربوط به این موارد توجه ویژه ای دارد.
                </p>
                <p>
                تلاش شبانه روزی پرسنل و همچنین اطمینان مشتریان و همراهی آن ها باعث شده که ظریف بار در دهه گذشته تبدیل به بزرگترین و پر افتخار ترین شرکت اتوبار در ایران شود.
                </p>
                <p>
                همچنین اتوبار و باربری ظریف بار دارای گواهینامه ایزو 9001:2008 می باشد که نشان دهنده پیاده سازی مدیریت کیفیت در پایه های سازمان با توجه به خواسته های مشتری می باشد. اطمینان حاصل می کنیم که همه خواسته ها و کیفیت مورد نظر مشتری فراهم آید.
                </p>
            </div>
            <div class="col-md-6">
                <div class="img"><img src="{{asset('site/images/main-truck.png')}}" alt="main-truck"></div>
            </div>
        </div>
    </div>
</section>
{{--@include('site.parts.customer-main-board')--}}

{{--@include('site.parts.support')--}}

{{--@include('site.parts.work_with_us')--}}

{{--@include('site.parts.download-app')--}}

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')
<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script>
</script>
</body>
</html>
