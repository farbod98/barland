<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('site/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/responsive.css')}}">
    <title>{{$page_title}}</title>
</head>
<body>

<section class="main-page">
    <div class="text-center">
        <h1 class="page-title">سامانه هوشمند حمل و نقل بار</h1>
        <div class="button">
            <a href="{{asset('/customer')}}" class="btn page-button">صاحب بار</a>
        </div>
        <div class="button">
            <a href="{{asset('/driver')}}" class="btn page-button">راننده</a>
        </div>
    </div>

    <nav class="top-right menu navbar-light">
        <button class="navbar-toggler" type="button" onclick="openNav();">
            <span class="navbar-toggler-icon"></span>
            <span class="title">منو</span>
        </button>
    </nav>

</section>

<div id="sidebar-nav-toggle" class="sidebar-nav-toggle">

    <div class="top-close-button">
        <a href="javascript:void(0)" onclick="closeNav()"><i class="fas fa-times"></i></a>
    </div>

    <ul class="menu-items">
        <li><a href="{{asset('/about')}}" onclick="closeNav()">درباره ما</a></li>
        <li><a href="{{asset('/faq')}}" onclick="closeNav()">سوالات متداول</a></li>
        <li><a href="{{asset('/contact')}}" onclick="closeNav()">تماس با ما</a></li>
        <li><a href="{{asset('/news-latter')}}" onclick="closeNav()">خبرنامه</a></li>
        <li><a href="{{asset('/partner')}}" onclick="closeNav()">همکاران ما</a></li>
    </ul>

    <div class="socials">
        <ul class="social-items">
            <li><a href="#" onclick="closeNav()"><i class="fab fa-twitter"></i></a></li>
            <li><a href="#" onclick="closeNav()"><i class="fab fa-instagram"></i></a></li>
            <li><a href="#" onclick="closeNav()"><i class="fab fa-linkedin-in"></i></a></li>
        </ul>
    </div>
</div>

<div id="nav-overlay" onclick="closeNav()">
    <div class="close-button">
        <a href="javascript:void(0)" onclick="closeNav()">بستن</a>
    </div>
</div>

<script src="{{asset('site/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('site/js/bootstrap.min.js')}}"></script>
<script>
    function openNav() {
        console.log($(window).width());
        document.getElementById("sidebar-nav-toggle").style.width = "300px";
        document.getElementById("nav-overlay").style.display = "block";
        document.getElementById("nav-overlay").style.height = document.body.offsetHeight+'px';
    }

    function closeNav() {
        let overlay = document.getElementById("nav-overlay");
        document.getElementById("sidebar-nav-toggle").style.width = "0";
        // overlay.style.width = overlay.style.width+300;
        setTimeout(function(){ $(overlay).fadeOut() }, 250);
        //overlay.fadeOut()();
    }

    function scrollToTop(){
        $("html, body").animate({scrollTop: 0}, 1000);
    }

</script>
</body>
</html>
