<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
    <style>
        .modal.show .modal-dialog{margin-top: 150px;}
        .captcha {color: #9e9e9e;}
        .captcha input {padding: 6px;border-radius: 5px;border: 2px solid #9e9e9e;width: 100px;margin-right: 30px;text-align: left;}
        .captcha .captcha-p-t {padding-top: 7px;}
        .captcha label {display: inline-block !important;max-width: 100%;width: auto !important;margin-bottom: 5px;font-weight: 700;}
        .captcha .invalid-feedback{color:red;}
    </style>
</head>
<body>

@include('site.parts.menu')

<section class="latest-news blog-single clear-pt-150">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="top-section container mb-5">
                    <h2 class="title">
                        {{$post->title}}
                    </h2>
                </div>
            </div>
        </div>

        <div class="row pt-5 mb-5">
            @include('site.pages.blog-sections.sidebar')
            <div class="col-md-8 col-sm-8 col-xs-12">
                @include('site.pages.blog-sections.blog-single-content')
                @include('site.pages.blog-sections.comments')
            </div>
        </div>
    </div>
</section>
{{--@include('site.parts.customer-main-board')--}}

{{--@include('site.parts.support')--}}

{{--@include('site.parts.work_with_us')--}}

{{--@include('site.parts.download-app')--}}

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')
<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
@include('site.pages.blog-sections.global_modal')
<script>
    function show_modal_reply(element,id){
        let content=`<ul class="row">
                        {{--<li class="col-sm-12">
                            <label> نام
                                <input type="text" class="form-control" name="modal_title" placeholder="">
                            </label>
                        </li>--}}

                        <li class="col-sm-12">
                            <label> متن دیدگاه
                                <textarea class="form-control" name="modal_content" id="modal_content"></textarea>
                            </label>
                        </li>
                        <li class="col-sm-12">
                            <div class="row captcha">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <label for="modal_captcha">حروف تصویر را وارد کنید</label>
                                    <input id="modal_captcha" name="modal_captcha" maxlength="5" minlength="5" type="text" class="">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-6 captcha-p-t">
                                <span onclick="refreshCaptcha(${id})">
                                    <img src="{{asset('site/images/refresh.png')}}" alt="refresh">
                                </span>
                                    <img id="modal_captcha_image" src="{{captcha_src()}}" alt="captcha">
                                </div>
                            </div>
                        </li>
                    </ul>`;
        let footer_close_btn = `<button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>`;
        let footer_submit = `<button type="submit" form="global_base_page_modal_form" class="btn btn-primary">ثبت دیدگاه</button>`;
        let user_name = $(element).closest('.media-body').find('.media-heading span.user_name');
        global_modal_label.html('').append(` پاسخ به ${$(user_name).text()}`);
        global_modal_form.attr('action',`javascript:store_comment(${id});`);
        global_modal_footer.html('').append(`${footer_submit} ${footer_close_btn}`);
        // global_modal_header.css('display','none');
        global_modal_form.html('').append(content);
        global_modal_dialog.addClass('modal-lg');
        global_modal_id.modal('show');
    }

    function store_comment(id=0){
        if (!validate_comment(id))
            return;
        console.log(id);
        let url = "{{route('store_comment')}}";
        let form = $("#blog_comment_form");
        let formData = id===0 ? new FormData(form[0]) : new FormData(global_modal_form[0]);
        formData.append('token', "{{csrf_token()}}");
        formData.append('parent_id',`${id}`);
        formData.append('commentable_id',`{{$post->id}}`);
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        $.ajax(url, {
            data: formData,
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                swal.close();
                refreshCaptcha(id);
                if(response.status==='success') {
                    let prefix = id===0 ? '' : 'modal_';
                    $(`#${prefix}content`).val('');
                    $(`#${prefix}captcha`).val('');
                    swal.fire({
                        title: 'ثبت دیدگاه',
                        text: 'عملیات ثبت دیدگاه با موفقیت انجام شد',
                        type: "success",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                    global_modal_id.modal('hide');
                }else{
                    swal.fire({
                        title: 'ثبت دیدگاه',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با ‍پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function validate_comment(id){
        let form = id===0 ? $("#blog_comment_form") : global_modal_form;
        let prefix = id===0 ? '' : 'modal_';
        let all_rules={rules:{}};
        all_rules.rules[`${prefix}content`]={required: true};
        form.validate(all_rules);
        if (!form.valid()) {
            console.log('not valid');
            swal.fire({
                title: 'ثبت دیدگاه',
                text: 'متن دیدگاه نمیتواند خالی باشد',
                type: "error",
                confirmButtonText: 'بسیار خب',
                confirmButtonClass: "btn btn-brand"
            });
            return false;
        }
        let value = $(`#${prefix}content`).val();
        console.log(value);
        if(value.length < 3){
            console.log('not valid');
            swal.fire({
                title: 'ثبت دیدگاه',
                text: 'متن دیدگاه کوتاه است',
                type: "error",
                confirmButtonText: 'بسیار خب',
                confirmButtonClass: "btn btn-brand"
            });
            return false;
        }
        console.log('valid');
        return true;
    }

    function refreshCaptcha(id=0) {
        let prefix = id===0 ? '' : 'modal_';
        let captcha = $(`#${prefix}captcha_image`);
        captcha.attr('src', captcha.attr('src') + '{{ captcha_src() }}');
    }
</script>
</body>
</html>
