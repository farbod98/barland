<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="user_page">
    @include('site.parts.user-sidebar')
    <div class="main-content user_info">
        <div class="padding-around user-info-input">
            <form action="" id="reset_password_form">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="current_password">رمز عبور فعلی</label>
                            <input type="password" onblur="check_password(this,'old');" class="form-control" id="current_password" name="current_password" placeholder="" value="">
                            <div id="current_password-feedback"></div>
                            <span class="form-text text-muted">لطفا رمز عبور فعلی را وارد کنید</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="new_password">رمز عبور جدید</label>
                            <input type="password" onblur="check_password(this,'new');" class="form-control" id="new_password" name="new_password" placeholder="" value="">
                            <div id="new_password-feedback"></div>
                            <span class="form-text text-muted">لطفا رمز عبور را وارد کنید</span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="repeat_password">تکرار رمز عبور</label>
                            <input type="password" onblur="check_password(this,'repeat');" class="form-control" id="repeat_password" name="repeat_password" placeholder="" value="">
                            <div id="repeat_password-feedback"></div>
                            <span class="form-text text-muted">لطفا تکرار رمز عبور را وارد کنید</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center mt-5">
                        <a href="javascript:;" onclick="update_user_password();" class="btn btn-update">به روز رسانی رمز عبور</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

{{--@include('site.parts.social')--}}

{{--@include('site.parts.footer')--}}

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}"></script>
<script>
    function change_sortable_filter(element,filter){
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
    }

    function check_password(element,type){
        let pass = $(element).val();
        let id = $(element).attr('id');
        let title='';
        switch(type){
            case 'old':
                title='رمز عبور فعلی';
                break;
            case 'new':
                title='رمز عبور جدید';
                break;
            case 'repeat':
                title='تکرار رمز عبور';
                break;
        }

        if(pass===''){
            $(element).addClass('is-invalid');
            $(`#${id}-feedback`).addClass('invalid-feedback').text(` مقدار ${title} نمیتواند خالی باشد `);
        }
        else if(pass.length < 3){
            $(element).addClass('is-invalid');
            $(`#${id}-feedback`).addClass('invalid-feedback').text(` مقدار ${title} نمیتواند کوچکتر از 3 کاراکتر باشد `);
        }
        else{
            let password = $("#new_password").val();
            let repeat_password=$("#repeat_password");
            if(type==='repeat'){
                if(pass===password){
                    $(element).removeClass('is-invalid').addClass('is-valid');
                    $(`#${id}-feedback`).removeAttr('class').text('');
                }else{
                    $(element).addClass('is-invalid');
                    $(`#${id}-feedback`).addClass('invalid-feedback').text(` رمز عبور با ${title} مطابقت ندارد `);
                }
            }
            else{
                let repeat_val=repeat_password.val();
                if(repeat_val!==password && repeat_val!==''){
                    repeat_password.addClass('is-invalid');
                    $(`#repeat_password-feedback`).addClass('invalid-feedback').text(` رمز عبور با ${title} مطابقت ندارد `);
                }
                $(element).removeClass('is-invalid').addClass('is-valid');
                $(`#${id}-feedback`).removeAttr('class').text('');
            }
        }
    }

    function update_user_password(){
        if (!validate_user_form_password_fields())
            return;

        if($('#current_password-feedback').hasClass('invalid-feedback') || $('#new_password-feedback').hasClass('invalid-feedback') || $('#repeat_password-feedback').hasClass('invalid-feedback'))
            return;
        let form =$('#reset_password_form');
        let formData = new FormData(form[0]);
        swal.fire({
            title:'درحال بررسی اطلاعات',
            text: 'لطفا منتظر بمانید',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let url = "{{url('/user/update/password')}}";
        $.ajax({
            url: url,
            data:formData,
            type: 'post',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
        }).done(function(response){
            swal.close();
            if (response.status === 'success') {
                swal.fire({
                    title: 'تغییر رمز عبور!',
                    text: 'عملیات تغییر رمز عبور با موفقیت انجام شد',
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: 'بسیار خب'
                });
                $("#current_password").val('');
                $("#new_password").val('');
                $("#repeat_password").val('');
            }
            if (response.status === 'error') {
                swal.fire({
                    title: 'تغییر رمز عبور!',
                    text: response.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonText: 'بسیار خب'
                });
            }
        }).fail(function(error){
            console.log(error);
            swal.close();
        });

    }

    function validate_user_form_password_fields(){
        let form =$('#reset_password_form');
        let all_rules={
            rules: {
                current_password: {required: true},
                new_password: {required: true},
                repeat_password:{required:true}
            }
        };
        form.validate(all_rules);
        if (!form.valid())
            return false;
        return true;
    }
</script>
</body>
</html>
