<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance:textfield;
        }
    </style>
    <link rel="stylesheet" href="{{asset('metronic-assets/toastr/css/toastr.min.rtl.css')}}">
    <link rel="stylesheet" href="{{asset('metronic-assets/sweetalert2/css/sweetalert2.min.rtl.css')}}">
</head>
<body>

<section class="main-page login-page">
    <div class="login-box">
        <h1>فراموشی رمز عبور</h1>
        <h3>شماره تلفن همراه خود را وارد کنید</h3>
        <form action="{{asset('/forgot_password')}}" method="post" id="login">
            <div class="form-group">
                <label for="from">شماره موبایل</label>
                <input type="number" class="form-control" id="from" aria-describedby="formHelp" placeholder="مثال: 09353322681" name="mobile">
            </div>
            <div class="form-group">
                <div class="register-new text-center">
                    <a href="javascript:;" onclick="submit()" class="btn">مرحله بعد
                        {{--                        <span>مرحله بعد</span>--}}
                        <i class="fas fa-long-arrow-alt-left"></i>
                    </a>
                </div>
                <a href="{{asset('login/'.session('type'))}}">بازگشت</a>
            </div>
            @csrf
            <input type="hidden" name="role" value="{{$page_type}}">
        </form>
    </div>

    <div class="top-left logo"><a href="{{asset('/')}}"><img src="{{asset('/site/images/logo-black.png')}}" alt="logo"></a></div>

</section>

<script src="{{asset('/site/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('/site/js/bootstrap.min.js')}}"></script>
<script src="{{asset('metronic-assets/sweetalert2/js/sweetalert2.min.js')}}"></script>
<script src="{{asset('metronic-assets/toastr/js/toastr.min.js')}}"></script>
<script>
    function submit() {
        let number=$('#from').val();
        if(number[0]!=='0' || number[1]!=='9' || number.length!==11) {
            let status = 'error';
            let myTitle = 'عملیات ناموفق';
            let message = 'شماره نامعتبر است';
            ToastrMessage(myTitle, message, status);
            return;
        }

        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let url ='{{asset('/forgot_password')}}';
        $.ajax(url, {
            data: {mobile:number,role:'{{$page_type}}'},
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    window.location = "{{url('/confirm')}}";
                }
                if(response.status==='error') {
                    swal.fire({
                        title: 'عملیات ناموفق',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });//.then(function(result) {
                        //if(result.value)
                        //    window.location = "{ {url('/driver')}}";
                    //});
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });

    }
</script>
</body>
</html>
