<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <script src="{{asset('site/js/vue.js')}}" type="text/javascript"></script>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
    <link rel="stylesheet" href="{{asset('metronic-assets/sweetalert2/css/sweetalert2.min.rtl.css')}}"/>
    <link rel="stylesheet" href="{{asset('metronic-assets/select2/css/select2.min.css')}}" />
</head>
<body>

@include('site.parts.menu')

<section class="driver_page_bars_list" id="vueJs">
    <div class="container">
        <div class="row">
            <div class="col-md-3 bar-filters">
                <aside>
                    <div class="card">
                        <div class="card-header">
                            فیلتر ها
                        </div>
                        {{--$selected_data=[];
                        $selected_data['from_state']='';
                        $selected_data['from_city']=['selected'=>'','options'=>[]];
                        $selected_data['to_state']='';
                        $selected_data['to_city']=['selected'=>'','options'=>[]];--}}

                        <div class="card-body">
                            <form action="">
                                <div class="form-group">
                                    <label for="filter_from_state">استان مبدا</label>
                                    <select class="form-control select2" name="filter_from_state" id="filter_from_state" onchange="change_cities(this,'from',1);">
                                        <option value=""></option>
                                        @foreach(\App\Models\Province::get_accepted_provinces() as $province)
                                            <option value="{{$province->id}}" {{$province->id==$selected_data['from_state']?'selected':''}}>{{$province->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="filter_from_city">شهر مبدا</label>
                                    <select class="form-control select2" name="filter_from_city" id="filter_from_city" onchange="get_city(this,2);">
                                        <option value=""></option>
                                        @foreach($selected_data['from_city']['options'] as $city)
                                            <option value="{{$city->id}}" {{$city->id==$selected_data['from_city']['selected']?'selected':''}}>{{$city->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="filter_to_state">استان مقصد</label>
                                    <select class="form-control select2" name="filter_to_state" id="filter_to_state" onchange="change_cities(this,'to',3);">
                                        <option value=""></option>
                                        @foreach(\App\Models\Province::get_provinces()->get() as $province)
                                            <option value="{{$province->id}}" {{$province->id==$selected_data['to_state']?'selected':''}}>{{$province->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="filter_to_city">شهر مقصد</label>
                                    <select class="form-control select2" name="filter_to_city" id="filter_to_city" onchange="get_city(this,4);">
                                        <option value=""></option>
                                        @foreach($selected_data['to_city']['options'] as $city)
                                            <option value="{{$city->id}}" {{$city->id==$selected_data['to_city']['selected']?'selected':''}}>{{$city->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mt-5">
                                    <a href="javascript:;" class="btn filter-btn" onclick="go_to_order_page_form();">اعمال فیلتر</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="col-md-9">
                <div class="bar-sorting">
                    <ul>
                        <li>مرتب سازی بر اساس : </li>
                        <li><a href="javascript:;" onclick="change_sortable_filter(this,'max_price');" class="active">بیشترین قیمت</a></li>
                        <li><a href="javascript:;" onclick="change_sortable_filter(this,'min_weight');">کمترین وزن</a></li>
                        <li><a href="javascript:;" onclick="change_sortable_filter(this,'nearest_time');">نزدیک ترین زمان</a></li>
                    </ul>
                </div>
                <p v-if="alt==1">سفارشات به مقصدهای نزدیک مقصد انتخابی</p>
                <p v-if="alt==2">سفارشی پیدا نشد</p>
                <div class="bars-list" id="bar_list">
                    <div class="bar-item" v-for="order in orders">
                        <div class="right-box">
                            <div class="details">
                                <ul>
                                    <li><span class="label">تاریخ بارگیری</span><span class="value">@{{ order.date }}</span></li>
                                    <li><span class="label">عنوان بار</span><span class="value">@{{order.product.title}}</span></li>
                                    <li><span class="label">بسته بندی</span><span class="value">@{{order.packing.title}}</span></li>
                                    <li><span class="label">وزن بار</span><span class="value">@{{order.tonage}}</span></li>
                                </ul>
                            </div>
                            <div class="design-road">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="from"><span class="label">مبدا</span></td>
                                        <td class="road"><i class="fas fa-truck reverse-icon"></i> <span class="letter">__________</span> <i class="fas fa-map-marker-alt"></i></td>
                                        <td class="to"><span class="label">مقصد</span></td>
                                    </tr>
                                    <tr class="tr-value">
                                        <td class="from"><span class="value pt-3">@{{order.start_city.title}}</span></td>
                                        <td class="road"></td>
                                        <td class="to"><span class="value pt-3">@{{order.dest_city.title}}</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="left-box">
                            <span>قیمت پیشنهادی صاحب بار:</span>
                            <span class="price">@{{order.price}}</span>
                            <a v-if="order.selected==0" href="javascript:;" class="btn filter-btn" @click="select_bar(order,1)">انتخاب بار</a>
                            <a v-else href="javascript:;" class="btn btn-danger filter-btn" style="background-color: #bd2130" @click="select_bar(order,0)">لغو انتخاب</a>
                        </div>
                        <div class="extra">
                            <div class="details">
                                <ul>
                                    <li><span class="label">ساعت دقیق</span><span class="value">@{{ order.date }}</span></li>
                                    <li><span class="label">میزان بیمه</span><span class="value">12 میلیون</span></li>
                                    <li><span class="label">سند حمل</span><span class="value">بارنامه</span></li>
                                </ul>
                            </div>
                            <div class="more text-center"><a href="javascript:;" data-type="open" onclick="more_details_bar_item(this);">اطلاعات بیشتر <i class="fas fa-chevron-down"></i></a></div>
                        </div>

                    </div>
                </div>

                {{--<div class="bars-list">--}}
                    {{--@foreach($bars as $bar)--}}

                        {{--<div class="bar-item">--}}
                            {{--<div class="right-box">--}}
                                {{--<div class="details">--}}
                                    {{--<ul>--}}
                                        {{--<li><span class="label">تاریخ بارگیری</span><span class="value">{{$bar->date}}</span></li>--}}
                                        {{--<li><span class="label">عنوان بار</span><span class="value">{{$bar->product->title}}</span></li>--}}
                                        {{--<li><span class="label">بسته بندی</span><span class="value">{{$bar->packing->title}}</span></li>--}}
                                        {{--<li><span class="label">وزن بار</span><span class="value">{{$bar->tonage}}</span></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="design-road">--}}
                                    {{--<table>--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td class="from"><span class="label">مبدا</span></td>--}}
                                            {{--<td class="road"><i class="fas fa-truck reverse-icon"></i> <span class="letter">__________</span> <i class="fas fa-map-marker-alt"></i></td>--}}
                                            {{--<td class="to"><span class="label">مقصد</span></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr class="tr-value">--}}
                                            {{--<td class="from"><span class="value pt-3">{{$bar->start_city->slug}}</span></td>--}}
                                            {{--<td class="road"></td>--}}
                                            {{--<td class="to"><span class="value pt-3">{{$bar->dest_city->slug}}</span></td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="left-box">--}}
                                {{--<span>قیمت پیشنهادی صاحب بار:</span>--}}
                                {{--<span class="price">{{$bar->price}}</span>--}}
                                {{--<a href="javascript:;" class="btn filter-btn" onclick="select_bar();">انتخاب بار</a>--}}
                            {{--</div>--}}
                            {{--<div class="extra">--}}
                                {{--<div class="details">--}}
                                    {{--<ul>--}}
                                        {{--<li><span class="label">ساعت دقیق</span><span class="value">10:20</span></li>--}}
                                        {{--<li><span class="label">میزان بیمه</span><span class="value">12 میلیون</span></li>--}}
                                        {{--<li><span class="label">سند حمل</span><span class="value">بارنامه</span></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="more text-center"><a href="javascript:;" data-type="open" onclick="more_details_bar_item(this);">اطلاعات بیشتر <i class="fas fa-chevron-down"></i></a></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                    {{--@endforeach--}}

                {{--</div>--}}
            </div>
        </div>
    </div>
</section>

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('site/js/axios.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}"></script>
<script>
    let query='';
    let cities=@json(\App\Models\City::all());
    let filters=[];
    let data=@json($filters);

    let filter_types={start_prov:1,start_city:2,dest_prov:3,dest_city:4};
    $.each(data,function(key,item){
        item=item.split(':');
        key=item[0];
        filters[filter_types[key]]=item[1];
    });
    // console.log(filters);

    $(document).ready(function(){
        $('.select2').select2({placeholder:'انتخاب کنید'});
    });

    function change_cities(element,type,index) {
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let province=$(element).val();
        let data='<option value=""></option>';
        filters[index]=province;
        let city_index= type==='from'?'start_city':'dest_city';
        filters[filter_types[city_index]]='';
        $.each(cities,function (key,city) {
            if(Number(city.province_id)===Number(province))
                data+=`<option value="${city.id}">${city.title}</option>`;
        });
        $(`#filter_${type}_city`).html('').append(data);
        change_url();
        swal.close();
    }

    function change_url() {
        // console.log(filters);
       data=`${filters[0]?'sort:'+filters[0]+';':''}${filters[1]?'start_prov:'+filters[1]+';':''}${filters[2]?'start_city:'+filters[2]+';':''}${filters[3]?'dest_prov:'+filters[3]+';':''}${filters[4]?'dest_city:'+filters[4]+';':''}${filters[5]?'page:'+filters[5]+';':''}`;
       data=data.slice(0, -1);
        // let filters=query.split(';');
        // let value='';
        // let arr=[];
        // $.each(filters ,function (key,filter) {
        //     arr=filter.split(':');
        //
        // });
        window.history.pushState("", "", `/bars/${data}`);
    }

    function change_sortable_filter(element,filter){
        filters[0]=filter;
        change_url();
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
        que.setOrders(data);
    }

    function more_details_bar_item(element){
        let type = $(element).attr('data-type');
        let close = `بستن <i class="fas fa-chevron-up"></i>`;
        let open = `اطلاعات بیشتر <i class="fas fa-chevron-down"></i>`;
        //let details =
        //$(element).closest('div.extra').find('> .details').slideToggle();
        if(type==='close')
            $(element).html(open).attr('data-type','open').closest('div.extra').find('> .details').slideToggle();
        else
            $(element).html(close).attr('data-type','close').closest('div.extra').find('> .details').slideToggle();
    }

    function get_city(element,index) {
        let city=$(element).val();
        filters[index]=city;
        change_url();
    }

    function go_to_order_page_form() {
        que.setOrders(data);
    }

    let que = new Vue({
        el: '#vueJs',
        data: {
            orders: [],
            alt:0,
            page: 1,
            ast:"{{rtrim(asset(''),'/')}}",
        },
        methods: {
            setOrders: function (text) {
                let vm = this;
                swal.fire({
                    title: 'لطفا منتظر بمانید',
                    text:'درحال دریافت اطلاعات',
                    allowOutsideClick:false,
                    onOpen: function() {
                        swal.showLoading()
                    }
                });

                axios.post("{{url('get_orders')}}", {data:text}).then(function (response) {
                     vm.orders = response.data.result.orders;
                     vm.alt = response.data.result.is_alt;
                     vm.page=Number(response.data.result.page);
                     vm.last_page=Number(response.data.result.last_page);
                     swal.close();
                })
            },select_bar:function (order,status) {
                axios.post("{{url('driver_accept')}}", {data:order.id,status: status,user:'{{auth()->user()->id}}'}).then(function (response) {
                    // console.log(response.data.result.orders.packing);
                    // vm.orders = response.data.result.orders;
                    // vm.alt = response.data.result.is_alt;
                    // vm.page=Number(response.data.result.page);
                    // vm.last_page=Number(response.data.result.last_page);
                    order.selected=status;
                    swal.close();
                })
            }
        },
        mounted: function () {
            let filters = '';
                {{--let url = window.location.pathname;--}}
                {{--let url_data = url.split('/');--}}
                {{--if (url_data.length === 3)--}}
                {{--filters = url_data[2];--}}
                {{--else {--}}
                {{--filters = 'city:null;expertise:null;education:null;sex:null;language:null;search:null;';--}}
                {{--}--}}
                {{--filters=unescape(decodeURIComponent(filters));--}}
                {{--@if(isset($doctors))--}}

                {{--let search='{{$search2}}';--}}


                {{--filters = `orginal_search:${search};`;--}}
                {{--//console.log(filters);--}}
                {{--@endif--}}
                {{--set_filter_data(filters);--}}
                this.setOrders(data);

        },
    })
</script>

</body>
</html>
