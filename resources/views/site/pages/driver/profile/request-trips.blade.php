<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="user_page">
    @include('site.parts.user-sidebar')
    <div class="main-content user_info">
        <div class="padding-around user-info-input">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="name">نام</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="مجید">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="name">نام خانوادگی</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="ذوالفقاری">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="name">ایمیل</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="a@b.com">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="name">شماره همراه</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="09353322681">
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="padding-around user-info-input">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label>نوع خودرو</label>
                        <div class="box-inputs-toggle">
                            <input type="radio" name="car_type" value="khavar" id="khavar" checked="checked" />
                            <label for="khavar">خاور</label>
                            <input type="radio" name="car_type" value="tak" id="tak"/>
                            <label for="tak">تک</label>
                            <input type="radio" name="car_type" value="dah_charkh" id="dah_charkh" />
                            <label for="dah_charkh">ده چرخ</label>
                            <input type="radio" name="car_type" value="trailer" id="trailer"/>
                            <label for="trailer">تریلی</label>
                            <input type="radio" name="car_type" value="kamar_shekan" id="kamar_shekan"/>
                            <label for="kamar_shekan">کمرشکن</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-mb car_no">
                        <label>پلاک</label>
                        <ul>
                            <li><input type="text" class="form-control" value="12" maxlength="2" aria-label="car_no"></li>
                            <li><input type="text" class="form-control" value="ع" maxlength="1" aria-label="car_no"></li>
                            <li><input type="text" class="form-control" value="345" maxlength="3" aria-label="car_no"></li>
                            <li><input type="text" class="form-control" value="ایران" aria-label="car_no" disabled></li>
                            <li><input type="text" class="form-control" value="67" maxlength="2" aria-label="car_no"></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="name">شماره هوشمند راننده</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="name">شماره هوشمند خودرو</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-mb">
                        <label for="filter_from_city">ویژگی خودرو</label>
                        <select class="form-control" name="filter_from_city" id="filter_from_city">
                            <option value="tehran">مسقف</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center mt-5">
                    <a href="#" class="btn btn-cancel">لغو</a>
                    <a href="#" class="btn btn-update">ذخیره تغییرات</a>
                </div>
            </div>
        </div>

    </div>
</section>

{{--@include('site.parts.social')--}}

{{--@include('site.parts.footer')--}}

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        let li_s = $('.user_page > aside.sidebar nav li');
        $.each(li_s,function(key,li){
            if($(li).hasClass('active'))
                if($(li).parent().hasClass('dropdown_menu'))
                    $(li).parent().parent().find('> a')[0].click();
        });
    });
    function change_sortable_filter(element,filter){
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
    }

    function select_bar(){alert('بار انتخاب شد');}

    function more_details_bar_item(element){
        let type = $(element).attr('data-type');
        let close = `بستن <i class="fas fa-chevron-up"></i>`;
        let open = `اطلاعات بیشتر <i class="fas fa-chevron-down"></i>`;
        //let details =
        //$(element).closest('div.extra').find('> .details').slideToggle();
        if(type==='close')
            $(element).html(open).attr('data-type','open').closest('div.extra').find('> .details').slideToggle();
        else
            $(element).html(close).attr('data-type','close').closest('div.extra').find('> .details').slideToggle();
    }

    /*function dropdown_menu_item(element){
        $(element).find('i.has-sub-menu').toggleClass('fa-chevron-left').toggleClass('fa-chevron-down');
        $(element).parent().find('> ul').toggleClass('active');
    }*/
</script>
</body>
</html>
