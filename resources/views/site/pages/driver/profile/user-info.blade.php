<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="user_page">
    @include('site.parts.user-sidebar')
    <div class="main-content user_info">
        <form action="" id="user_profile_form">
            <div class="padding-around user-info-input">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="first_name">نام</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="" value="{{$user->first_name}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="last_name">نام خانوادگی</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="" value="{{$user->last_name}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="email">ایمیل</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="" value="{{$user->email}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="mobile">شماره همراه</label>
                            <input type="text" class="form-control" name="mobile" id="mobile" placeholder="" value="{{$user->mobile}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 has-padding-45">
                        <div class="row sub-column">
                            <div class="col-md-6">
                                <div class="form-group has-image">
                                    <label for="role_title">تصویر پروفایل</label>
                                    <span class="btn btn-info upload-btn-span">
                                    تغییر تصویر
                                    <input type="file" class="btn-file" name="photo_id" data-id="profile-image-photo_id" id="upload-profile-image-photo_id">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-6 mz-text-left">
                                <a href="javascript:;" class="alert-danger mz-hidden remove_image_profile" onclick="reset_form_element('upload-profile-image-photo_id')" id="remove_image_profile">حذف</a>
                                <img src="{{$user->image}}" alt="" id="profile-image-photo_id" class="image-profile">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="padding-around user-info-input">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="national_id">کد ملی</label>
                            <input type="text" class="form-control" name="national_id" id="national_id" value="{{$user->object()->national_id}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="driver_smart_code">شماره هوشمند راننده</label>
                            <input type="text" class="form-control" name="driver_smart_code" id="driver_smart_code" value="{{$user->object()->smart_code}}">
                        </div>
                    </div>
                </div>

                <div class="row " id="mz_repeater">
{{--                    @forelse(json_decode($user->object()->tels) as $tel)--}}
{{--                        <div class="col-md-6 deleteable">--}}
{{--                            <div class="mb-3">--}}
{{--                                <div class="input-group">--}}
{{--                                    <div class="input-group-prepend">--}}
{{--                                        <span class="input-group-text">--}}
{{--                                            <i class="fas fa-phone"></i>--}}
{{--                                        </span>--}}
{{--                                    </div>--}}
{{--                                    <input type="text" name="tels[]" value="{{$tel}}" aria-label="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">--}}
{{--                                    @if($loop->index>0)--}}
{{--                                        <div class="input-group-append">--}}
{{--                                            <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">--}}
{{--                                                <i class="fas fa-times"></i>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @empty--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="mb-3">--}}
{{--                                <div class="input-group">--}}
{{--                                    <div class="input-group-prepend">--}}
{{--                                                <span class="input-group-text">--}}
{{--                                                    <i class="fas fa-phone"></i>--}}
{{--                                                </span>--}}
{{--                                    </div>--}}
{{--                                    <input type="text" name="tels[]" value="" aria-label="tels" class="form-control form-control-danger valid" placeholder="شماره تلفن را وارد کنید" aria-invalid="false">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endforelse--}}
                </div>

                <div class="row " id="add">
                    <div class="col-md-6">
                        <div id="phone_message_box" class="text-danger mz-hidden">
                            لطفا ابتدا مقادیر قبلی را پر نموده سپس افزودن جدید را انتخاب کنید
                        </div>
                    </div>
                    <div class="col-md-6">
                        <a href="javascript:;" onclick="mz_repeater()" class="btn btn btn-warning pull-left">
                            <i class="fas fa-plus"></i>
                            <span>افزودن شماره تلفن</span>
                        </a>
                    </div>
                </div>

            </div>
            <hr>
            <div class="padding-around user-info-input">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label>نوع خودرو</label>
                            <div class="box-inputs-toggle">
                                @foreach(\App\Models\CarType::all() as $car_type)
                                    <input type="radio" onchange="get_car_feature(this);" {{$data['car_type']==$car_type->id?'checked':''}} name="car_type" value="{{$car_type->id}}" id="car_type_{{$car_type->id}}" />
                                    <label for="car_type_{{$car_type->id}}">{{$car_type->title}}</label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="car_feature_id">ویژگی خودرو</label>
                            <select class="form-control" name="car_feature_id" id="car_feature_id">
                                @foreach(\App\Models\CarFeature::all() as $car_feature)
                                    @if($car_feature->car_type_id==$data['car_type'])
                                        <option value="{{$car_feature->id}}" {{$car_feature->id==$user->object()->car->car_feature_id?'selected':''}}>{{$car_feature->title}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-mb">
                            <label for="car_smart_code">شماره هوشمند خودرو</label>
                            <input type="text" class="form-control" name="car_smart_code" id="car_smart_code" value="{{$data['car']->smart_code}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-mb car_no">
                            <label>پلاک</label>
                            <ul>
                                <li><input type="text" name="car_no[]" class="form-control" value="{{$data['car_no'][0]}}" maxlength="2" aria-label="car_no"></li>
                                <li><input type="text" name="car_no[]" class="form-control" value="{{$data['car_no'][1]}}" maxlength="1" aria-label="car_no"></li>
                                <li><input type="text" name="car_no[]" class="form-control" value="{{$data['car_no'][2]}}" maxlength="3" aria-label="car_no"></li>
                                <li><input type="text" name="car_no[]" class="form-control" value="ایران" aria-label="car_no" disabled></li>
                                <li><input type="text" name="car_no[]" class="form-control" value="{{$data['car_no'][3]}}" maxlength="2" aria-label="car_no"></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center mt-5">
                        {{--<a href="#" class="btn btn-cancel">لغو</a>--}}
                        <a href="javascript:;" onclick="update_user_profile();" class="btn btn-update">ذخیره تغییرات</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

{{--@include('site.parts.social')--}}

{{--@include('site.parts.footer')--}}

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}"></script>
<script>
    let $car_features=@json(\App\Models\CarFeature::all());
    let $car_types=@json(\App\Models\CarType::all());
    let image_element;
    $(document).ready(function () {
        $('.select2').select2({placeholder:'انتخاب کنید'});
        image_element = document.getElementById('upload-profile-image-photo_id');
        image_element.addEventListener('change', onFileUploadChange, false);
    });
    ///////////////////////start for image picker
    let glob_data_id;
    function onFileUploadChange(e) {
        glob_data_id = e.target.getAttribute('data-id');
        let file = e.target.files[0];
        let fr = new FileReader();
        fr.onload = onFileReaderLoad;
        fr.readAsDataURL(file);
    }
    function onFileReaderLoad(e) {
        $(`#${glob_data_id}`).attr('src', e.target.result);
        $("#remove_image_profile").removeClass('mz-hidden');
    }

    function reset_form_element(element) {
        element=$(`#${element}`);
        element.wrap('<form>').parent('form').trigger('reset');
        element.unwrap();
        $(`#${element.attr('data-id')}`).attr('src','{{$user->image}}');
        $("#remove_image_profile").addClass('mz-hidden');
    }
    ///////////////////////end image picker
    ///////////////////////start repeater_item
    function mz_repeater(id=''){
        let inputs = $('#user_profile_form input[name="tels[]"]');
        let flag=false;
        $.each(inputs,function(key,input){
            if($(input).val()==='')
                flag=true;
        });

        if(flag) {
            $("#phone_message_box").removeClass('mz-hidden');
            return flag;
        }
        else
            $("#phone_message_box").addClass('mz-hidden');

        if(id==='validate')
            return flag;

        let col = `<div class="col-md-6 deleteable">
                            <div class="mb-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tels[]" aria-label="tels" class="form-control form-control-danger" placeholder="شماره تلفن را وارد کنید">
                                    <div class="input-group-append">
                                        <a href="javascript:;" onclick="mz_remove_repeater_item(this)" class="btn btn-danger btn-icon">
                                            <i class="fas fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>`;
        $("#mz_repeater").append(col);
    }

    function mz_remove_repeater_item(element){
        $(element).closest('.deleteable').remove();
    }
    ///////////////////////end repeater_item

    function change_sortable_filter(element,filter){
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
    }

    function get_car_feature(element) {
        let car_id = Number($(element).val());
        let options = '<option value=""></option>';
        $.each($car_features, function (key, feature) {
            if (feature['car_type_id'] === car_id)
                options += `<option value="${feature.id}">${feature.title}</option>`;
        });
        $("#car_feature_id").html('').append(options);
    }

    function update_user_profile(){
        if (!validate_user_profile())
            return;
        if (mz_repeater('validate')) {
            return;
        }

        let form =$('#user_profile_form');
        let formData = new FormData(form[0]);

        swal.fire({
            title:'درحال بررسی اطلاعات',
            text: 'لطفا منتظر بمانید',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let url = "{{url('/user/update/profile')}}";
        $.ajax({
            url: url,
            data:formData,
            type: 'post',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
        }).done(function(response){
            swal.close();
            if (response.status === 'success') {
                swal.fire({
                    title: 'به روز رسانی پروفایل!',
                    text: 'عملیات به روز رسانی پروفایل کاربری با موفقیت انجام شد',
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: 'بسیار خب'
                });
                $("#current_password").val('');
                $("#new_password").val('');
                $("#repeat_password").val('');
            }
            if (response.status === 'error') {
                swal.fire({
                    title: 'به روز رسانی پروفایل!',
                    text: response.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonText: 'بسیار خب'
                });
            }
        }).fail(function(error){
            console.log(error);
            swal.close();
        });
    }

    function validate_user_profile(){
        let form =$('#user_profile_form');
        let all_rules={
            rules: {
                current_password: {required: true},
                new_password: {required: true},
                repeat_password:{required:true}
            }
        };
        form.validate(all_rules);
        if (!form.valid())
            return false;
        return true;
    }
</script>
</body>
</html>
