<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
</head>
<body>

@include('site.parts.menu')

<section class="user_page">
    @include('site.parts.user-sidebar')
    <div class="main-content user_info">
        <div class="padding-around user-info-input">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>عنوان بار</th>
                    <th>صاحب بار</th>
                    <th>مبداء</th>
                    <th>مقصد</th>
                    <th>تاریخ بارگیری</th>
                    <th>تاریخ تحویل</th>
                    <th>کرایه</th>
                    <th>جزئیات</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>شیشه <br> سنگ</td>
                    <td>محمود حسینی</td>
                    <td>یزد</td>
                    <td>تهران</td>
                    <td>1398-10-07</td>
                    <td>1398-10-09</td>
                    <td>108,000</td>
                    <td><a href="#"><i class="fas fa-chevron-left"></i></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>شیشه</td>
                    <td>محمود حسینی</td>
                    <td>یزد</td>
                    <td>تهران</td>
                    <td>1398-10-07</td>
                    <td>1398-10-09</td>
                    <td>108,000</td>
                    <td><a href="#"><i class="fas fa-chevron-left"></i></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>شیشه</td>
                    <td>محمود حسینی</td>
                    <td>یزد</td>
                    <td>تهران</td>
                    <td>1398-10-07</td>
                    <td>1398-10-09</td>
                    <td>108,000</td>
                    <td><a href="#"><i class="fas fa-chevron-left"></i></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>شیشه</td>
                    <td>محمود حسینی</td>
                    <td>یزد</td>
                    <td>تهران</td>
                    <td>1398-10-07</td>
                    <td>1398-10-09</td>
                    <td>108,000</td>
                    <td><a href="#"><i class="fas fa-chevron-left"></i></a></td>
                </tr>
                </tbody>
            </table>


            <nav aria-label="Page navigation" class="mt-5">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item active"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </div>
</section>

{{--@include('site.parts.social')--}}

{{--@include('site.parts.footer')--}}

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        let li_s = $('.user_page > aside.sidebar nav li');
        $.each(li_s,function(key,li){
            if($(li).hasClass('active'))
                if($(li).parent().hasClass('dropdown_menu'))
                    $(li).parent().parent().find('> a')[0].click();
        });
    });

    function change_sortable_filter(element,filter){
        $(element).closest('ul').find('li>a.active').removeClass('active');
        $(element).addClass('active');
    }

    function select_bar(){alert('بار انتخاب شد');}

    function more_details_bar_item(element){
        let type = $(element).attr('data-type');
        let close = `بستن <i class="fas fa-chevron-up"></i>`;
        let open = `اطلاعات بیشتر <i class="fas fa-chevron-down"></i>`;
        //let details =
        //$(element).closest('div.extra').find('> .details').slideToggle();
        if(type==='close')
            $(element).html(open).attr('data-type','open').closest('div.extra').find('> .details').slideToggle();
        else
            $(element).html(close).attr('data-type','close').closest('div.extra').find('> .details').slideToggle();
    }

    /*function dropdown_menu_item(element){
        $(element).find('i.has-sub-menu').toggleClass('fa-chevron-left').toggleClass('fa-chevron-down');
        $(element).parent().find('> ul').toggleClass('active');
    }*/
</script>
</body>
</html>
