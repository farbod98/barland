<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('site.layouts.styles')
    <title>{{$page_title}}</title>
    <link rel="stylesheet" href="{{asset('metronic-assets/select2/css/select2.min.css')}}" />
</head>
<body>

@include('site.parts.menu')

@include('site.parts.driver-main-board')

@include('site.parts.cooperation')

@include('site.parts.testimonial')

@include('site.parts.support')

@include('site.parts.work_with_us')

@include('site.parts.download-app')

@include('site.parts.social')

@include('site.parts.footer')

@include('site.parts.footer_nav')

@include('site.layouts.scripts')

<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('site/js/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
<script>
    let features=@json($car_features);
    let asset_url='{{asset('/site/')}}';
    let driver_page_work=$("#driver-page-work");
    let cities=@json(\App\Models\City::all());
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 70) {
            $('nav.top-navigation').addClass('has_scroll');
            $('#site-main-logo').attr('src',`${asset_url}/images/logo-black.png`);
        } else {
            $('nav.top-navigation').removeClass('has_scroll');
            $('#site-main-logo').attr('src',`${asset_url}/images/logo-white.png`);
        }

        let winHeight = $(this).height();
        let scrollTop = $(this).scrollTop();
        let elemHeight = driver_page_work.height();
        let elementTop = driver_page_work.position().top;
        if (elementTop < scrollTop + winHeight && scrollTop < elementTop + elemHeight){
            setTimeout(function () {$("#driver-page-work-step-2").addClass('active');},1500);
            setTimeout(function () {$("#driver-page-work-step-3").addClass('active');},3000);
        }
    });

    let setIntervalHandler;
    $(document).ready(function(){
        $('.select2').select2({placeholder:'انتخاب کنید'});
        /*let box = $(".main-board .box-inputs")[0];
        let box_height = $(box).height();
        let window_height = $(window).height();
        let margin_top = ((window_height - box_height)/2)-70;
        console.log(box_height);
        console.log(window_height);
        console.log(margin_top);
        $(box).css('margin-top',margin_top);*/

        $('.regular').slick({
            speed: 2000,
            // autoplaySpeed: 2000,
            arrows:true,
            autoplay: false,
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: false,
            infinite: true,
            cssEase: 'linear',
            rtl: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        setIntervalHandler = setInterval(function(){ driver_page_support();}, 5000);
    });

    function driver_page_support(element=null){
        let support = $("#driver_page_support");
        let items = support.find('.item');
        let active_item = support.find('.item.active');
        let next_item = active_item.next();

        if(element!==null)
            next_item = $(element);
        else {
            $.each(items, function (key, item) {
                if ($(item).hasClass('active')) {
                    if (key + 1 === items.length) {
                        next_item = items[0];
                        next_item = $(next_item);
                    }
                }
            });
        }

        active_item.removeClass('active');
        $(active_item.find('img')).attr('src',`${asset_url}/images/black-car.png`);
        $(active_item.find('.item-content.active')).removeClass('active');
        $(active_item.find('span')).removeClass('active');

        $(next_item).addClass('active');
        $(next_item.find('img')).attr('src',`${asset_url}/images/white-car.png`);
        $(next_item.find('.item-content')).addClass('active');
        $(next_item.find('span')).addClass('active');
    }

    function set_new_support_item(element){
        // When you want to cancel it:
        element=$(element).closest('.item');
        clearInterval(setIntervalHandler);
        setIntervalHandler = 0; // I just do this so I know I've cleared the interval
        driver_page_support(element);
        //setInterval
        setIntervalHandler = setInterval(function(){ driver_page_support();}, 5000);
    }

    function get_car_feature(element){
        let car_id=Number($(element).val());
        let car_features=[];
        let options='<option value=""></option>';
        $.each(features,function (key,feature) {
            if(feature['car_type_id']===car_id )
                //car_features.push(feature);
                options+=`<option value="${feature.id}">${feature.title}</option>`;
        });

        console.log(car_features);
        let car_feature=$("#car_feature");
        car_feature.html('').append(options);
        // car_feature.select2({placeholder:'انتخاب کنید'});

    }

    function store_new_driver() {
        let form = $("#driver_form_id");
        if (!validate_form())
            return;

        let formData = new FormData(form[0]);
        formData.append('token', "{{csrf_token()}}");
        let number=$('#mobile').val();
        if(number[0]!=='0' || number[1]!=='9' || number.length!==11) {
            swal.fire({
                title: 'عملیات ناموفق',
                text:'شماره نامعتبر است',
                type: "error",
                confirmButtonText: 'بسیار خب',
                confirmButtonClass: "btn btn-brand"
            });
            return;
        }

        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let url ='{{asset('driver')}}';
        $.ajax(url, {
            data: formData,
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                swal.close();
                if(response.status==='success') {
                    window.location = "{{url('/driver')}}";
                }
                if(response.status==='error') {
                    swal.fire({
                        title: 'عملیات ناموفق',
                        text: response.message,
                        type: "error",
                        confirmButtonText: 'بسیار خب',
                        confirmButtonClass: "btn btn-brand"
                    });//.then(function(result) {
                    //if(result.value)
                    //    window.location = "{ {url('/driver')}}";
                    //});
                }
            },
            error: function (data) {
                swal.close();
                let status = 'error';
                let myTitle = 'عملیات ناموفق';
                let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                ToastrMessage(myTitle, message, status);
            }
        });
    }

    function get_bars(){
        let from_state=$("#filter_from_state").val();
        let from_city=$("#filter_from_city").val();
        let to_state=$("#filter_to_state").val();
        let to_city=$("#filter_to_city").val();
        window.location = `{{asset('/bars')}}/start_prov:${from_state};start_city:${from_city};dest_prov:${to_state};dest_city:${to_city}`;
    }

    function validate_form(){
        let form = $("#driver_form_id");
        let all_rules={rules:{
                first_name: {required: true},
                last_name: {required: true},
                mobile: {required: true},
                car_type: {required: true},
                car_feature_id: {required: true},
                accept_rules: {required: true},
            }};
        form.validate(all_rules);
        if (!form.valid()) {
            console.log('not valid');
            return false;
        }
        return true;
    }

    function change_cities(element,type) {
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });

        let province=$(element).val();
        let data='<option value=""></option>';
        $.each(cities,function (key,city) {
            if(Number(city.province_id)===Number(province))
                data+=`<option value="${city.id}">${city.title}</option>`;
        });
        $(`#filter_${type}_city`).html('').append(data);
        swal.close();
    }
</script>
</body>
</html>
