@extends('master-empty')

@section('style')
    <style>
        .kt-login.kt-login--v3 .kt-login__wrapper .kt-login__container {border: 1px solid;padding: 15px;border-radius: 5px;}
    </style>
    <link href="{{asset('custom-assets/pages/lock/css/lock.min.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('content')
    <!-- begin:: Page -->
    <div class="page-lock">
        <div class="page-logo">
            <a class="brand" href="/">
                <img src="{{asset('custom-assets/pages/lock/img/logo-big.png')}}" alt="logo" /> </a>
        </div>
        <div class="page-body">
            <div class="lock-head"> @lang(__('login/lock_page.title')) </div>
            <div class="lock-body">
                <div class="lock-cont">
                    <div class="lock-item">
                        <div class="pull-left lock-avatar-block">
                            <img src="{{asset('custom-assets/pages/lock/img/photo3.jpg')}}" class="lock-avatar" alt=""> </div>
                    </div>
                    <div class="lock-item lock-item-full">
                        <form class="lock-form pull-left" action="/" method="post">
                            <h4>{{auth()->user()->name}}</h4>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix" id="password" type="password" autocomplete="off" placeholder="@lang(__('login/lock_page.password'))" name="password" /> </div>
                            <div class="form-actions">
                                <button type="button" class="btn red uppercase" onclick="javascript:unlock_user();">@lang(__('login/lock_page.btn'))</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="lock-bottom">
                <a href="{{url('/logout')}}">
                    @lang(__('login/lock_page.before_name')) {{auth()->user()->name}} @lang(__('login/lock_page.after_name'))
                </a>
            </div>
        </div>
        <div class="page-footer-custom"> 2019 &copy; ERP. Admin Dashboard IdeGostaran.co </div>
    </div>

    <div id="app"></div>
    <!-- end:: Page -->
@endsection


@section('script')
    <script>
        let toastr_success='{{__('general.toastr_success')}}';
        let toastr_error='{{__('general.toastr_error')}}';
        function unlock_user(){
            let password = $("#password").val();
            if(password===''){
                let status = 'error';
                let myTitle = toastr_error;
                let message = 'رمز عبور نمیتواند خالی باشد';
                ToastrMessage(myTitle, message, status);
            }else {
                $.ajax({
                    url: "{{url('/unlock')}}",
                    type: 'post',
                    data: {password:password},
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}',},
                    dataType: 'json',
                }).done(function(response){
                    let status,myTitle;
                    if (response.status === 'success') {
                        status = 'success';
                        myTitle = toastr_success;
                        window.location = '/';
                    } else {
                        status = 'error';
                        myTitle = toastr_error;
                    }
                    let message = response.message;
                    ToastrMessage(myTitle, message, status);
                }).fail(function(response){
                    let status = 'error';
                    let myTitle = toastr_error;
                    let message = 'عملیات بامشکل مواجه شد. لطفا با پشتیبانی تماس حاصل فرمایید.';
                    ToastrMessage(myTitle, message, status);
                });
            }
        }
    </script>
@endsection