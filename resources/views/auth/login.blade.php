@extends('master-empty')

@section('style')
    <link href="{{asset('custom-assets/pages/login/css/login-3.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .kt-login.kt-login--v3 .kt-login__wrapper .kt-login__container {border: 1px solid;padding: 15px;border-radius: 5px;}
        .captcha-mt{margin-top: 20px;}
    </style>
@endsection

@section('content')

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{asset('metronic-assets/media/bg/bg-3.jpg')}});">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img alt="logo" src="{{asset('metronic-assets/media/logos/logo-5.png')}}">
                            </a>
                        </div>

                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">@lang(__('login/login_page.login_title'))</h3>
                            </div>
                            <form class="kt-form" action="" id="kt-login__signin">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="@lang(__('login/login_page.username'))" name="username" id="login_username" autocomplete="off">
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="@lang(__('login/login_page.password'))" name="password" id="login_password">
                                </div>
                                <div class="row kt-login__extra">
                                    <div class="col">
                                        <label class="kt-checkbox">
                                            <input type="checkbox" name="remember" id="remember">@lang(__('login/login_page.remember_me'))
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col kt-align-right">
                                        <a href="javascript:;" id="kt_login_forgot" class="kt-login__link">@lang(__('login/login_page.forget_password'))</a>
                                    </div>
                                </div>
                                <div id="captcha_login_place_holder"></div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">@lang(__('login/login_page.login_btn'))</button>
                                </div>
                            </form>

                        </div>
                        <div class="kt-login__signup">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">@lang(__('login/login_page.sign_up_title'))</h3>
                                <div class="kt-login__desc">@lang(__('login/login_page.sign_up_description'))</div>
                            </div>
                            <form class="kt-form" action="">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="@lang(__('login/login_page.full_name'))" name="fullname">
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="@lang(__('login/login_page.email'))" name="email" autocomplete="off">
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="@lang(__('login/login_page.password'))" name="password">
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="@lang(__('login/login_page.r_password'))" name="rpassword">
                                </div>
                                <div class="row kt-login__extra">
                                    <div class="col kt-align-left">
                                        <label class="kt-checkbox">
                                            <input type="checkbox" name="agree"> @lang(__('login/login_page.agree_terms.0')) <a href="#" class="kt-link kt-login__link kt-font-bold"> @lang(__('login/login_page.agree_terms.1')) </a>  @lang(__('login/login_page.agree_terms.2'))
                                            <span></span>
                                        </label>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                                <div id="captcha_register_place_holder"></div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_signup_cancel" class="btn btn-light btn-elevate kt-login__btn-secondary">@lang(__('login/login_page.cancel_btn'))</button>
                                    <button id="kt_login_signup_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">@lang(__('login/login_page.sign_up_btn'))</button>&nbsp;&nbsp;
                                </div>
                            </form>
                        </div>

                        <div class="kt-login__forgot">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">@lang(__('login/login_page.forget_title'))</h3>
                                <div class="kt-login__desc">@lang(__('login/login_page.forget_description'))</div>
                            </div>
                            <form class="kt-form" action="">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="@lang(__('login/login_page.email'))" name="email" id="kt_email" autocomplete="off">
                                </div>
                                <div id="captcha_forget_place_holder"></div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_forgot_cancel" class="btn btn-light btn-elevate kt-login__btn-secondary">@lang(__('login/login_page.cancel_btn'))</button>
                                    <button id="kt_login_forgot_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">@lang(__('login/login_page.forget_request'))</button>&nbsp;&nbsp;
                                </div>
                            </form>
                        </div>

                        {{--<div class="kt-login__account">
								<span class="kt-login__account-msg">
									@lang(__('login/login_page.no_sign_up_yet'))
								</span>
                            &nbsp;&nbsp;
                            <a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">@lang(__('login/login_page.sign_up_link'))</a>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="app"></div>
    <!-- end:: Page -->
@endsection



@section('script')
    <script>
        let asset_url="{{url('/')}}";
        let login_url="{{asset('/login')}}";
        let forget_url="{{asset('/password/email')}}";
        let attempt_url="{{asset('/login_attempt_count')}}";
        let csrf_token="{{csrf_token()}}";
        let toastr_success='{{__('general.toastr_success')}}';
        let toastr_error='{{__('general.toastr_error')}}';
        let captcha_label='{{__('general.captcha_label')}}';
        let USER_LOG_COUNT = 50;
        let global_user_log = Number('{{login_attempt_count('login')}}');

        // ToastrMessage('myTitle','message','success');
        // ToastrMessage('myTitle','message','info');
        // ToastrMessage('myTitle','message','error');
        // ToastrMessage('myTitle','message','warning');
        async function global_modal_user_getAjax(url,type,data,btn=null) {
            data['ajax'] = true;
            data['token'] = csrf_token;
            let result;
            try {
                result = await $.ajax({
                    url: url,
                    type: type,
                    data: data,
                    headers: {'X-CSRF-TOKEN': csrf_token,},
                    dataType: 'json',
                });

                if (result.status === 'error' || result.status == 'error') {
                    let status = 'error';
                    ToastrMessage(toastr_error, result.message, status);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data['captcha'] !== undefined)
                        refreshCaptcha('_global_modal_login');
                }
                return result;
            } catch (error) {
                let message = '';
                if (error.responseJSON) {
                    $.each(error.responseJSON.errors, function (key, error) {
                        message = error;
                    });
                }
                let status = 'error';
                ToastrMessage(toastr_error, message, status);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                return error;
            }
        }

        function refreshCaptcha(id) {
            let captcha = $(`#captcha_image${id}`);
            captcha.attr('src', captcha.attr('src') + '{{ captcha_src() }}');
        }

        function get_login_attempt_count(response,type){
            if(response.attempt===true){
                global_user_log = Number(response.data);
                let captcha_holder=$(`#captcha_${type}_place_holder`);
                if(global_user_log >= USER_LOG_COUNT){
                    captcha_holder.html('');
                    captcha_holder.append(set_captcha(type));
                }
            }
        }

        $(document).ready(function () {
            global_modal_user_getAjax(attempt_url,'post',{type:'login'}).then( (response) => {
                get_login_attempt_count(response,'login');
            });
        });

        function check_captcha(type){
            let captcha,response={};
            response['captcha']='';response['rule']= false;
            let captcha_holder=$(`#captcha_${type}_place_holder`);
            if(captcha_holder.html()!==''){
                captcha = $(`#${type}_captcha_text`).val();
                response['captcha']= captcha;
                response['rule']= true;
            }
            return response;
        }

        function set_captcha(type){
            return `<div class="captcha">
                        <div class="col-md-12 col-sm-12 col-xs-12 captcha-p-t">
                            <span onclick="javascript:refreshCaptcha('_${type}')">
                            <img src="{{asset('custom-assets/images/refresh.png')}}" alt="refresh"></span>
                            <img id="captcha_image_${type}" src="{{captcha_src()}}" alt="captcha">
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 captcha-mt">
                            <label for="captcha">${captcha_label}</label>
                            <input id="${type}_captcha_text" maxlength="5" name="captcha" type="text" class="">
                        </div>
                    </div>`;
        }

    </script>
    <script src="{{asset('metronic-assets/jquery-validation/jquery.validate-fa.js')}}" type="text/javascript"></script>
    <script src="{{asset('custom-assets/pages/login/js/login-general.js')}}" type="text/javascript"></script>
@endsection
