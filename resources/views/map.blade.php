<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.0/dist/leaflet.css"/>
    <link rel="stylesheet" href="{{asset('leaflet/src/leaflet-search.css')}}"/>
    <link rel="stylesheet" href="{{asset('leaflet/src/style.css')}}"/>
</head>

<body>

<div id="mapid" style="height: 600px"></div>

<form action="javascript:submit('1','1');">
    <div class="form-group">
        <input id="lat"></input>
    </div>
    <div class="form-group">
        <input id="long"></input>
    </div>
    <div class="form-group">
        <input id="map_address"></input>
    </div>
    <div class="form-group">
        <input id="address"></input>
    </div>
    <div class="form-group">
        <input id="title"></input>
    </div>


    <input type="submit" value="value">
</form>
<script src="https://unpkg.com/leaflet@1.3.0/dist/leaflet.js"></script>
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="{{asset('leaflet/src/leaflet-search.js')}}"></script>
<script>
    {{--var address =@json($address);--}}
    var mymap = L.map('mapid').setView([1, 1], 5);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZmFyYm9kOTgiLCJhIjoiY2syb2doenhpMHhqaDNjbmw1aDVxY2lmdSJ9.QHlIU9_JK48f8VcJ0daKhQ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZmFyYm9kOTgiLCJhIjoiY2syb2doenhpMHhqaDNjbmw1aDVxY2lmdSJ9.QHlIU9_JK48f8VcJ0daKhQ    '
    }).addTo(mymap);
    // var marker = L.marker([address.lat, address.long], {
    //     draggable: 'true'
    // }).addTo(mymap);


    mymap.addControl(new L.Control.Search({
        url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
        jsonpParam: 'json_callback',
        propertyName: 'display_name',
        propertyLoc: ['lat', 'lon'],
        marker: marker,
        autoCollapse: true,
        autoType: false,
        minLength: 2,
        zoom: 8
    }));

    marker.on('dragend', function (e) {
        let lng = marker.getLatLng().lng;
        let lat = marker.getLatLng().lat;
        $.ajax({
            url: 'https://nominatim.openstreetmap.org/reverse',
            data: {
                format: 'jsonv2',
                lat: lat,
                lon: lng,
            },
            success: function (response) {
                $("#lat").val(lat);
                $("#long").val(lng);
                $("#map_address").val(response.display_name);

                console.log(response);
            }


        });
    });
    mymap.on('click', function (e) {
        marker.setLatLng([e.latlng.lat, e.latlng.lng]);

        let lng = marker.getLatLng().lng;
        let lat = marker.getLatLng().lat;
        $.ajax({
            url: 'https://nominatim.openstreetmap.org/reverse',
            data: {
                format: 'jsonv2',
                lat: lat,
                lon: lng,
            },
            success: function (response) {
                $("#lat").val(lat);
                $("#long").val(lng);
                $("#map_address").val(response.display_name);

                console.log(response);
            }

        });
    });


    mymap.addControl(new L.Control.Search({sourceData: searchByAjax, text: 'Color...', markerLocation: true}));

    function submit(id, action) {
        let url = `/admin/customer_address`;
        let method = 'post';
        if (action === 'edit') {
            url = `/admin/customer_address/${id}`;
            method = 'put';
        }
        let address = $("#address").val();
        let title = $("#title").val();
        $.ajax({
            method: method,
            url: url,
            data: {
                lat: marker.getLatLng().lat,
                long: marker.getLatLng().lng,
                address: address,
                title: title,
                city_id: 1
            },
            success: function (response) {
                alert('done');
            }


        });
    }


</script>

<div id="copy"><a href="http://labs.easyblog.it/">Labs</a> &bull; <a rel="author"
                                                                     href="http://labs.easyblog.it/stefano-cudini/">Stefano
        Cudini</a></div>

{{--<script type="text/javascript" src="/labs-common.js"></script>--}}

</body>
</html>
