<!DOCTYPE html>
<html lang="fa" direction="rtl" dir="rtl" style="direction: rtl">
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>Metronic | Master Page</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('metronic-assets/media/logos/favicon.ico')}}" />
    @yield('before-styles')
    @include('master-parts.global-styles')
    @yield('style')
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
    <!-- begin:: Page -->

    <!-- begin:: Header Mobile -->
    @include('master-parts.header-mobile')
    <!-- end:: Header Mobile -->

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <!-- begin:: Aside -->
            @include('master-parts.sidebar')
            <!-- end:: Aside -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- begin:: Header -->
                @include('master-parts.top-header')
                <!-- end:: Header -->
                @include('master-parts.content')
            </div>
        </div>
    </div>

    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->
    @include('master-parts.global-scripts')
    @yield('script')
</body>
<!-- end::Body -->
</html>




