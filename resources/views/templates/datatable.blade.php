@extends('master')
@section('before-styles')
{{--    <link href="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('style')
    <style>
        /*Style Section*/
    </style>
@endsection
@section('content')
   @yield('datatable')
@endsection
@section('script')
    <script src="{{asset('custom-assets/pages/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
{{--    <script src="{{asset('custom-assets/pages/components/datatables/scrollable.js')}}" type="text/javascript"></script>--}}
    <script>
        /*Custom Scripts Section*/
        $(document).ready(function(){
            dataTablesBasicScrollable();
        });

        function dataTablesBasicScrollable() {
            let data_tables = $(".mz-multi-data-table");
            $.each(data_tables,function(key,table){
                $(table).DataTable({
                    scrollY: '50vh',
                    scrollX: true,
                    scrollCollapse: true,
                });
            });
        }
    </script>
@endsection
