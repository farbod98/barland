<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {"brand": "#5d78ff", "dark": "#282a3c", "light": "#ffffff", "primary": "#5867dd", "success": "#34bfa3", "info": "#36a3f7", "warning": "#ffb822", "danger": "#fd3995"},
            "base": {"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"], "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]}
        }
    };
</script>
<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="{{asset('metronic-assets/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/popper.js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

{{--<script src="{{asset('custom-assets/pages/components/jquery-ui/jquery-ui.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/js-cookie/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/moment/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/tooltip.js/tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/perfect-scrollbar/js/perfect-scrollbar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/sticky-js/sticky.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/wnumb/wNumb.js')}}" type="text/javascript"></script>--}}




<!--end:: Global Mandatory Vendors -->
@yield('before-scripts')
<!--begin::Global Theme Bundle(used by all pages) -->
{{--<script src="{{asset('mix-assets/js/app.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('mix-assets/js/all.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-session-timeout/bootstrap-session-timeout.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    let sessionTimeout_title='{{__('general.sessionTimeout_title')}}';
    let sessionTimeout_message='{{__('general.sessionTimeout_message')}}';
    let sessionTimeout_timer_0='{{__('general.sessionTimeout_timer.0')}}';
    let sessionTimeout_timer_1='{{__('general.sessionTimeout_timer.1')}}';
    let sessionTimeout_logoutButton='{{__('general.sessionTimeout_logoutButton')}}';
    let sessionTimeout_keepAliveButton='{{__('general.sessionTimeout_keepAliveButton')}}';
    function global_search_field(element){
        let search_text = $(element).val();
        let items_place = $("#all_page_items_place");
        let items = items_place.find('.col-md-3.col-sm-3');
        if(search_text.length>=2){
            $.each(global_search_array,function(k,i) {
                $.each(items,function(key,item) {
                    let title = $.trim($(item).find(i).text());
                    if (title.indexOf(search_text) === -1) {
                        $(item).addClass('hidden-field');
                    }
                    else {
                        $(item).removeClass('hidden-field');
                    }
                });
            });
        }
        else if(search_text.length===0){
            $.each(items,function(key,item){
                $(item).removeClass('hidden-field');
            });
        }
    }

    async function mz_global_async_ajax(url,type,data){
        swal.fire({
            title: 'لطفا منتظر بمانید',
            text:'درحال دریافت اطلاعات',
            allowOutsideClick:false,
            onOpen: function() {
                swal.showLoading()
            }
        });
        data['token']="{{csrf_token()}}";
        let result;
        try {
            result = await $.ajax({
                url: url,
                type: type,
                data: data,
                headers: {'X-CSRF-TOKEN': "{{csrf_token()}}",},
                dataType: 'json',
            });
            if(result.status==='error' || result.status=='error'){
                //let status = 'error';
                let myTitle = 'عملیات ناموفق';
                //ToastrMessage(myTitle, result.message, status);
                swal.fire({
                    title: myTitle,
                    text: result.message,
                    type: "error",
                    confirmButtonText: 'بسیار خب',
                    confirmButtonClass: "btn btn-brand"
                });
            }
            return result;
        } catch (error) {
            swal.close();
            let message='';
            if(error.responseJSON) {
                $.each(error.responseJSON.errors, function (key, error) {
                    message = error;
                });
            }
            let status = 'error';
            let myTitle = 'عملیات ناموفق';
            ToastrMessage(myTitle, message, status);
            return error;
            //console.error(error);
        }
    }
</script>
{{--<script src="{{asset('custom-assets/pages/components/utils/session-timeout.js')}}" type="text/javascript"></script>--}}


{{--<script src="{{asset('js/app.js')}}" type="text/javascript"></script>--}}
<!--end::Global Theme Bundle -->

