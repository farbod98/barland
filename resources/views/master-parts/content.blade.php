<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
    <!-- begin:: Subheader -->
    @include('master-parts.sub-sections.content.subheader-content')
    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    @include('master-parts.sub-sections.content.main-content')
    <!-- end:: Content -->

    <!-- begin:: Footer -->
    @include('master-parts.sub-sections.content.footer-content')
    <!-- end:: Footer -->
</div>