<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

    @include('master-parts.sub-sections.top-header.right-menu')
    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    @include('master-parts.sub-sections.top-header.left-menu')
    <!-- end:: Header Topbar -->
</div>