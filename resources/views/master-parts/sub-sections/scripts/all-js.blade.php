<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="{{asset('metronic-assets/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/popper.js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/js-cookie/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/moment/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/tooltip.js/tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/perfect-scrollbar/js/perfect-scrollbar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/sticky-js/sticky.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/wnumb/wNumb.js')}}" type="text/javascript"></script>
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="{{asset('metronic-assets/jquery-form/jquery.form.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/bootstrap-datepicker.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/bootstrap-timepicker.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-daterangepicker/js/daterangepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-maxlength/src/bootstrap-maxlength.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/bootstrap-switch.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/ion-rangeslider/js/ion.rangeSlider.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/typeahead.js/dist/typeahead.bundle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/handlebars/handlebars.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/inputmask/js/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/inputmask/js/inputmask/inputmask.date.extensions.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/inputmask/js/inputmask/inputmask.numeric.extensions.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/nouislider/js/nouislider.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/owl.carousel/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/autosize/autosize.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/clipboard/clipboard.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/dropzone/js/dropzone.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/summernote/js/summernote.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/markdown/markdown.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-markdown/js/bootstrap-markdown.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/bootstrap-markdown.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-notify/bootstrap-notify.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/bootstrap-notify.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/jquery-validation/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/jquery-validation/additional-methods.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/jquery-validation.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/toastr/js/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/raphael/raphael.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/morris.js/js/morris.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/chart.js/js/Chart.bundle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/bootstrap-session-timeout/bootstrap-session-timeout.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/jquery-idletimer/idle-timer.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/waypoints/jquery.waypoints.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/es6-promise-polyfill/promise.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/custom-js/sweetalert2.init.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/jquery.repeater/lib.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/jquery.repeater/jquery.input.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/jquery.repeater/repeater.js')}}" type="text/javascript"></script>
<script src="{{asset('metronic-assets/dompurify/purify.min.js')}}" type="text/javascript"></script>
<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('assets/js/demo12/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="{{asset('metronic-assets/fullcalendar/js/fullcalendar.bundle.min.js')}}" type="text/javascript"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
<script src="{{asset('metronic-assets/gmaps/gmaps.min.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/demo12/pages/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Scripts -->