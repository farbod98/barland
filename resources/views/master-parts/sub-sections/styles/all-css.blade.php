<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{asset('metronic-assets/fullcalendar/css/fullcalendar.bundle.rtl.min.css')}}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->

<!--begin:: Global Mandatory Vendors -->
<link href="{{asset('metronic-assets/perfect-scrollbar/css/perfect-scrollbar.min.rtl.css')}}" rel="stylesheet" type="text/css" />
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<link href="{{asset('metronic-assets/tether/css/tether.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-timepicker/css/bootstrap-timepicker.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-daterangepicker/css/daterangepicker.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-select/css/bootstrap-select.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/ion-rangeslider/css/ion.rangeSlider.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/nouislider/css/nouislider.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/owl.carousel/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/owl.carousel/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/dropzone/css/dropzone.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/summernote/css/summernote.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/bootstrap-markdown/css/bootstrap-markdown.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/animate.css/animate.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/toastr/css/toastr.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/morris.js/css/morris.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/sweetalert2/css/sweetalert2.min.rtl.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('metronic-assets/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css"/>
<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{asset('assets/css/demo12/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="{{asset('assets/css/demo12/skins/header/base/dark.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/demo12/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/demo12/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/demo12/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />
<!--end::Layout Skins -->
<link rel="shortcut icon" href="{{asset('metronic-assets/media/logos/favicon.ico')}}" />