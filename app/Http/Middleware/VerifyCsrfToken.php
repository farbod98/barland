<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'customer/*',
        'customer/',
        'receiver/*',
        'product/*',
        'car/*',
        'price',
        'price/*',
        'customer_address',
        'customer_address/*',
        'order',
        'order/*',
        'receiver',
        'car',
        'product',
        'car_type',
        'car_type/*',
        'driver',
        'driver/*',
        'admin/*',
        '*'
        ];
}
