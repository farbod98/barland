<?php

namespace App\Http\Middleware;

use Closure;

class IsCustomer
{
    public function handle($request,Closure $next){

        if(!auth()->user())
            return redirect(404);
        if(auth()->user()->role_id==2)
            return $next($request);
        else
            return redirect(404);
    }
}
