<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    public function handle($request,Closure $next){

        if(!auth()->user())
            return redirect(404);
      $not=['customer','driver'];
        if(!in_array(auth()->user()->role->slug,$not))
            return $next($request);
        else
            return redirect(404);
    }
}
