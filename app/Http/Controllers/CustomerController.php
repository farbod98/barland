<?php

namespace App\Http\Controllers;

use App\Models\CarFeature;
use App\Models\CarType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'بارلند | صاحب بار ';
        $page_type='customer';
        $car_types=CarType::all();
        $car_features=CarFeature::all();
        session(['type'=>'customer']);
        if(session('redirect_login')) {
            session()->forget('redirect_login');
            return redirect(url('/login/' . session('type')));
        }
        return view('site.pages.customer.index',compact('page_title','page_type','car_features','car_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $user=Auth::loginUsingId(1);
        $user=Auth::user();
        $new_customer=$request->except('_token');
        $new_customer['tels']=json_encode($new_customer['tels']);
        $new_customer+=['user_id'=>$user->id,'active'=>0];
        $customer=Customer::create($new_customer);
        if ($request->ajax())
            return response()->json(['status'=>'success','data'=>$customer]);
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer=Customer::find($id);
        $prepends=$this->get_prepends();
        return response()->json(['status'=>'success','data'=>$customer,'prepends'=>$prepends]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
//        $user=Auth::loginUsingId(2);
        $input=$request->except('_token');
        $customer=Customer::find($id);
        $customer->update($input);
        return response()->json(['status'=>'success','data'=>$customer]);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Customer::find($user->customer->id)->update(['active'=>0]);
    }

    public function status($id)
    {
        $customer=Customer::find($id);
        if($customer->active)
            $customer->update(['active'=>0]);
        else
            $customer->update(['active'=>1]);
        return response()->json(['status'=>'success','data'=>$customer]);
    }

    public function get_prepends()
    {
        return['اقای','خانم','شرکت'];
    }

    public function prepends()
    {
        return response()->json(['status'=>'success','prepends'=>['آقای','خانم','شرکت']]);

    }

}
