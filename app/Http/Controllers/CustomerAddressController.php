<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CustomerAddress;
use Illuminate\Support\Facades\Auth;


class CustomerAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $input = create_real_address($input);
        $address=CustomerAddress::create($input);
        if($address) {
            $address->city;
            $address->city->province;
            return response()->json(['status' => 'success', 'address' => $address]);
        }
        return response()->json(['status'=>'error','message'=>'خطا در ثبت آدرس']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $address = CustomerAddress::find($id);
        $input=$request->all();
        $input = create_real_address($input);
        $address->update($input);
        if($address) {
            $address->city;
            $address->city->province;
            return response()->json(['status' => 'success', 'address' => $address]);
        }
        return response()->json(['status'=>'error','message'=>'خطا در به روز رسانی آدرس']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustomerAddress::find($id)->update(['active'=>0]);
    }
}
