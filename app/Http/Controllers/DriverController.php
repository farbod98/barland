<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarFeature;
use App\Models\CarType;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class DriverController extends Controller
{
    public function index()
    {
        $page_title = 'بارلند | راننده ';
        $page_type='driver';
        $car_types=CarType::all();
        $car_features=CarFeature::all();
        session(['type'=>'driver']);
        if(session('redirect_login')) {
            session()->forget('redirect_login');
            return redirect(url('/login/' . session('type')));
        }
        return view('site.pages.driver.index' ,compact('page_title','page_type','car_types','car_features'));
    }
    public function update(Request $request,$id)
    {
//        $user=Auth::loginUsingId(2);
        $input=$request->except('_token');
        $driver=Driver::find($id);
        $driver->update($input);
        return response()->json(['status'=>'success','data'=>$driver]);
    }
    public function edit($id)
    {
        $driver=Driver::find($id);
        $cars=CarType::all();
        return response()->json(['status'=>'success','data'=>$driver,'car_types'=>$cars]);
    }

    public function store(Request $request)
    {
        $mobile=$request->get('mobile');
        $car_fields=$request->only('car_feature_id');
        $user_fields=$request->only(['first_name','last_name','mobile']);
        $user=User::where('mobile',$mobile)->first();
        if($user)
            return response()->json(['status'=>'error','message'=>'این شماره همراه از قبل در سیستم موجود است']);

        $car=Car::create($car_fields);
        $driver=Driver::create(['car_id'=>$car->id]);
        $password=rand(10000,99999);
        //sms password
        $role=Role::where('slug','driver')->first()->id;
        $hash=bcrypt($password);
        //session(['pass'=>$password,'hash'=>$hash]);
        $user_fields+=['password'=>$hash,'code'=>$hash,'role_id'=>$role,'object_id'=>$driver->id];
        $user=User::create($user_fields);
        auth()->loginUsingId($user->id);
        return response()->json(['status'=>'success','message'=>'حساب کاربری با موفقیت ایجاد گردید']);
    }

    public function login(Request $request)
    {

    }
}
