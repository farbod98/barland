<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Receiver;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

class ReceiverController extends Controller
{
    public function store(Request $request)
    {
        $input=$request->all();
        $input = create_real_address($input);
        $address=Receiver::create($input);
        if($address) {
            $address->city;
            $address->city->province;
            return response()->json(['status' => 'success', 'address' => $address]);
        }
        return response()->json(['status'=>'error','message'=>'خطا در ثبت آدرس']);
    }

    public function destroy($id)
    {
        Receiver::find($id)->update(['active'=>0]);
    }

    public function update(Request $request,$id)
    {
        $input=$request->all();
        $address=Receiver::find($id);
        $input = create_real_address($input);
        $address->update($input);
        if($address) {
            $address->city;
            $address->city->province;
            return response()->json(['status' => 'success', 'address' => $address]);
        }
        return response()->json(['status'=>'error','message'=>'خطا در به روز رسانی آدرس']);
    }

    public function edit($id)
    {
        $receiver=Receiver::find($id);
        return response()->json(['status'=>'success','data'=>$receiver]);

    }
}
