<?php

namespace App\Http\Controllers;


use App\Models\CarFeature;
use App\Models\CarType;
use App\Models\City;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Packing;
use App\Models\Price;
use App\Models\Province;
use App\Models\Receiver;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\Driver;
use Illuminate\Http\Response as ResponseAlias;


class OrderController extends Controller
{
    public function index(Request $request){
        $customer=auth()->user()->object();
        $data=[];
        $data['car_type']=1;
        $data['from_city']='';
        $data['to_city']='';
        if(count($request->all()) > 0){
            $data['car_type']=$request['send_form_order_car_type'];
            $data['from_city']=$request['send_form_order_from'];
            $data['to_city']=$request['send_form_order_to'];
        }
        $page_title='بارلند | درخواست بار جدید';
        $page_type='customer';
        $products=Product::all();
        $car_types=CarType::all();
        $car_features=CarFeature::all();
        $cities=City::all();
        $provinces=Province::get_accepted_provinces();
        $packings=Packing::all();
        //dd($customer);
        $receivers=Receiver::where('customer_id',$customer->id)->get();
        return view('site.pages.customer.order',compact('page_type','page_title','data','products','car_types','car_features','cities','packings','customer','provinces','receivers'));
    }

    public function bars($filters=null){
        $selected_data=[];
        $selected_data['from_state']='';
        $selected_data['from_city']=['selected'=>'','options'=>[]];
        $selected_data['to_state']='';
        $selected_data['to_city']=['selected'=>'','options'=>[]];
        if($filters!=null){
            $filters=explode(';',$filters);
            $data=[];
            foreach ($filters as $filter) {
                $temp = explode(':', $filter);
                $data[$temp[0]] = $temp[1];
            }
            $selected_data['from_state']=isset($data['start_prov'])?$data['start_prov']:'';
            $selected_data['from_city']=[
                'selected'=>isset($data['start_city'])?$data['start_city']:'',
                'options'=>$selected_data['from_state']!=''?City::where('province_id',$selected_data['from_state'])->get(['id','title']):[]];
                //'options'=>$selected_data['from_state']!=''?Province::find($selected_data['from_state'])->cities:[]];
            $selected_data['to_state']=isset($data['dest_prov'])?$data['dest_prov']:'';
            $selected_data['to_city']=[
                'selected'=>isset($data['dest_city'])?$data['dest_city']:'',
                'options'=>$selected_data['to_state']!=''?City::where('province_id',$selected_data['to_state'])->get(['id','title']):[]];
                //'options'=>$selected_data['to_state']!=''?Province::find($selected_data['to_state'])->cities:[]];
        }
        $bars=Order::where('driver_id',0)->get();
        $page_title='بارلند | همه بار ها';
        $page_type='driver';
        return view('site.pages.driver.bars',compact('page_type','page_title','bars','filters','selected_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return ResponseAlias
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return ResponseAlias
     */
    public function store(Request $request)
    {

        $input = $this->get_order_fields($request->all());
        $order = Order::create($input);
     //   dd($order);
        $order->car_features()->sync($request['step_one_bar_car_details']);
        if($order)
            return response()->json(['status'=>'success','data'=>'']);
        return response()->json(['status'=>'error','message'=>'خطا در ثبت درخواست']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ResponseAlias
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return ResponseAlias
     */
    public function edit($id)
    {
        $order=Order::find($id);
        $features=$order->car_features;
        $order->car_type=$order->car_features[0]->car_type->title;
        //$order->car_type=$order->car_feature->car_type->title;
        $order->owner=$order->customer->user->name;
        return response()->json(['status'=>'success','data'=>$order]);

//        $order=Order::find($id);
//        $products=Product::all();
//        $drivers=Driver::all();
//        return response()->json(['status'=>'success','data'=>$order,'products'=>$products,'drivers'=>$drivers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return ResponseAlias
     */
    public function update(Request $request, $id)
    {
        $update_order=$request->except('_token');
        $order=Order::find($id);
        $order->update($update_order);
        return response()->json(['status'=>'success','data'=>$order]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        Order::find($id)->update(['active'=>0]);
    }

    public function get_address(Request $request){
        $input = $request->all();
        $address = get_address_details($input);
        return response()->json(['status'=>'success','data'=>$address,'message'=>'']);
    }

    public function edit_address(Request $request){
        $input = $request->all();
        $address = get_address_details($input);
        return response()->json(['status'=>'success','data'=>$address,'message'=>'']);
    }

    public function get_all_address_request(Request $request){
        $input['sender'] = CustomerAddress::where('id',$request['sender'])->first();
        $input['receiver'] = Receiver::where('id',$request['receiver'])->first();
        return response()->json(['status'=>'success','data'=>$input,'message'=>'']);
    }

    public function calculate_way_price(Request $request){
        $input = $request->all();
        $input['from'] = CustomerAddress::where('id',$request['sender'])->first()->city_id;
        $input['to'] = Receiver::where('id',$request['receiver'])->first()->city_id;
        $input['calculate']=true;
        //dd($input);
        $input = $this->inquiry($input);
        //$input['average_request']='1,500,000';
        //$input['tariff_request']='500,000';
        return response()->json(['status'=>'success','data'=>$input,'message'=>'']);
    }

    public function get_ten_last_request(Request $request){
        $input = $request->all();
        $input['calculate']=false;
        $input = $this->inquiry($input);
        //$input['average_request']='1,500,000';
        //$input['tariff_request']='500,000';
        return response()->json(['status'=>'success','data'=>$input,'message'=>'']);
        //return response()->json(['status'=>'error','data'=>'','message'=>'خطای استعلام قیمت']);
    }

    public function inquiry($input)
    {
        //dd($input);
        //$input['from']=429;
        //$input['to']=438;
        //$input['car_type']=1;
        //$input['calculate']=true;
        $end=City::where('id',$input['to'])->first();
        $price=Price::where('start_city',$input['from'])->where('end_city',$end->id)->where('car_id',$input['car_type'])->first();
       if($price){
           $tariff_request=$price->price;
           if($input['calculate'])
           {
               if($input['weight']>22)
                   $tariff_request=$tariff_request/22*$input['weight'];
               $insurance=calculate_insurance($input['cargo_value']);
               $tariff_request=$tariff_request+$insurance+$input['bascule']+$input['lading_price']+$input['vacate_price'];
           }

           $average_request=calculate_average($input['from'], $input['to'],$input['car_type']);
           $average_request= number_format($average_request);
           $tariff_request= number_format($tariff_request);

           return compact('tariff_request','average_request');
       }
    }

    public function get_order_fields($input){
        $data=[];
        //$data['car_type'] = $input['step_one_bar_car_type'];
        $data['product_id'] = $input['step_one_bar_title'];
        $data['packing_id'] = $input['step_one_bar_box_type'];
        $data['tonage'] = $input['step_one_bar_weight'];
        $data['cargo'] = ta_latin_num_price($input['step_one_bar_cargo_value']);
        $data['bascule'] = ta_latin_num_price($input['step_one_bar_bascule_price']);
        $data['lading'] = ta_latin_num_price($input['step_one_bar_lading_price']);
        $data['vacate'] = ta_latin_num_price($input['step_one_bar_vacate_price']);
        $data['description'] = $input['step_one_bar_description'];
        $data['customer_address_id'] = $input['step_two_bar_sender_place'];
        $data['receiver_id'] = $input['step_two_bar_receiver_place'];

        $data['date'] = Date('Y-m-d',strtotime("+".$input['step_two_bar_date'].' days'));
        $data['hour_from'] = $input['step_two_bar_hour_from'];
        $data['hour_to'] = $input['step_two_bar_hour_to'];

        $data['price'] = ta_latin_num_price($input['step_four_bar_price']);
        $data['insurance'] = calculate_insurance($data['cargo']);
        $data['tarefe'] = ta_latin_num_price($input['tariff_request']);
        $data['avg'] = ta_latin_num_price($input['average_request']);
        return $data;
    }

    public function get_orders(Request $request)
    {
        // $input=$request->all();
        $text=$request->data;
        $driver=null;
        if(auth()->user()->role->slug=='driver')
            $driver = auth()->user()->object();

        $data=get_orders($text,$driver);

        //dd($data);
        if ($data['orders']) {
            return response()->json(['status'=>'success','result'=>$data]);
        }
        return response()->json(['status'=>'error','data'=>'','message'=>'سفارشی یافت نشد']);
    }

    public function driver_accept(Request $request)
    {
        $input=$request->all();
        driver_accept($input);
    }
}
