<?php

namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\CustomerAddressController;
use App\Http\Controllers\ReceiverController;
use App\Http\Controllers\UserController;
use App\Models\City;
use App\Models\Driver;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Http\Controllers\Api\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\OrderController as myOrderController;


class OrderController extends Controller
{

    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
//        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function get_orders(Request $request)
    {
        $text=$request->data;
        $data=get_orders($text);

        if ($data['orders']) {
            return $this->response->setResult($data)
                ->setStatus(200)->render();
        }
        return $this->response->setNotFoundStatus('سفارشی یافت نشد')->setStatus(404)->render();

    }

    public function driver_accept(Request $request)
    {
        $input=$request->all();
        driver_accept($input);


    }


    public function request_bars(){
        $page_title = 'بارلند | بارهای درخواستی ';
        $items=get_requset_bars();
        return response()->json(['status'=>'success','result'=>$items,'message'=>'success']);



    }

    public function in_way_bars()
    {
        $items=get_in_way_bars();
        return response()->json(['status'=>'success','result'=>$items,'message'=>'success']);
    }


    public function finished_bars()
    {
        $items=get_finished_bars();
        return response()->json(['status'=>'success','result'=>$items,'message'=>'success']);
    }

    public function store_order(Request $request)
    {
        $order=new myOrderController();
        $response=$order->store($request);
        return $response;
    }
    public function get_ten_last_request(Request $request){
        $order=new myOrderController();
        $response=$order->get_ten_last_request($request);
        return $response;
    }


    public function check(Request $request)
    {
      dd($this->user);
    }

    public function store_address(Request $request)
    {
        $address=new CustomerAddressController();
        $response=$address->store($request);
        return $response;
    }

    public function store_receiver(Request $request)
    {
        $receiver=new ReceiverController();
        $response=$receiver->store($request);
        return $response;
    }

    public function calculate_way_price(Request $request)
    {
        $order=new myOrderController();
        $response=$order->calculate_way_price($request);
        return $response;
    }




}
