<?php

namespace App\Http\Controllers\Api;
use App\Services\Response\ClientResponseBuilder;
use http\Env\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $response;

    public function __construct()
    {
        $this->response=new ClientResponseBuilder();
    }

    public function get_orders(\Illuminate\Http\Request $request)
    {
        dd(1);
//        return $this->response()->json(['status'=>'success']);
    }
}
