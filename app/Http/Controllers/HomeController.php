<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Customer;
use App\Models\NewsLetter;
use App\Models\Post;
use App\Models\Province;
use App\Models\Role;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $page_title = 'بارلند | برترین سایت ';
        return view('site.pages.index',compact('page_title'));
    }
    public function login($type)
    {
        $page_title='بارلند | ورود';
        $page_type=$type;
        return view('site.pages.login',compact('page_title','page_type'));
    }

    public function login_attemp(Request $request)
    {

        session()->forget('change_password');
        $role=Role::where('slug',$request->role)->first();
        $mobile=$request->mobile;
        $user=User::where('mobile',$mobile)->where('role_id',$role->id)->first();
        dd($mobile);
        if($user){
           $status=1;
        }
        else{
            if($role->slug=='customer') {
                $password = rand(10000, 99999);
                $hash = bcrypt($password);
                $user = new User();
                $customer = new Customer();
                $user->mobile = $mobile;
                $user->password = $hash;
                $user->code = $hash;
                $user->role_id = $role->id;
                $user->save();
                $status = 2;
                //$customer->user_id = $user->id;
                $customer->save();
                $user->object_id = $customer->id;
                $user->save();
            }
            else{
                return redirect(url('/'));
            }
//               session(['code'=>$password]);
        }

        session(['selectedUser'=>$user->id.'*'.$status]);
        return redirect(asset('confirm'));
    }

    public function locked(){
        return view('auth.lock');
    }

    public function unlock(Request $request){
        if(Hash::check($request->password, auth()->user()->password))
            return response()->json(['status'=>'success', 'message'=>'ورود موفقیت آمیز بود']);
        return response()->json(['status'=>'error', 'message'=>'رمز عبور وارد شده صحیح نیست']);
    }


    public function about()
    {
        //dd(page_type);
        $page_title='بارلند | درباره ما';
        return view('site.pages.about',compact('page_title'));
    }
    public function contact()
    {
        $page_title='بارلند | تماس با ما';
        return view('site.pages.contact',compact('page_title'));
    }

    public function contact_submit(Request $request)
    {
        $inputs=[];
        $input=$request->except('_token');
        foreach ($input as $key=>$value){
            $inputs[$key]=strip_tags($value);
        }
        $contact=Contact::create($input);
        if($contact)
            return response()->json(['status'=>'success','message'=>'پیام با موفقیت ثبت شد']);
        return response()->json(['status'=>'error','message'=>'خطا در ثبت پیام']);

    }


    public function faq()
    {
        $page_title='بارلند | سوالات متداول';
        return view('site.pages.faq',compact('page_title'));
    }

    public function get_cities(Request $request)
    {
        return response()->json(['status'=>'success','data'=>Province::find($request->province)->cities]);
    }

    public function news_letter_store(Request $request)
    {
        $input=$request->except('_token');
        $email=strip_tags($input['email']);
        $check=NewsLetter::where('email',$email)->first();
        if($check){
            return response()->json(['status'=>'error','message'=>'این ایمیل در خبرنامه عضو است']);

        }
        $news_letter=NewsLetter::create(['email'=>$email]);
        if($news_letter)
            return response()->json(['status'=>'success','message'=>'عضویت با موفقیت انجام شد']);
        return response()->json(['status'=>'error','message'=>'خطا در عضویت']);

    }

    public function contacts_all()
    {
        $contacts=Contact::all();
        dd($contacts);

    }


    public function blog($filter=null)
    {
        $page_title='بارلند | وبلاگ';
        $title = 'وبلاگ';
        $posts = Post::paginate(10);
        $model='';
        if($filter!=null){
            $filter = explode('=',$filter);
            switch ($filter[0]){
                case 'tag':
                    $model = Tag::class;
                    break;
                case 'category':
                    $model = Category::class;
                    break;
            }
            $item = $model::where('slug',$filter[1])->first();
            if(!isset($item->id))
                return redirect(route('404'));
            $posts = get_id($item->posts);
            $posts = Post::whereIn('id',$posts)->paginate(10);
            $page_title = ' بارلند | '.$item->title;
        }
        return view('site.pages.blog',compact('page_title','posts','title'));
    }

    public function blog_search(Request $request){
        $page_title='بارلند | جستجوی '.$request['s'];
        $title = 'جستجوی: '.$request['s'];
        $posts = Post::where('title', 'like','%'.$request['s'].'%')->orWhere('body', 'like', '%'.$request['s'].'%')->paginate(10);
        return view('site.pages.blog',compact('page_title','posts','title'));
    }

    public function blog_single($slug)
    {
        $post = Post::where('slug',$slug)->first();
        if(!isset($post->id))
            return redirect(route('404'));
        $page_title='بارلند | '.$post->title;
        $post->increment('viewed');
        return view('site.pages.blog-single',compact('page_title','post'));
    }

    public function store_comment(Request $request){
        $captcha=true;
        $input=[];
        $input['parent_id']=$request['parent_id'];
        $prefix= $input['parent_id']!=0 ? 'modal_' : '';
        $temp['captcha']=$request[$prefix.'captcha'];
        if(Validator::make($temp, ['captcha' => 'required|captcha'])->fails()) {
            $captcha=false;
        }
        if ($captcha) {
            $input['title'] = $request[$prefix .'title'];
            $input['comment'] = strip_tags($request[$prefix .'content']);
            $input['commentable_id'] = $request['commentable_id'];
            $input['commentable_type'] = 'App\\Models\\Post';
            $input['user_id'] = auth()->user()->id;
            $result = Comment::create($input);
            if($result)
                return response()->json(['status'=>'success','data'=>'','message'=>'']);
            return response()->json(['status'=>'error','data'=>'','message'=>'خطا در ثبت دیدگاه']);
        }
        return response()->json(['message'=>'کپچای وارد شده صحیح نیست','status'=>'error']);
    }
}
