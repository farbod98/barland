<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function store(Request $request)
    {
        $new_product=$request->except('_token');
        Product::create($new_product);
    }

    public function destroy($id)
    {
        Product::find($id)->update(['active'=>0]);
    }

    public function update(Request $request,$id)
    {
        $update_product=$request->except('_token');
        Product::find($id)->update($update_product);
    }

}
