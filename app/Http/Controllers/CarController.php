<?php

namespace App\Http\Controllers;

use App\Models\CarFeature;
use App\Models\CarType;
use Illuminate\Http\Request;
use App\Models\Car;

class CarController extends Controller
{
    public function store(Request $request)
    {
        $new_car=$request->except('_token');
        Car::create($new_car);

    }

    public function destroy($id)
    {
        Car::find($id)->update(['active'=>0]);
    }
    public function edit($id)
    {
        $car=Car::find($id);
        $data['car']=$car;
        $cf=$car->feature;

        $temps=CarFeature::where('car_type_id',$cf->car_type_id)->get();

        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->id]=$temp['title'];
        $data['car_features']=$ret;
        return response()->json(['status'=>'success','data'=>$data]);
    }

    public function update(Request $request,$id)
    {
        $update_car=$request->except('_token');
        $car=Car::find($id)->update($update_car);
        return response()->json(['status'=>'success','data'=>$car]);
    }
}
