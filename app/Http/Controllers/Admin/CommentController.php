<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::latest()->get();
        return view('admin.comment.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.comment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'comment' => 'required|min:3',
                'commentable_type' => 'required|min:7',
                'id' => 'required',
            ]
            , [
                'comment.required' => 'متن نظر نمیتواند خالی باشد',
                'comment.min' => 'متن نظر باید بیشتر از سه حرف باشد',
                'commentable_type.required' => ' نمیتواند خالی باشد',
                'id' => ' صحیح نمی باشد',
            ]
        );

        $find = $request->commentable_type::where('id', $request->id)->first();
       $out= 0;
        if (isset($find->id))
        {
            $comment=$this->Comment->create([
                'user_id'=>auth()->user()->id,
                'parent_id'=>$request->parent_id,
                'comment'=>$request->comment,
                'commentable_type'=>$request->commentable_type,
                'commentable_id'=>$request->id,
                'is_active'=>0,
            ]);

            $out= 1;
        }


        if ($out)
            return $this->response->setMessage('عملیات با موفقیت انجام شد')
                ->setStatus(200)->render();
        return $this->response->setNotFoundStatus('عملیات انجام نشد')->setStatus(400)->render();


    }

    /**
     * Display the specified resource.
     *
     * @param Comment $comment
     * @return void
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Comment $comment
     * @return void
     */
    public function edit(Comment $comment)
    {
        return view('admin.comment.edit',compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Comment $comment
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {
        $comment->update($request->all());
        return redirect(route('comment.index'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return void
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect(route('comment.index'));
    }


    public function comment_status(Request $request)
    {

        $post =Comment::where('id', $request->id)->first();
        //if: for check id exists in table
        if (!isset($post->id)) {
            //if data not found return zero
            return response()->json('error',200);
        }
        if ($post->is_active == 1){
            $post->update(['is_active' => 0]);
            $active = 0;
        }
        else{
            $post->update(['is_active' => 1]);
            $active = 1;
        }
        return response([
            'data'=>$active,
            'status'=> 'success'
        ]);
        //return response()->json('success',200);
    }

    public function comment_approved(Request $request)
    {
       $post =Comment::where('id', $request->id)->first();
        //if: for check id exists in table
        if (!isset($post->id)) {
            //if data not found return zero
            return response()->json('error',200);
        }
        if ($post->user_acceptor==null){
            $post->update(
                [
                    'is_active' =>1,
                    'user_acceptor'=>auth()->user()->id,
                    'accept_time'=>Carbon::now(),

                    ]
                );
        }

        return response([
            'data'=>auth()->user()->name,
            'status'=> 'success'
        ]);
    }

    public function comment_answer(Request $request)
    {
        $com=Comment::where('id',$request->id)->first();
        $comment=Comment::create([
            'user_id'=>auth()->user()->id,
            'parent_id'=>$request->id,
            'comment'=>$request->comment,
            'commentable_type'=>$com->commentable_type,
            //'commentable_type'=>"App\Site\Post",
            'commentable_id'=>$com->commentable_id,
            'is_active'=>1,
            'user_acceptor'=>auth()->user()->id,
        ]);


        if ($comment){
            return response([
                'data'=>true,
                'status'=> 'success'
            ]);
        }

        return response([
            'data'=>false,
            'status'=> 'error'
        ]);


    }
}
