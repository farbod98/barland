<?php

namespace App\Http\Controllers\Admin;

use App\Models\Car;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\CarType;
use App\Http\Controllers\Controller;

class CarTypeController extends Controller
{

    public function index()
    {
        $car_types=CarType::withoutGlobalScope('active')->get();
        return view('pages.admin.car.index',compact('car_types'));
    }
    public function get()
    {
        $car_types=CarType::all();
        return response()->json(['status'=>'success','data'=>$car_types]);
    }

    public function store(Request $request)
    {
        $new_car_type=$request->except('_token');
        $car_type=CarType::create($new_car_type);
        return response()->json(['status'=>'success','data'=>$car_type]);

    }

    public function update(Request $request,$id)
    {
        $update_car_type=$request->except('_token');
        $car_type=CarType::where('id',$id)->withoutGlobalScope('active')->first();
        $car_type->update($update_car_type);
        return response()->json(['status'=>'success','data'=>$car_type]);

    }
    public function status($id)
    {
        $car_type=CarType::find($id);
        if($car_type->active)
            $car_type->update(['active'=>0]);
        else
            $car_type->update(['active'=>1]);
        return response()->json(['status'=>'success','data'=>$car_type]);
    }

    public function destroy($id)
    {
        CarType::find($id)->update(['active'=>0]);
        return response()->redirectTo('https://irinn.ir');
        return response()->json(['a'=>1,'b'=>2]);
    }

    public function edit($id)
    {
        $car_type=CarType::where('id',$id)->withoutGlobalScope('active')->first();
        return response()->json(['status'=>'success','data'=>$car_type]);
    }
}
