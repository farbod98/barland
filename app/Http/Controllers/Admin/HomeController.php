<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Gift;
use App\Models\NewsLetter;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function contact_all()
    {
        $items=Contact::all();
        return view('pages.admin.contact.index',compact('items'));

    }

    public function news_letter()
    {
        $items=NewsLetter::all();
        return view('pages.admin.news_letter.index',compact('items'));

    }
    public function gift()
    {
        $items=Gift::all();
        return view('pages.admin.gift.index',compact('items'));

    }


    public function gift_update(Request $request)
    {
        $inputs=$request->except('_token');
        $gift=Gift::find($inputs['id']);
        $gift->update($inputs);
        return response()->json(['status'=>'success','data'=>$gift]);

    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('site.pages.index');
    }

    public function locked(){
        return view('auth.lock');
    }

    public function unlock(Request $request){
        if(Hash::check($request->password, auth()->user()->password))
            return response()->json(['status'=>'success', 'message'=>'ورود موفقیت آمیز بود']);
        return response()->json(['status'=>'error', 'message'=>'رمز عبور وارد شده صحیح نیست']);
    }
}
