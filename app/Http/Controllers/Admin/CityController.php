<?php

namespace App\Http\Controllers\Admin;

use App\Models\Car;
use App\Models\CarType;
use App\Models\Customer;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Models\City;
use App\Http\Controllers\Controller;

class CityController extends Controller
{

    public function show($id)
    {

        $province=Province::where('id',$id)->withoutGlobalScope('active')->first();
        return view('pages.admin.city.index',compact('province'));
    }
    public function store(Request $request)
    {
//        dd($request->all());
//        $carFeature=CarFeature::find($id);
        $update_car_type=$request->except('_token');
        $update_car_type['slug']=$update_car_type['title'];
        $carFeature=City::create($update_car_type);
        return response()->json(['status'=>'success','data'=>$carFeature]);

    }
    public function edit(City $city)
    {
        return response()->json(['status'=>'success','data'=>$city]);
    }
    public function update(Request $request,$id)
    {
        $carFeature=City::where('id',$id)->withoutGlobalScope('active')->first();
        $update_car_type=$request->except('_token');
        $carFeature->update($update_car_type);
        return response()->json(['status'=>'success','data'=>$carFeature]);

    }
//    public function destroy ($id)
//    {
//        $carFeature=CarFeature::find($id);
//    $carFeature->update(['active'=>0]);
//        return response()->json(['status'=>'success','data'=>[]]);
//    }
}
