<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products=Product::withoutGlobalScope('active')->get();
        return view('pages.admin.product.index',compact('products'));
    }
    public function edit($id)
    {
        $car_type=Product::where('id',$id)->withoutGlobalScope('active')->first();
        return response()->json(['status'=>'success','data'=>$car_type]);
    }
    public function update(Request $request,$id)
    {
        $update_car_type=$request->except('_token');
        $car_type=Product::where('id',$id)->withoutGlobalScope('active')->first();
        $car_type->update($update_car_type);
        return response()->json(['status'=>'success','data'=>$car_type]);

    }


    public function store(Request $request)
    {
        $new_car_type=$request->except('_token');
        $car_type=Product::create($new_car_type);
        return response()->json(['status'=>'success','data'=>$car_type]);

    }

}
