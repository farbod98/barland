<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\CustomerAddress;
use App\Models\Province;
use App\Models\Receiver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Http\Controllers\Controller;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=Customer::all();
        return view('pages.admin.customer.index',compact('customers'));
    }


    public function store_data(Request $request){
        $all_cities = json_decode($request->data);
        $cities = City::all();
        $provinces = Province::all();
        //dd($cities);
        //$all_cities = $request->data;
        //dd($all_cities);
        foreach ($all_cities as $item){
            $item = json_to_array($item);
            foreach ($provinces as $city){
                if($city->title == $item['city'] && $item["capital"]=="admin"){
                    //dd($city);
                    $input=[];
                    $input['latitude']=$item['lat'];
                    $input['longitude']=$item['lng'];
                    $city->update($input);
                }
            }
            /*foreach ($cities as $city){
                if($city->title == $item['city'] && $city->province->title==$item['admin']){
                    //dd($city);
                    $input=[];
                    $input['latitude']=$item['lat'];
                    $input['longitude']=$item['lng'];
                    $city->update($input);
                }
            }*/
        }

        return response()->json(['status'=>'success','data'=>'','message'=>'']);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$user=Auth::loginUsingId(1);
        $new_customer=$request->except('_token');
        $new_customer['tels']=json_encode($new_customer['tels']);
        $new_customer+=['user_id'=>$user->id,'active'=>0];
        $customer=Customer::create($new_customer);
        if ($request->ajax())
            return response()->json(['status'=>'success','data'=>$customer]);
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer=Customer::find($id);
        $prepends=$this->get_prepends();
        return response()->json(['status'=>'success','data'=>$customer,'prepends'=>$prepends]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
//        $user=Auth::loginUsingId(2);
        $input=$request->except('_token');
        $customer=Customer::find($id);
        $customer->update($input);
        return response()->json(['status'=>'success','data'=>$customer]);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //$user=Auth::loginUsingId(1);
        Customer::find($user->customer->id)->update(['active'=>0]);
    }

    public function status($id)
    {
        $customer=Customer::find($id);
        if($customer->active)
            $customer->update(['active'=>0]);
        else
            $customer->update(['active'=>1]);
        return response()->json(['status'=>'success','data'=>$customer]);
    }

    public function get_prepends()
    {
        return['اقای','خانوم','شرکت'];
    }

    public function prepends()
    {
        return response()->json(['status'=>'success','prepends'=>['اقای','خانوم','شرکت']]);

    }

    public function get_address($id)
    {
        return response()->json(['status'=>'success','data'=>Customer::find($id)->address]);
    }

    public function address($id)
    {
        $address=CustomerAddress::find($id);
        $action='edit';
        return view('map',compact('address','action'));
    }

    public function manage_address($id)
    {
        $items['sender']=CustomerAddress::where('customer_id',$id)->get();
        $items['receiver']=Receiver::where('customer_id',$id)->get();
        return view('pages.admin.customer.manage_address',compact('items','id'));
    }

}
