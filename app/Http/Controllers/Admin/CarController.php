<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Car;

class CarController extends Controller
{
    public function store(Request $request)
    {
        $new_car=$request->except('_token');
        Car::create($new_car);

    }

    public function destroy($id)
    {
        Car::find($id)->update(['active'=>0]);
    }

    public function update(Request $request,$id)
    {
        $update_car=$request->except('_token');
        Car::find($id)->update($update_car);
    }

    public function index()
    {
        $cars=Car::all();
        return view('pages.admin.car.index',compact('cars'));
    }
}
