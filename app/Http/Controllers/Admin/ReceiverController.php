<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Receiver;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ReceiverController extends Controller
{
    public function store(Request $request)
    {
        $new_receiver=$request->except('_token');
        $receiver=Receiver::create($new_receiver);
        return response()->json(['status'=>'success','data'=>$receiver]);
    }

    public function destroy($id)
    {
        Receiver::find($id)->update(['active'=>0]);
    }

    public function update(Request $request,$id)
    {
        $edit_receiver=$request->except('_token');
//        $edit_receiver+=['customer_id'=>$user->customer->id];
        $receiver=Receiver::find($id);
        $receiver->update($edit_receiver);
        return response()->json(['status'=>'success','data'=>$receiver]);
    }

    public function edit($id)
    {
        $receiver=Receiver::find($id);
        $prepends=$this->get_prepends();
        return response()->json(['status'=>'success','data'=>$receiver,'prepends'=>$prepends]);

    }

    public function get_prepends()
    {
        return ['اقای','خانوم','شرکت'];
    }
    public function details()
    {
        $prepends=$this->get_prepends();
        $cities=DB::table('cities')->select('*')->get();
        return response()->json(['status'=>'success','prepends'=>$prepends,'cities'=>$cities]);
    }
}
