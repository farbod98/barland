<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Comment;
use App\Education;
use App\Expertise;
use App\Http\Requests\PostRequest;
use App\Photo;
use Carbon\Carbon;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\PanelController;

class PostController extends PanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
         * how to save comment in table comment, with relationship post
         *
        $post=new Post();
        $post->title="test";
        $post->body="test body";
        $post->user_id="1";
        $post->photo_id="1";
        $post->save();
        $comment=new Comment();
        $comment->body="comment";
        $post->comments()->save($comment);*/

        //$posts = Post::where('id',1)->with(['user'])->with(['categories'])->with(['tags'])->get();
        $posts = Post::with(['user'])->with(['categories'])->with(['tags'])->latest()->get();
        //return $posts;
        return view('admin.post.index', compact('posts'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        //return $request->all();
        $soc_array=[];
        $socials=get_socilas();
        foreach($socials as $social)
        $soc_array[$social]=$request[$social];
        $socials=json_encode($soc_array);
        $photo = image_upload($request->file('photo'), 'images','post');
        //$photo = saveImage($request->file('photo'),'posts');
        //$post=Post::create(array_merge($request->all(), ['photo_id'=>$photo->id,'user_id'=>1]));

        $post=Post::create(array_merge($request->all(), ['photo_id'=>$photo->id,'user_id'=>auth()->user()->id,'socials'=>$socials]));
        $post->tags()->sync($request['tags']);
        $post->categories()->sync($request['categories']);
        return redirect(route('post.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return void
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //return $post;
        $categories = Category::all();
        $tags = Tag::all();
        $post->socials=json_decode($post->socials);
        //$tags = Tag::with(['posts']);
        //$photos = Photo::where('id',$post->photo_id)->get();
        return view('admin.post.edit', compact('post','categories','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest $request
     * @param Post $post
     * @return void
     */
    public function update(PostRequest $request, Post $post)
    {

        //dd(count($request->file()));
        $file = $request->file();
        if (count($request->file())>0) {
            $photo = saveImage($request->file('photo'), 'posts');
            $post->update(array_merge($request->all(), ['photo_id' => $photo->id]));
        }else{
            $post->update($request->all());
        }
        $post->tags()->sync($request['tags']);
        $post->categories()->sync($request['categories']);
        return redirect(route('post.index'));
    }

    public function ck_upload_image()
    {
         $this->validate(request(), [
        'upload' => 'required|mimes:jpeg,png,bmp',
        ]);
        $year = Carbon::now()->year;
        $imagePath = "/upload/images/posts/ck/{$year}/";

        $file = request()->file('upload');
        $filename = $file->getClientOriginalName();

        if (file_exists(public_path($imagePath) . $filename)) {
            $filename = Carbon::now()->timestamp . $filename;
        }

        $file->move(public_path($imagePath), $filename);
        $url = $imagePath . $filename;

        return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('1', '{$url}' , 'عکس مورد نظر با موفقیت ذخیره گردید');</script>";
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        if(isset($post->id))
            $post->comments_delete()->delete();
        $post->delete();
        return redirect(route('post.index'));

    }



}
