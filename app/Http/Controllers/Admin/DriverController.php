<?php

namespace App\Http\Controllers\Admin;

use App\Models\Car;
use App\Models\CarFeature;
use App\Models\CarType;
use App\Models\Order;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class DriverController extends Controller
{
    public function index()
    {
        $drivers=Driver::where('id','>',0)->get();
        return view('pages.admin.driver.index',compact('drivers'));
    }
    public function update(Request $request,$id)
    {
        $input=$request->except('_token');
        $driver=Driver::find($id);

        $d['smart_code']=$input['smart_code'];
        $d['national_id']=$input['national_id'];
        $d['tels']=$input['tels'];
        $driver->update($d);
        $u['first_name']=$input['first_name'];
        $u['last_name']=$input['last_name'];
        $u['mobile']=$input['mobile'];
          $user=$driver->user;
        $user->update($u);

         if($driver->car_type->id!=$input['car_type_id'])
         {
             $car=$driver->car;
             $feature['car_feature_id']=CarFeature::where('car_type_id',$input['car_type_id'])->first()->id;
             $car->update($feature);
         }
        return response()->json(['status'=>'success','data'=>$driver]);



    }
    public function edit($id)
    {

        $driver=Driver::find($id);
        $car_types=CarType::all();
        $prepends=['اقای','خانوم','شرکت'];
        return response()->json(['status'=>'success','data'=>$driver,'car_types'=>$car_types,'prepends'=>$prepends]);
    }

    public function store(Request $request)
    {
        $new_driver=$request->except('_token');

      $car['car_feature_id']=CarFeature::where('car_type_id',$new_driver['car_type_id'])->first()->id;
      $car=Car::create($car);
      $driver['national_id']=$new_driver['national_id'];
      $driver['tels']=$new_driver['tels'];
      $driver['smart_code']=$new_driver['smart_code'];
      $driver['car_id']=$car->id;
        $driver=Driver::create($driver);
      $user['role_id']=1;
      $user['object_id']=$driver->id;
      $user['first_name']=$new_driver['first_name'];
      $user['last_name']=$new_driver['last_name'];
      $user['mobile']=$new_driver['mobile'];
      $password=bcrypt('123456');
        $user['password']=$password;
        $user['code']=$password;
        $user=\App\Models\User::create($user);
        $driver=Driver::find($driver->id);
        return response()->json(['status'=>'success','data'=>$driver]);
    }

    public function get()
    {
        $car_types=CarType::all();
        $prepends=['اقای','خانوم','شرکت'];
        return response()->json(['status'=>'success','data'=>$car_types,'prepends'=>$prepends]);

    }
    public function driver_request($id)
    {

        $data=[];
        $orders=[];
        if($id=='new')
        $orders=Order::where('driver_id',0)->get();
        else
            $orders=Order::where('id',$id)->get();
        foreach ($orders as $order)
            foreach ($order->driver_requests as $req)
            {
                if($order->driver_id==0)
                $req['status']=0;
                else
                    $req['status']=$order->driver_id==$req->id?1:-1;
                $data[]=$req;
            }


        return view('pages.admin.driver.request',compact('data'));

    }
    public function driver_accept(Request $request)
    {
        $input=$request->all();
        $order=Order::find($input['order_id']);
        $temp['driver_id']=$input['driver_id'];
        $temp['status']=1;
        $order->update($temp);
        return response()->json(['status'=>'success','data'=>$order]);


    }
}
