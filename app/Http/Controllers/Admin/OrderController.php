<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\Driver;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $orders=Order::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_order=$request->except('_token');
        $order=Order::create($new_order);
        $order=Order::find($order->id);
        return response()->json(['status'=>'success','data'=>$order]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders=Order::withoutGlobalScope('active')->where('status',$id)->get();
        return view('pages.admin.order.index',compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order=Order::find($id);
        $products=Product::all();
        $drivers=Driver::all();
        return response()->json(['status'=>'success','data'=>$order,'products'=>$products,'drivers'=>$drivers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_order=$request->except('_token');
        $order=Order::find($id);
        $order->update($update_order);
        return response()->json(['status'=>'success','data'=>$order]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::find($id)->update(['active'=>0]);
    }

    public function get_details()
    {
        $customers=Customer::all();
        $drivers=Driver::all();
        $products=Product::all();
        return response()->json(['status'=>'success','drivers'=>$drivers,'customers'=>$customers,'products'=>$products]);
    }
}
