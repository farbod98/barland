<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CarType;
use App\Models\City;
use Illuminate\Http\Request;
use App\Models\Price;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_price=$request->except('_token');
        $price= Price::create($new_price);
        $price->car;
        return response()->json(['status'=>'success','data'=>$price]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city=City::where('id',$id)->withoutGlobalScope('active')->first();
        $prices=get_element($city->prices,'end_city');
        $temps=City::whereNotIn('code',$prices)->get();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->code]=$temp['title'];
       $cities=$ret;

        $temps=CarType::all();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->id]=$temp['title'];
        $cars=$ret;
        return view('pages.admin.price.city',compact('city','cars','cities'));
    }
    public function car_price($id)
    {
        $car=CarType::where('id',$id)->withoutGlobalScope('active')->first();
        $prices=get_element($car->prices,'end_city');
        $temps=City::whereNotIn('code',$prices)->get();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->code]=$temp['title'];
        $cities=$ret;
        $temps=CarType::all();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->id]=$temp['title'];
        $cars=$ret;
        return view('pages.admin.price.car',compact('car','cars','cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $price=Price::find($id);
        $data['price']=$price;
        $temps=City::all();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->code]=$temp['title'];
        $data['cities']=$ret;
        $temps=CarType::all();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->id]=$temp['title'];
        $data['cars']=$ret;

        return response()->json(['status'=>'success','data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_car_type=$request->except('_token');
        $province=Price::find($id);
        $province->update($update_car_type);
        $province->car;
        return response()->json(['status'=>'success','data'=>$province]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Price::find($id)->update(['active'=>0]);
    }
}
