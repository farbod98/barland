<?php

namespace App\Http\Controllers\Admin;

use App\Models\Car;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{

    public function index()
    {
        $provinces=Province::withoutGlobalScope('active')->where('id','>',0)->get();
        $ret=[];
        foreach (Province::withoutGlobalScope('active')->get() as $temp)
            $ret[$temp->id]=$temp['title'];
        $provinces_list=$ret;
        return view('pages.admin.province.index',compact('provinces','provinces_list'));
    }
    public function update(Request $request,$id)
    {
        $update_car_type=$request->except('_token');
        $province=Province::where('id',$id)->withoutGlobalScope('active')->first();
        $province->update($update_car_type);
        $province->parent;
        return response()->json(['status'=>'success','data'=>$province]);

    }
    public function edit($id)
    {
        $province=Province::where('id',$id)->withoutGlobalScope('active')->first();
        $data['province']=$province;
        $temps=Province::all();
        $ret=[];
        foreach ($temps as $temp)
            $ret[$temp->id]=$temp['title'];
        $data['provinces']=$ret;
        return response()->json(['status'=>'success','data'=>$data]);
    }
    public function store(Request $request)
    {
        $new_car_type=$request->except('_token');
        $new_car_type['slug']=$new_car_type['title'];

        $province=Province::create($new_car_type);
        $province->parent;
        return response()->json(['status'=>'success','data'=>$province]);

    }
}
