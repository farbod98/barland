<?php

namespace App\Http\Controllers\Admin;

use App\Models\Car;
use App\Models\CarType;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\CarFeature;
use App\Http\Controllers\Controller;

class CarFeatureController extends Controller
{

    public function show($id)
    {

        $car_type=CarType::where('id',$id)->withoutGlobalScope('active')->first();
        return view('pages.admin.car_feature.index',compact('car_type'));
    }
    public function store(Request $request)
    {
//        dd($request->all());
//        $carFeature=CarFeature::find($id);
        $update_car_type=$request->except('_token');
        $carFeature=CarFeature::create($update_car_type);
        return response()->json(['status'=>'success','data'=>$carFeature]);

    }
    public function edit(CarFeature $carFeature)
    {
        return response()->json(['status'=>'success','data'=>$carFeature]);
    }
    public function update(Request $request,$id)
    {
        $carFeature=CarFeature::where('id',$id)->withoutGlobalScope('active')->first();
        $update_car_type=$request->except('_token');
        $carFeature->update($update_car_type);
        return response()->json(['status'=>'success','data'=>$carFeature]);

    }
    public function destroy ($id)
    {
        $carFeature=CarFeature::find($id);
    $carFeature->update(['active'=>0]);
        return response()->json(['status'=>'success','data'=>[]]);
    }
}
