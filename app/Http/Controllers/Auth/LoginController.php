<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\LoginLog;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Input;
/*use Symfony\Component\Console\Input\Input;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;*/

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    public function showLoginForm()
    {
        if(!session('type')) {
            session(['redirect_login'=>true]);
            return redirect(url('/'));
        }
        return view('auth.login');
    }

    public function login(Request $request)
    {
        if(isset($request['captcha'])){
            if(Validator::make(['captcha'=>$request->input('captcha')], ['captcha' => 'required|captcha'])->fails()) {
                $message = __('general.failed_captcha');
                return response()->json(['message' => $message, 'status' => 'error']);
            }
        }

        $user_pass = ['username'=>$request['username'],'password'=>$request['password']];
        $log = LoginLog::create([
            'user_id'=>'0',
            'ip'=> request()->ip(),
            'user_pass'=>json_encode($user_pass),
            'type'=>'login',
        ]);

        $this->validateLogin($request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

//    protected function sendLoginResponse(Request $request)
//    {
//        $last_attepmt=get_last_attempt('login');
//        $last_attepmt->update(['user_pass'=>'','user_id'=>auth()->user()->id]);
//
//        $request->session()->regenerate();
//        $this->clearLoginAttempts($request);
//        if(isset($request->ajax)) {
//            if (auth()->user()->is_active==0) {
//                $message = __('login/login_page.need_to_confirm');
//                return response()->json(['status' => 'error', 'reload' => true, 'data' => true, 'message' => $message]);
//            }
//            else {
//                $message = __('login/login_page.toastr_login_success');
//                return response()->json(['status' => 'success', 'reload' => true, 'data' => true, 'message' => $message]);
//            }
//        }
//        return $this->authenticated($request, $this->guard()->user())
//            ?: redirect()->intended($this->redirectPath());
//    }

    protected function authenticated(Request $request, $user)
    {
        $last_attepmt=get_last_attempt('login');
        $last_attepmt->update(['user_pass'=>'','user_id'=>$user->id]);
    }

    public function attempt_count(Request $request){
        return response()->json(['status'=>'success', 'data'=>login_attempt_count($request->type), 'attempt'=>true]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/');
    }

    public function unlock(){
        dd(auth()->user()->id);
        return response()->json(['status'=>'success', 'data'=>'', 'attempt'=>true]);
    }

    public function login_type($type){
        $page_title='بارلند | ورود';
        $page_type=$type;
        return view('site.pages.login',compact('page_title','page_type'));
    }

    public function login_attempt(Request $request){
        session()->forget('change_password');
        $role=Role::where('slug',$request->role)->first();
        $mobile=$request->mobile;
        //$user=User::where('mobile',$mobile)->where('role_id',$role->id)->first();
        $user=User::where('mobile',$mobile)->first();
        if($user)
            $status=1;
        else{
            $input=[];
            $password = 1234;
            $hash = bcrypt($password);
            $input['mobile'] = $mobile;
            $input['password'] = $hash;
            $input['code'] = $hash;
            $input['role_id'] = $role->id;
            $status = 2;
            if($role->slug=='customer') {
                $customer = Customer::create([]);
                $input['object_id'] = $customer->id;
                $user = User::create($input);
            }
            else if($role->slug=='driver'){
                return response()->json(['status'=>'error','data'=>'','message'=>'چنین شماره ای در سیستم وجود ندارد، لطفا از صفحه اصلی اقدام به ساخت حساب کنید']);
            }
        }
        session(['selectedUser'=>$user->id.'*'.$status]);
        return response()->json(['status'=>'success','data'=>'','message'=>'']);
    }

    public function confirm_view()
    {
        $data=explode('*',session('selectedUser'));
        $user_id=$data[0];
        $status=$data[1];
        $mobile=User::find($user_id)->mobile;
        return view('site.pages.confirm',compact('status','user_id','status','mobile'));
    }

    public function confirm_validate(Request $request)
    {
        $password=$request->code;
        $data=explode('*',session('selectedUser'));
        $user_id=$data[0];
        $status=$data[1];
        $mobile=User::find($user_id)->mobile;
        $cred=['mobile'=>$mobile,'password'=>$password];
        if(session('change_password')){
            $user=User::find($user_id);
            if(Hash::check($password,$user->code)) {
                session()->forget('change_password');
                $user->password=bcrypt($password);
                $user->save();
                return response()->json(['status'=>'success','route'=>'login/'.session('type')]);
            }
            return response()->json(['status'=>'error','data'=>'','message'=>'اطلاعات وارد شده صحیح نیست']);
        }
        if(Auth::attempt($cred,false)){
            $user=auth()->user();
            $item=$user->object();
            if ($item->status < $item->get_required_status()){
                return response()->json(['status'=>'success','route'=>route('user.index')]);
            }
            return response()->json(['status'=>'success','route'=>url($user->role->slug)]);
        }
        return response()->json(['status'=>'error','data'=>'','message'=>'اطلاعات وارد شده صحیح نیست']);
    }

    public function forgot_password_view()
    {
        $page_title='بارلند | فراموشی رمز عبور';
        $page_type=session('type');
        return view('site.pages.forgot_password',compact('page_title','page_type'));
    }

    public function forgot_password(Request $request)
    {
        $mobile=$request->mobile;
        $user=User::where('mobile',$mobile)->first();
        if(!isset($user->id))
            return response()->json(['status'=>'error','data'=>'','message'=>'شماره تلفن وارد شده در سیستم یافت نشد']);
        $code=rand(10000, 99999);
        $user->update(['code',bcrypt($code)]);
        $status=2;
        session(['selectedUser'=>$user->id.'*'.$status,'change_password'=>true]);
        return response()->json(['status'=>'success','data'=>'','message'=>'']);
    }
}
