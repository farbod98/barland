<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\LoginLog;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        $user_pass = ['email'=>$request['email']];
        $user=User::where('email',$request['email'])->first();
        $log = LoginLog::create([
            'user_id'=>$user->id,
            'ip'=> request()->ip(),
            'user_pass'=>json_encode($user_pass),
            'type'=>'forget',
        ]);

        if (request()->ajax) {
            $data=['swal_title' => __('login/login_page.swal_forget_title'), 'swal_confirm' => __('login/login_page.swal_forget_confirm')];
            $message=__('login/login_page.forgetPassword.send_email_response');
            return response()->json(['status' => 'success', 'data' => $data, 'forget' => true, 'message' => $message]);
        }
        else
            return back()->with('status', trans($response));
    }

    public function sendResetLinkEmail(Request $request)
    {
        if(isset($request['captcha'])){
            if(Validator::make(['captcha'=>$request->input('captcha')], ['captcha' => 'required|captcha'])->fails()) {
                $message = __('general.failed_captcha');
                return response()->json(['message' => $message, 'status' => 'error']);
            }
        }

        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        if (request()->ajax)
            return response()->json(['status' => 'error', 'message' => __('login/login_page.forgetPassword.failed_email')]);
        else
        return back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => trans($response)]);
    }



}
