<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarFeature;
use App\Models\CarType;
use App\Models\City;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Photo;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        $user = auth()->user();
        $data = get_role()=='driver'?$this->driver_index($user):$this->customer_index($user);
        $page_title = 'بارلند | کاربر ';
        return view('site.pages.'.get_role().'.profile.user-info',compact('page_title','user','data'));
    }

    public function driver_index($user){
        $input=[];
        $input['car_type'] = $user->object()->car_type->id;
        $input['car'] = $user->object()->car;
        $input['car_no'] = json_decode($input['car']->pelak);
        return $input;
    }

    public function customer_index($user){
        $input=[];
        $input['provinces'] = Province::get_provinces()->get(['id','title']);
        $input['province_selected'] = '';
        $input['cities'] = [];
        if(isset($user->object()->city)) {
            if ($user->object()->city_id > 0) {
                $input['selected_province']=$user->object()->city->province_id;
                $input['cities'] = City::where('province_id', $input['selected_province'])->get(['id', 'title']);
            }
        }
        return $input;
    }

    public function update_profile(Request $request){
        $user = auth()->user();
        $input=$request->all();
        //////////////////// User Update ///////////////////
        $user_data=[];
        $user_data['first_name']=$input['first_name'];
        $user_data['last_name']=$input['last_name'];
        $user_data['mobile']=$input['mobile'];
        $user_data['email']=$input['email'];
        if(isset($input['photo_id'])) {
            $path = image_upload($input['photo_id'], 'images','user',$user->id);
            $photo_data=['path' => $path, 'title' => $user['name'], 'slug' => 'user_image'];
            $photo = isset($user->photo)? $user->photo->update($photo_data) : Photo::create($photo_data);
            $user_data['photo_id']=isset($user->photo) ? $user->photo_id : $photo->id;
        }
        $user->update($user_data);

        $data = get_role()=='driver'?$this->driver_update_profile($user,$input):$this->customer_update_profile($user,$input);
        if ($data)
            return response()->json(['status' => 'success', 'data' => $data, 'message' => 'عملیات به روز رسانی حساب کاربری با موفقیت انجام شد']);
        else
            return response()->json(['status' => 'error', 'message' => 'عملیات به روز رسانی حساب کاربری با خطا مواجه شد', 'data' => '']);
    }

    public function driver_update_profile($user,$input){
        //////////////////// Driver Update ///////////////////
        $driver_data=[];
        $driver_data['national_id']=$input['national_id'];
        $driver_data['tels']=json_encode($input['tels']);
        $driver_data['smart_code']=$input['driver_smart_code'];
        $driver = Driver::find($user->object_id);
        $driver->update($driver_data);
        ///////////////////// Car Update ////////////////////
        $car_data=[];
        $car = Car::find($driver->car_id);
        $car_data['smart_code']=$input['car_smart_code'];
        $car_data['car_feature_id']=$input['car_feature_id'];
        $car_data['pelak']=json_encode($input['car_no']);
        $car->update($car_data);
        return true;
    }

    public function customer_update_profile($user,$input){
        $customer_data=[];
//        $customer_data['title']=$input['title'];
        $customer_data['city_id']=$input['city_id'];
        $customer_data['postal_code']=$input['postal_code'];
//        $customer_data['tels']=json_encode($input['tels']);
        $customer = Customer::find($user->object_id);
        $customer->update($customer_data);
        return true;
    }

    public function old_trips(){
        $page_title = 'بارلند | سفرهای قبلی راننده ';
        return view('site.pages.'.get_role().'.profile.old-trips',compact('page_title'));
    }

    public function request_trips(){
        $page_title = 'بارلند | سفرهای درخواستی راننده ';
        return view('site.pages.'.get_role().'.profile.request-trips',compact('page_title'));
    }

    public function request_bars(){
        $page_title = 'بارلند | بارهای درخواستی ';
        $items=get_requset_bars();
        $page_type='request-bars';
        return view('site.pages.'.get_role().'.profile.manage-bars',compact('page_title','items','page_type'));
    }
    public function finished_bars(){
        $page_title = 'بارلند | بارهای به مقصد رسیده ';
        $items=get_finished_bars();
        $page_type='finished-bars';
        return view('site.pages.'.get_role().'.profile.manage-bars',compact('page_title','items','page_type'));
    }
    public function in_way_bars(){
        $page_title = 'بارلند | بارهای در حال ارسال ';
        $items=get_in_way_bars();
        $page_type='in-way-bars';
        return view('site.pages.'.get_role().'.profile.manage-bars',compact('page_title','items','page_type'));
    }

    public function change_password(){
        $page_title = 'بارلند | تغییر رمز عبور ';
        return view('site.pages.change-password',compact('page_title'));
    }

    public function update_password(Request $request){
        $input=[];$data=[];$user=auth()->user();
        $input['old_password'] = ta_latin_num($request['current_password']);
        $input['new_password'] = ta_latin_num($request['new_password']);
        $input['re_password']  = ta_latin_num($request['repeat_password']);

        if ($input['new_password'] != $input['re_password'])
            return response()->json(['status'=>'error', 'data'=>'', 'message'=>'رمز عبور جدید با تکرار آن مطابقت ندارد']);
        $valid=Hash::check($input['old_password'], auth()->user()->password);
        if (!$valid)
            return response()->json(['status' => 'error', 'data' => '', 'message' => 'رمز عبور فعلی صحیح نیست']);
        $data['password'] = bcrypt($input['new_password']);
        $res = $user->update($data);
        if ($res)
            return response()->json(['status' => 'success', 'data' => '', 'message' => 'عملیات تغییر رمز عبور با موفقیت انجام شد']);
        else
            return response()->json(['status' => 'error', 'message' => 'عملیات تغییر رمز عبور با خطا مواجه شد', 'data' => '']);
    }

    public function manage_address(){
        $page_title = 'بارلند | مدیریت آدرس ها ';
        $items=[];
        $customer = auth()->user()->object();
        $items['sender'] = $customer->customer_address;
        $items['receiver'] = $customer->receivers;
        $id=$customer->id;
        return view('site.pages.'.get_role().'.profile.manage-address',compact('page_title','items','id'));
    }

    public function login(){}

    public function register(Request $request){
        $user = User::create([
            'email'    => $request->email,
            'password' => $request->password,
        ]);

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }
}
