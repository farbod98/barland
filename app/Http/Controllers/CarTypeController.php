<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CarType;


class CarTypeController extends Controller
{

    public function store(Request $request)
    {
        $new_car_type=$request->except('_token');
        CarType::create($new_car_type);
    }

    public function update(Request $request,$id)
    {
        $update_car_type=$request->except('_token');
        CarType::find($id)->update($update_car_type);
    }

    public function destroy($id)
    {
        CarType::find($id)->update(['active'=>0]);
        return response()->redirectTo('https://irinn.ir');
        return response()->json(['a'=>1,'b'=>2]);
    }
}
