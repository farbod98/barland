<?php

use App\Models\CarType;
use App\Models\CustomerAddress;
use App\Models\Driver;
use \App\Models\LoginLog;
use App\Models\Order;
use App\Models\Receiver;
use App\Models\User;
use Carbon\Carbon;
use Morilog\Jalali\CalendarUtils;

function get_prepends(){
    return['اقای','خانوم','شرکت'];

}

function get_last_attempt($type){
    $last = LoginLog::where('ip', Request::ip())->where('type',$type)
        ->where('user_id', 0)->whereDate('created_at', date('Y-m-d'))->orderBy('id', 'desc')->first();
    return $last;
}

function get_last_login(){
    $last = LoginLog::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->first();
    return $last;
}

function login_attempt_count($type){
    $last = LoginLog::where('ip', Request::ip())->where('type',$type)
        ->whereDate('created_at', date('Y-m-d'));
    if($type=='login')
        $last->where('user_id', 0);
//    if($type=='forget')
//        $last->where('user_id', 0);
    return $last->count();
}

function get_role(){
    if(auth()->check())
        return auth()->user()->role->slug;
    return null;
}

function get_id($array)
{
$arr2 = [];
    if (is_countable($array))
        foreach ($array as $ar)
            if (isset($ar->id))
                $arr2[] = $ar->id;
    return $arr2;
}

function get_cities_from_request($province_id,$city_id){
     if($city_id>0)
         return [$city_id];
     if($province_id>0)
         return get_id(\App\Models\City::where('province_id',$province_id)->get());
     return get_id(\App\Models\City::all());

}

function get_order_of_request($arr){
     $order['type']='price';
     $order['sort']='desc';
     if(isset($arr['sort']))
         switch ($arr['sort']){
             case 'min_weight':
                 $order['type']='tonage';
                 $order['sort']='asc';
                 break;
             case 'max_price':
                 $order['type']='price';
                 $order['sort']='desc';
                 break;
             case 'nearest_time':
                 $order['type']='date';
                 $order['sort']='asc';
                 break;
             default:
                 $order['type']='date';
                 $order['sort']='asc';
                 break;



     }
     return $order;
}

function get_close_province_cities($dest_id){
    $arr=[
        '1'=>[2,3,14],
        '2'=>[1,14,20],
        '3'=>[1,14,25],
        '4'=>[9,10,15,17,19,23,26,28,31],
        '5'=>[8,18,27,28],
        '6'=>[13,22,26],
        '7'=>[13,17,23,29],
        '8'=>[5,15,19,27,28],
        '9'=>[4,13,23,26],
        '10'=>[4,11,15,16,21,31],
        '11'=>[10,12,15],
        '12'=>[11,15,24],
        '13'=>[6,9,23,26],
        '14'=>[1,2,3,18,20,25,30],
        '15'=>[4,8,10,11,12,19,24,27],
        '16'=>[10,21,29],
        '17'=>[4,7,21,23,29,31],
        '18'=>[5,14,25,28,30],
        '19'=>[4,8,15,28],
        '20'=>[2,14,22,30],
        '21'=>[10,16,17,29,31],
        '22'=>[6,20,26,30],
        '23'=>[4,7,9,13,17],
        '24'=>[12,15,27],
        '25'=>[3,14,18,27],
        '26'=>[4,6,13,22,28,30],
        '27'=>[5,8,15,18,24,25],
        '28'=>[4,5,8,18,19,26,30],
        '29'=>[7,16,17,21],
        '30'=>[14,18,20,22,26,28],
        '31'=>[4,10,17,21]
    ];
    $cities=\App\Models\City::whereIn('province_id',$arr[$dest_id])->get();
    return $cities;


}

function json_to_array($array)
{
    $temp=[];
    if (!$array)
        return json_decode('[]');;
    foreach ($array as $key => $value)
        $temp[$key] = $value;
    return $temp;
}

function top_navigation_class(){
    $array=['customer','driver'];
    $class = in_array(request()->path(), $array)?'':'has_scroll';
    //dd($class);
    return $class;
}

function get_type_of_login(){
    return session('type');
}

function edit_date($dt,$op='-')
{
    if($dt==null)
        return '';
    if ($dt) {
        $date = explode(' ', $dt);
        $time=$date[1];
        $date=$date[0];
        $date = explode($op, $date);
        $ret= implode('/', CalendarUtils::toJalali($date[0], $date[1], $date[2]));
        return $ret.' '.$time;
    } else return null;
}


function get_month()
{
    $month = ['01' => 'فروردین', '02' => 'اردیبهشت', '03' => 'خرداد',
        '04' => 'تیر', '05' => 'مرداد', '06' => 'شهریور',
        '07' => 'مهر', '08' => 'آبان', '09' => 'آذر',
        '10' => 'دی', '11' => 'بهمن', '12' => 'اسفند'
    ];
    return $month;
}

function create_real_address($input){
    $array = explode(',',$input['address']);
    $address = array_reverse($array);
    $final_address=[];
    foreach ($address as $key=>$item){
        $item = remove_extra_char($item);
        if(!is_numeric($item))
            $final_address[]=$item;
    }
    $address = implode(' - ',$final_address);

    //$input['customer_id'] = auth()->user()->object()->id;
    $input['address'] = $address;
    if(isset($input['tels']))
        $input['tels'] = json_encode($input['tels']);
    return $input;
}

function remove_extra_char($item){
    $excepts = ['_'];
    foreach ($excepts as $except)
        $item = str_replace($except,'',$item);
    return $item;
}

function get_element($array, $element){
    $arr2 = [];
    foreach ($array as $ar)
        $arr2[] = $ar->$element;
    return $arr2;
}
function calculate_insurance($cost)
{
    return ta_latin_num_price($cost)/100;
}

function calculate_average($from,$to,$car_type)
{
    $car=CarType::find($car_type);
    $car_features=get_id($car->car_features);
    $orders=Order::where('active',1)->where('status',1)->where('driver_id','>',0)->whereHas('customer_address',function($q1) use ($from){
        $q1->where('city_id',$from);
    })->whereHas('receiver',function($q2) use ($to){
        $q2->where('city_id',$to);
    })->orderBy('id','desc')->take(5)->get();
    if(count($orders)>0)
        return array_sum(get_element($orders,'price'))/count($orders);
    return 0;
}

function ta_latin_num_price($price){
    return str_replace(',','',$price);
}

function get_orders($text,$driver=null)
{
    $count=10;
    if($text!=null){

        $filters=explode(';',$text);

        $arr=[];
        foreach ($filters as $filter){
            $key_data=explode(':',$filter);
            $arr[$key_data[0]]=$key_data[1];
        }
        $data=[];
        $data['is_alt']=0;
        $data['page']=isset($arr['page'])?$arr['page']:1;
        $page=$data['page']-1;
        $sort=get_order_of_request($arr);
        $start_selected_cities=[];
        $start_selected_cities=get_cities_from_request(isset($arr['start_prov'])?$arr['start_prov']:0,isset($arr['start_city'])?$arr['start_city']:0);
        $dest_selected_cities=get_cities_from_request(isset($arr['dest_prov'])?$arr['dest_prov']:0,isset($arr['dest_city'])?$arr['dest_city']:0);
        $orders=Order::where('active',1)->where('driver_id',null)->whereHas('customer_address',function($q1) use ($start_selected_cities){
            $q1->whereIn('city_id',$start_selected_cities);
        })->whereHas('receiver',function($q2) use ($dest_selected_cities){
            $q2->whereIn('city_id',$dest_selected_cities);
        })->orderBy($sort['type'],$sort['sort'])->skip($count*$page)->take($count)->get();
        $data['orders']=$orders;
        if(!count($orders)){
            if(isset($arr['dest_city'])){

                $dest_selected_cities=get_cities_from_request(isset($arr['dest_prov'])?$arr['dest_prov']:0,0);
            }
            else {
                $dest_selected_cities = get_close_province_cities($arr['dest_prov']);
            }


            $alt_orders=Order::where('active',1)->where('driver_id',null)->whereHas('customer_address',function($q1) use ($start_selected_cities){
                $q1->whereIn('city_id',$start_selected_cities);
            })->whereHas('receiver',function($q2) use ($dest_selected_cities){
                $q2->whereIn('city_id',$dest_selected_cities);
            })->orderBy($sort['type'],$sort['sort'])->skip($count*$page)->take($count)->get();
            $data['orders']=$alt_orders;
            $data['is_alt']=1;
            if(!count($alt_orders))$data['is_alt']=2;
        }


        $data['last_page']=ceil(count($orders)/$count);

    }
    else{
        $data['orders']=Order::where('active',1)->where('driver_id',0)->orderBy('price','desc')->get();
        $data['last_page']=ceil(count($data['orders'])/$count);
    }


    return $data;
}

function driver_accept($input)
{
    $order_id=$input['data'];
    $user=User::find($input['user']);
    $driver=Driver::find($user->object_id);
    if($input['status']==1)
        $driver->orders_request()->attach($order_id,['created_at'=>date('Y-m-d H:i:s')]);
    else
        $driver->orders_request()->detach($order_id,['created_at'=>date('Y-m-d H:i:s')]);
}

function get_address_details($input){
    $model = $input['type'] == 'sender' ? CustomerAddress::class : Receiver::class;
    $address = $model::where('id',$input['id'])->first();
    $address->city;
    $address->city->province;
    return $address;
}

function get_customer_addresses(){
    $customer = auth()->user()->object();
    $addresses=$customer->customer_address;
    $arr=[];
    foreach ($addresses as $address)
        $arr[]=$address->id;
    return $arr;
}

function get_requset_bars(){
    $arr=get_customer_addresses();
    $items=Order::whereIn('customer_address_id',$arr)->where('active',1)->where('driver_id',null)->get();
    return $items;
}
function get_in_way_bars(){
    $arr=get_customer_addresses();
    $items=Order::whereIn('customer_address_id',$arr)->where('active',1)->where('driver_id','!=',null)->where('status',1)->get();
    return $items;
}
function get_finished_bars(){
    $arr=get_customer_addresses();
    $items=Order::whereIn('customer_address_id',$arr)->where('active',1)->where('driver_id','!=',0)->where('status',2)->get();
    return $items;
}

function ta_latin_num($string)
{
    $persian_num = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $latin_num = range(0, 9);
    $string = str_replace($persian_num, $latin_num, $string);
    return $string;
}
function my_json_decode($elements)
{
    if($elements==null)
        return [];

    return json_decode($elements);
}

function image_upload($file, $directory, $type,$filename=null)
{
    $year = Carbon::now()->year;
    $extra_path = $type=='post'?"{$year}/":'';
    $imagePath = "/upload/{$directory}/{$type}/".$extra_path;
    $extension = $file->getClientOriginalExtension();
//    if (file_exists(public_path($imagePath) . $filename))
//        unlink(public_path($imagePath).$filename);
    $filename = $filename!=null?$filename.'.'.$extension:Carbon::now()->timestamp . $file->getClientOriginalName();
    $file = $file->move(public_path($imagePath), $filename);
    return $imagePath . $filename;
}
