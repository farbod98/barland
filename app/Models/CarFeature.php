<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarFeature extends Model
{
    //protected $table="car_feature";
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('active',function ($builder){
            $builder->where('active',1);
        });
    }
    protected $guarded=['id'];

    public function car_type()
    {
        return $this->belongsTo(CarType::class);
    }
}
