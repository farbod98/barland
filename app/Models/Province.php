<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $guarded=['id'];
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('active',function ($builder){
            $builder->where('active',1);
        });
    }
    public static function get_provinces(){
        return Province::where('id','>',0);
    }

    public static function get_accepted_provinces()
    {
        return Province::where('id','>',0)->where('accepted',1)->get();
    }

    public function cities(){
        return $this->hasMany(City::class);
    }
    public function parent(){
        return $this->belongsTo(Province::class);
    }
}
