<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $guarded=[];
    protected $appends=['user','receiver'];
    public function getUserAttribute()
    {
        $user=User::find($this->user_id);
        return $user;
    }
    public function getReceiverAttribute()
    {
        $receiver=User::find($this->receiver_id);
        return $receiver;
    }
}
