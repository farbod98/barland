<?php

namespace App\Models;

use App\Models\Packing;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Morilog\Jalali\Jalalian;

class Order extends Model
{
    protected $guarded=['id'];
    protected $appends=['product','customer','receiver','start_city','dest_city','packing','jalaliDate','selected','time_to','time_from'];
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('active',function ($builder){
            $builder->where('active',1);
        });
    }
    public function getProductAttribute()
    {
        return Product::find($this->product_id);
    }
    public function getSenderAttribute()
    {
        return Customer::find($this->product_id);
    }
    public function getCustomerAttribute()
    {
      return $this->customer_address->customer;
    }
    public function getReceiverAttribute()
    {
        return Receiver::find($this->receiver_id);
    }
    public function getDriverAttribute()
    {
        return Driver::find($this->driver_id);
    }
    public function getSelectedAttribute()
    {
        if(auth()->user())
            if(auth()->user()->role_id==1)
        {
            $driver=auth()->user()->object();
            if(isset($driver->car_id))
                if ($this->driver_requests()->where('driver_id', $driver->id)->first() != null)
                    return 1;

        }
        return 0;
    }
    public function getPackingAttribute()
    {
        return Packing::find($this->packing_id);
    }
    public function getStartCityAttribute()
    {
        $address=CustomerAddress::find($this->customer_address_id);
        return City::find($address->city_id);
    }
    public function getDestCityAttribute()
    {
        $receiver=Receiver::find($this->receiver_id);
        return City::find($receiver->city_id);
    }

    public function car_features()
    {
        return $this->belongsToMany(CarFeature::class);
    }

    public function customer_address()
    {
        return $this->belongsTo(CustomerAddress::class);
    }
    public function receiver()
    {
        return $this->belongsTo(Receiver::class);
    }
    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
    public function driver_requests()
    {

        return $this->belongsToMany(Driver::class,'driver_order_request')->withPivot('created_at');
    }

    public function getJalaliDateAttribute()
    {
        if($this->date==null)
            return null;
        $d = explode(' ',$this->date);
        $date = Carbon::createFromFormat('Y-m-d',  $d[0]);
        $jalali=Jalalian::fromCarbon($date);
        return $jalali->format('Y-m-d');
    }

    public function getTimeToAttribute()
    {
        return Carbon::createFromFormat('H:i',$this->hour_to)->format('H:i');
    }

    public function getTimeFromAttribute()
    {
        return Carbon::createFromFormat('H:i',$this->hour_from)->format('H:i');
    }

}
