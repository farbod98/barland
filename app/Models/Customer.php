<?php

namespace App\Models;

//use DemeterChain\C;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded=['id'];
    public $timestamps=false;
    protected $appends=['customer_address'];
    public function receivers()
    {
        return $this->hasMany('App\Models\Receiver');
    }
    public function address()
    {
        return $this->hasMany('App\Models\CustomerAddress');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getCustomerAddressAttribute()
    {
        return CustomerAddress::where('customer_id',$this->id)->get();
    }

    public function get_required_status()
    {
        return -1;
    }



    public function active_check()
    {
        if(User::find($this->user_id)->active)
            return true;
        return false;
    }

    public function status_check()
    {
        return $this->status>=$this->get_required_status();
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function  getUserAttribute()
    {
        if(isset(User::where('object_id',$this->id)->where('role_id',2)->first()->id))
        return User::where('object_id',$this->id)->where('role_id',2)->first();
        dd($this->id);
    }
}
