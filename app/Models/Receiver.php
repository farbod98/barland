<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    protected $guarded=['id','created_at','updated_at'];

    public function city(){
        return $this->belongsTo(City::class);
    }
}
