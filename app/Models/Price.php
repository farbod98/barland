<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $guarded=['id'];
    protected $appends=['end'];
    public function getEndAttribute()
    {

        if(isset(City::where('code',$this->end_city)->first()->title))
        return City::where('code',$this->end_city)->first()->title;
        return 'ثبت نشده';
    }
    public function car()
    {
        return $this->belongsTo(CarType::class);
    }
    public function start()
    {
        return $this->belongsTo(City::class,'start_city');
    }
}
