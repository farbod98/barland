<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded=[];
    public function feature()
    {
        return $this->belongsTo(CarFeature::class,'car_feature_id');
    }
}
