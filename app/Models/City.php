<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('active',function ($builder){
            $builder->where('active',1);
        });
    }
    protected $guarded=['id'];
    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function prices()
    {
        return $this->hasMany(Price::class,'start_city');
    }

    /*public function areas()
    {
        return $this->hasMany(Area::class);
    }*/

}
