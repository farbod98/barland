<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['user_id', 'user_acceptor', 'accept_time', 'parent_id', 'comment', 'likes', 'dislike', 'commentable_id', 'commentable_type', 'is_active','title'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function scopeGet_post($id)
    {
        return Comment::where('id', $id);
    }

    public function scopeGet_comments()
    {
        return Comment::where('id', '>', 0);
    }


    public function setCommentAttribute($value)
    {
        $this->attributes['comment'] = str_replace(PHP_EOL, "<br>", $value);
    }

    public function commentable() {
        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class , 'parent_id' , 'id')->where('is_active', 1)->with(['user'=>function($query){
            $query->with('photo');
        }]);
    }

    public function getAcceptNameAttribute()
    {
        if (isset($this->user_acceptor)) {
            $user = User::where('id', $this->user_acceptor)->first();
            if (isset($user))
                return $user->username;
        }
        return "تایید نشده";
    }

    public function getAcceptDateAttribute()
    {
        if (isset($this->accept_time))
            return edit_date($this->accept_time,'-');
        return "-";
    }

    public function getShamsiAttribute()
    {
        $temp = explode('/', edit_date($this->created_at, '-'));
        if ($temp[2] < 10)
            $temp[2] = (int)$temp[2];
        if ($temp[1] < 10)
            $temp[1] = '0'.$temp[1];

        return $temp[2] . ' ' . get_month()[$temp[1]] . ' ' . $temp[0];
    }

}
