<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name','last_name', 'role_id','photo_id','object_id','email','mobile', 'password','active', 'email_verified_at','remember_token','code'];
    protected $appends=['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function getJWTIdentifier()
//    {
//        return $this->getKey();
//    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
//    public function getJWTCustomClaims()
//    {
//        return [];
//    }

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }

/*    public function getFirstNameAttribute(){
        $name=explode(' ',$this->name);
        return $name[0];
    }*/

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function getImageAttribute()
    {
        return isset($this->photo->path) ? asset($this->photo->path) : asset('site/images/blog/avatar-1.png');
    }

    public function object()
    {
//        if($this->role->slug=='customer')
//            return $this->hasOne(Customer::class);
//        if($this->role->slug=='driver')
//            return $this->hasOne(Driver::class);
    if(auth()->user())
        if(in_array(auth()->user()->role_id,[1,2]))
        {
            $class=auth()->user()->role_id==1?Driver::class:Customer::class;
            return $class::find($this->object_id);
        }

     dd('You Are Admin');
    }

    public function getFullNameAttribute()
    {
        if($this->first_name==null && $this->last_name==null )
            return 'کاربر گرامی';
        $prepends=get_prepends();
        $prepend='';
        if($this->object()->prepend!=null)
            $prepend=$prepends[$this->object()->prepend];
        return $prepend.' '.$this->first_name.' '.$this->last_name;
    }

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }


}
