<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $guarded=['id'];
    protected $table='customer_address';

    public function city(){
        return $this->belongsTo(City::class);
    }
    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
