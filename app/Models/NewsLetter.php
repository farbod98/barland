<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Morilog\Jalali\Jalalian;

class NewsLetter extends Model
{
    protected $fillable=['email'];
    protected $appends=['jalaliDate'];

    public function getJalaliDateAttribute()
    {
        if($this->created_at==null)
            return null;
        $date = Carbon::createFromFormat('Y-m-d H:i:s',  $this->created_at);
        $jalali=Jalalian::fromCarbon($date);
        return $jalali->format('Y-m-d');
    }


}
