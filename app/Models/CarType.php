<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    protected $guarded=['id'];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('active',function ($builder){
            $builder->where('active',1);
        });
    }
    public function car_features()
    {
        return $this->hasMany(CarFeature::class);
    }
    public function prices()
    {
        return $this->hasMany(Price::class,'car_id');
    }
}
