<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    protected $fillable = ['user_id', 'title', 'slug', 'description', 'body', 'photo_id', 'viewed', 'keywords', 'liked', 'socials'];
    protected $appends = ['shamsi'];
    use Sluggable;
    public function getShamsiAttribute()
    {
        $temp = explode('/', edit_date($this->created_at, '-'));
        if ($temp[2] < 10)
            $temp[2] = (int)$temp[2];
        if ($temp[1] < 10)
            $temp[1] = '0'.$temp[1];

        return $temp[2] . ' ' . get_month()[$temp[1]] . ' ' . $temp[0];
    }



    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->where('is_active', 1)->where('parent_id', 0)->with(['user' => function ($query) {
            $query->with('photo');
        }])->with('comments');
    }

    public function comments_delete()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function getImageAttribute()
    {
        return isset($this->photo->path) ? asset($this->photo->path) : asset('site/images/blog/blog-img-1.jpg');
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
