<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $guarded=['id'];
    protected $appends=['user','car_type'];

    /*public function getCarAttribute()
    {
        return CarType::find(Car::find($this->car_id)->type_id);
    }*/
    public function getCarTypeAttribute()
    {
        return $this->car->feature->car_type;
    }

    public function getUserAttribute()
    {
        if(isset(User::where('object_id',$this->id)->where('role_id',1)->first()->id))
            return User::where('object_id',$this->id)->where('role_id',1)->first();
        dd($this->id);
    }
//    public function getMobileAttribute()
//    {
//        return User::where('object_id',$this->id)->first()->mobile;
//    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function orders_request()
    {
        return $this->belongsToMany(Order::class,'driver_order_request')->withPivot('created_at');
    }
    public function get_required_status()
    {
        return -1;
    }
}
