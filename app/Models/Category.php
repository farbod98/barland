<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title','slug'];
    //protected $table = 'categories';

    public static function get_category_hasPost(){
        $posts=Post::all();
        $temps=[];
        foreach($posts as $post)
            foreach ($post->categories as $cat)
                $temps[]=$cat->id;
        return Category::whereIn('id',array_unique($temps))->get();
    }


    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

//    public function questions()
//    {
//        return $this->hasMany(Question::class);
//    }
}
