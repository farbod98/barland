<?php
namespace App\Services\Media;

use App\Photo;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Facades\ImageUpload;

class ImageHelper
{
    public function upload($file, $dir = null)
    {
        $imagesUrl = $this->uploadImages($file, $dir);
        //$original = array_shift($imagesUrl['images']);
        
        /*$photo = Photo::create([
            //'path'=>$original,
            'path'=>$imagesUrl['url'],
            'thumbnail_path'=>$imagesUrl['thumb'],
            'resized_path'=>$imagesUrl['images'],
            'url'=>$imagesUrl['url'],
        ]);*/
        //dd($photo);
        //return $photo;
        return [
            'original' => $imagesUrl['url'],
            'thumb' => $imagesUrl['thumb']
        ];
    }

    public function uploadImages($file, $directory)
    {
        $imagePath = "storage/uploaded_files/document/{$directory}/";
        $fileName = $file->getClientOriginalName();
        if (file_exists(public_path($imagePath)."original/" . $fileName)){
            $fileName = Carbon::now()->timestamp . $fileName;
        }
        $save_path = public_path($imagePath)."original";
        if (!file_exists($save_path)) {
            mkdir($save_path, 0755, true);
        }
        $file = $file->move(public_path($imagePath)."original/", $fileName);
        $sizes = ['300'=>'300'];
        $url['images'] = $this->resize($file->getRealPath(), $sizes, $imagePath, $fileName);
        
        $url['thumb'] = "{$imagePath}thumb/{$url['images']['300_300']}";
        $url['url'] = "{$imagePath}original/{$fileName}";
        return $url;
    }

    public function resize($path, $sizes, $imagePath, $fileName)
    {
        foreach ($sizes as $width=>$height){
            $size = $width.'_'.$height;
            //$size = $width;
            $images[$size] = $imagePath."thumb/" . "{$size}" . $fileName;
            $imagesReturn[$size] =  $size. $fileName;
            $save_path = public_path($imagePath."thumb");
            if (!file_exists($save_path)) {
                mkdir($save_path, 0755, true);
            }
            Image::make($path)->resize($width,$height)->save(public_path($images[$size]));
        }
        return $imagesReturn;
    }
    /*private $image;
    public function upload($image_file, $dir = null)
    {
        $this->image = $image_file;
        $org_path = $this->getOriginalPath($dir);
        $org_path = $this->image->store($org_path);
        list($width, $height) = $this->getThumbRatio($this->image);
        $image_object = $this->getImageObject($this->image, 'jpg');
        $thumb_path = $this->getThumbPath($image_object, $dir);
        if ($dir != null) {
            if (!File::exists(public_path() . 'storage/uploaded_files/document/' . $dir . '/thumb')) {
                $fileDirectory = 'storage/uploaded_files/document/' . $dir . '/thumb';
                //File::makeDirectory(public_path() . '/app/public/uploaded_files/document/' . $dir . '/thumb');
                File::makeDirectory($path=base_path("public/{$fileDirectory}"), $mode = 0755, $recursive = true, $force = true);
            }
        } else {
            if (
            !File::exists(
                public_path() . 'storage/uploaded_files/' . date("Y") . '/' . date("m") . '/thumb')
                //public_path() . '/app/public/uploaded_files/' . date("Y") . '/' . date("m") . '/thumb')
            ) {
                File::makeDirectory(
                    public_path() . 'storage/uploaded_files/' . date("Y") . '/' . date("m") . '/thumb'
                );
            }
        }
        $this->thumbMaker($image_object, $width, $height)->save(public_path() . '/app/' . $thumb_path);
        return [
            'original' => Storage::url($org_path),
            'thumb' => Storage::url($thumb_path)
        ];
    }

    public function getThumbRatio($path)
    {
        list($width, $height) = getimagesize($path);
        if ($height <> $width) {
            $width = $width > $height ? 300 : null;
            $height = $height > $width ? 300 : null;
        } else {
            $width = $height = 300;
        }
        return [$width, $height];
    }

    public function getOriginalPath($dir)
    {
        if ($dir != null)
            return 'public/uploaded_files/document/' . $dir . '/original';
        return 'public/uploaded_files/' . date("Y") . '/' . date("m") . '/original';
    }

    public function getThumbPath($image_object, $dir)
    {
        if ($dir != null) {
            $hash = md5(time() . $image_object->__toString());
            return 'public/uploaded_files/document/' . $dir . '/thumb/' . "{$hash}" . '.jpg';
        } else {
            $hash = md5(time() . $image_object->__toString());
            return 'public/uploaded_files/' . date("Y") . '/' . date("m") . '/thumb/' . "{$hash}" . '.jpg';
        }
    }

    public function getImageObject($path, $type)
    {
        return Image::make($path)->encode($type);
    }

    public function thumbMaker($image_object, $width, $height)
    {
        return $image_object->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
    }*/
}