<?php
/**
 * Created by PhpStorm.
 * User: masoud
 * Date: 5/26/18
 * Time: 3:41 PM
 */

namespace App\Services\Response;


/**
 * Class ClientResponseBuilder
 * @package App\one\Infrastructure\Service\Response
 */
class ClientResponseBuilder extends AbstractResponseBuilder
{
    /**
     * @var
     */
    protected $redirect;

    /**
     * ClientResponseBuilder constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setStatus(200);
        $this->addHeader("Content-Type", "application/json");
    }

    /**
     * @param string|null $message
     * @return AbstractResponseBuilder
     */
    public function setMessage(string $message = null): AbstractResponseBuilder
    {
        return $this->addData(["message_fa" => $message]);
    }

    /**
     * @param int $status
     * @return AbstractResponseBuilder
     */
    public function setStatus(int $status)
    {
        $status = $status == 200;

        return $this->addData(compact("status"));
    }

    /**
     * @param string|null $redirectTo
     * @return AbstractResponseBuilder
     */
    public function setRedirect(string $redirectTo = null): AbstractResponseBuilder
    {
        return $this->addData(["go_to" => $redirectTo]);
    }

    /**
     * @param array|null $result
     * @return AbstractResponseBuilder
     */
    public function setResult(array $result = null): AbstractResponseBuilder
    {
        return $this->addData(compact("result"));
    }
}