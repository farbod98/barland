<?php
/**
 * Created by PhpStorm.
 * User: masoud
 * Date: 5/26/18
 * Time: 3:40 PM
 */

namespace App\Services\Response;

use Illuminate\Http\Response;


abstract class AbstractResponseBuilder
{
    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $content = [];

    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * @param string $key
     * @param $value
     * @return AbstractResponseBuilder
     */
    public function addHeader(string $key, $value): AbstractResponseBuilder
    {
        $this->response->header($key, $value, true);

        return $this;
    }

    /**
     * add data to response content
     *
     * @param array $data
     * @return AbstractResponseBuilder
     */
    public function addData(array $data): AbstractResponseBuilder
    {
        $this->content = array_merge($this->content, $data);

        return $this;
    }

    /**
     * Set Response http status code
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->response->setStatusCode($status);

        return $this;
    }

    /**
     * helper for 200 status
     * @return AbstractResponseBuilder
     */
    public function setOkStatus()
    {
        return $this->setStatus(200);
    }

    public function setNotFoundStatus($message = "")
    {
        if ($message) {
            $this->setMessage($message);
        }

        return $this->setStatus(404);
    }

    public function setInternalServerErrorStatus($message = "")
    {
        if ($message) {
            $this->setMessage($message);
        }

        return $this->setStatus(500);
    }

    public function setUnauthorizedStatus($message = "")
    {
        if ($message) {
            $this->setMessage($message);
        }

        return $this->setStatus(401);
    }

    /**
     * Finalize building process and send response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render()
    {
        if (
            app()->bound('debugbar') &&
            app('debugbar')->isEnabled()
        ) {
            $this->addData([
                //'_debugbar' => app('debugbar')->getData(),
            ]);
        }

        $this->response->setContent($this->content);

        $this->response->send();
    }

    abstract public function setResult(array $result = null): AbstractResponseBuilder;

    abstract public function setMessage(string $message = null): AbstractResponseBuilder;

    abstract public function setRedirect(string $redirectTo = null): AbstractResponseBuilder;

    //    abstract public function setData (array $data): AbstractResponseBuilder;

}