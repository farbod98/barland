<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoginLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('login_logs')->insert([
            'user_id'=>'1',
            'ip'=>'127.0.0.1',
            'type'=>'forget',
        ]);
    }
}
