<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param \Faker\Generator $faker
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        DB::table('users')->insert([
            'username'=>'majid',
            'name'=>'مجید ذوالفقاری',
            'email'=>'majidzkaren@gmail.com',
            'mobile'=>'09353322681',
            'password'=>bcrypt('123456'),
            'sex'=>1,
            'is_active'=>1,
        ]);
    }
}
