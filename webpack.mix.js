const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js');
//    .sass('resources/sass/app.scss', 'public/css');


mix.combine([
    'public/metronic-assets/perfect-scrollbar/css/perfect-scrollbar.rtl.css',
    'public/metronic-assets/animate.css/animate.min.rtl.css',
    'public/metronic-assets/toastr/css/toastr.min.rtl.css',
    'public/metronic-assets/sweetalert2/css/sweetalert2.min.rtl.css',
    'public/custom-assets/css/style.bundle.rtl.min.css',
],'public/mix-assets/css/all-rtl.css');

mix.combine([
    'public/metronic-assets/perfect-scrollbar/css/perfect-scrollbar.css',
    'public/metronic-assets/animate.css/animate.min.css',
    'public/metronic-assets/toastr/css/toastr.min.css',
    'public/metronic-assets/sweetalert2/css/sweetalert2.min.css',
    'public/custom-assets/css/style.bundle.min.css',
],'public/mix-assets/css/all.css');

mix.js('resources/js/app.js', 'public/mix-assets/js/app.js');

mix.combine([
    'public/metronic-assets/js-cookie/js.cookie.js',
    'public/metronic-assets/moment/moment.min.js',
    'public/metronic-assets/tooltip.js/tooltip.min.js',
    'public/metronic-assets/perfect-scrollbar/js/perfect-scrollbar.min.js',
    'public/metronic-assets/sticky-js/sticky.min.js',
    'public/metronic-assets/wnumb/wNumb.js',
    'public/custom-assets/js/scripts.bundle.min.js',
    'public/metronic-assets/sweetalert2/js/sweetalert2.min.js',
    'public/metronic-assets/toastr/js/toastr.min.js',
],'public/mix-assets/js/all.js');


