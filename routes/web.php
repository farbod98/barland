<?php

use App\Models\Order;
use App\Models\User;
use App\Models\Customer;
use Illuminate\Http\Request;
use Ipecompany\Smsirlaravel\Smsirlaravel;

Route::get('/', function () {
    dd(1);
    return view('pages.home-page');
});
Route::get('/submit', function () {
    return response()->json(['status'=>'success', 'message'=>'عملیات موفق']);
});

Auth::routes();
Route::get('/locked', 'HomeController@locked')->name('locked');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/unlock', 'HomeController@unlock')->name('unlock');
Route::post('/login_attempt_count', 'Auth\LoginController@attempt_count');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/blog/{filter?}', 'HomeController@blog')->name('blog');
Route::get('/search', 'HomeController@blog_search')->name('blog_search');
Route::post('/blog/store_comment', 'HomeController@store_comment')->name('store_comment');
Route::get('/blog-single/{slug}', 'HomeController@blog_single')->name('blog_single');

Route::delete('/customer/delete','CustomerController@destroy');
Route::put('/customer/update','CustomerController@update');

//Route::post('/receiver/store','ReceiverController@store');
//Route::get('/receiver/delete/{id}','ReceiverController@delete');
//Route::post('/receiver/update/{id}','ReceiverController@update');

//Route::post('/product/store','ProductController@store');
//Route::get('/product/delete/{id}','ProductController@delete');
//Route::post('/product/update/{id}','ProductController@update');

//Route::post('/car/store','CarController@store');
//Route::get('/car/delete/{id}','CarController@delete');
//Route::post('/car/update/{id}','CarController@update');
Route::group(['middleware' => ['isAdmin'],'prefix'=>'admin','namespace'=>'Admin'], function () {
    Route::resource('customer','CustomerController');
    Route::get('customer/manage_address/{id}','CustomerController@manage_address');
    Route::resource('order','OrderController');
    Route::resource('driver','DriverController');
    Route::resource('car','CarController');
    Route::resource('car_type','CarTypeController');
    Route::resource('car_feature','CarFeatureController');
    Route::resource('province','ProvinceController');
    Route::resource('city','CityController');
    Route::resource('price','PriceController');
    Route::get('car_price/{id}','PriceController@car_price');
    Route::get('driver_request/{id}','DriverController@driver_request');
    Route::post('driver_accept','DriverController@driver_accept');
    Route::resource('packing','PackingController');
    Route::resource('product','ProductController');
    Route::get('customer/prepends/get','CustomerController@prepends');
    Route::get('driver/get/details','DriverController@get');
    Route::get('order/all/get','OrderController@get_details');
    Route::get('receiver/get/details','ReceiverController@details');
    Route::get('customer/{id}/address','CustomerController@get_address');
    Route::post('customer/store_data/address','CustomerController@store_data');
    Route::resource('receiver','ReceiverController');
    Route::get('customer/address/{id}','CustomerController@address');
    Route::resource('customer_address','CustomerAddressController');
    Route::get('contact','HomeController@contact_all');
    Route::get('news_letter','HomeController@news_letter');
    Route::get('gift','HomeController@gift');
    Route::PUT('gift/{id}','HomeController@gift_update');
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('order','OrderController');
    Route::post('order/get/address','OrderController@get_address')->name('user.get_address');
    Route::post('order/edit/address','OrderController@edit_address')->name('user.edit_address');
    Route::post('order/get/all_address','OrderController@get_all_address_request')->name('get_all_address_request');
    Route::post('order/get/calculate_way_price','OrderController@calculate_way_price')->name('calculate_way_price');

    Route::post('/order', 'OrderController@index')->name('order.index.post');
    Route::post('/order_request/store', 'OrderController@store')->name('order_request.store');
});



//Route::resource('customer','CustomerController');
Route::get('customer/status/{id}','CustomerController@status')->name('custmer_status');
Route::resource('customer_address','CustomerAddressController');

Route::get('/bars/{filters}', 'OrderController@bars')->name('bars')->middleware('isDriver');
Route::get('/bars', 'OrderController@bars')->name('bars')->middleware('isDriver');

Route::resource('car','CarController');
Route::resource('product','ProductController');
Route::resource('car_type','CarTypeController');
Route::resource('driver','DriverController');
Route::resource('customer','CustomerController');
Route::resource('user','UserController');
Route::resource('receiver','ReceiverController');
Route::get('user/trips/old','UserController@old_trips')->name('user.old_trips');
Route::get('user/trips/request','UserController@request_trips')->name('user.request_trips');
Route::get('user/bars/request','UserController@request_bars')->name('user.request_bars');
Route::get('user/bars/finished','UserController@finished_bars')->name('user.finished_bars');
Route::get('user/bars/in_way','UserController@in_way_bars')->name('user.in_way_bars');
Route::get('user/manage/address','UserController@manage_address')->name('user.manage_address');
Route::get('user/manage/password','UserController@change_password')->name('user.change_password');
Route::post('user/update/password','UserController@update_password')->name('user.update_password');
Route::post('user/update/profile','UserController@update_profile')->name('user.update_profile');

Route::get('/','HomeController@index');
Route::get('/login/{type}','Auth\LoginController@login_type');
Route::post('/login_attempt','Auth\LoginController@login_attempt');
Route::get('/confirm','Auth\LoginController@confirm_view');
Route::post('/confirm','Auth\LoginController@confirm_validate');
Route::get('/forgot_password','Auth\LoginController@forgot_password_view');
Route::post('/forgot_password','Auth\LoginController@forgot_password');
//Route::get('/login/{type}','HomeController@login');
//Route::post('/login','HomeController@login_attemp');
//Route::get('/confirm','HomeController@confirm_view');
//Route::post('/confirm','HomeController@confirm');
//Route::get('/forgot_password','HomeController@forgot_password_view');
//Route::post('/forgot_password','HomeController@forgot_password');

Route::get('/get_cities','HomeController@get_cities');
Route::get('/check_phone','HomeController@check_phone');
Route::post('/get_ten_last_request','OrderController@get_ten_last_request')->name('get_ten_last_request');
Route::get('/inquiry','OrderController@inquiry');
Route::post('/get_orders','OrderController@get_orders');
Route::post('driver_accept','OrderController@driver_accept');
Route::post('contact','homeController@contact_submit')->name('contact');
Route::post('news_letter','homeController@news_letter_store')->name('contact');
Route::get('/test',function (){
    dd(auth()->user());
});
