<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['prefix'=>'v1','namespace'=>'Api\v1'],function (){

    Route::post('login', 'ApiController@login');
    Route::post('register', 'ApiController@register');
    Route::post('check', 'OrderController@check');

    Route::post('get_orders','OrderController@get_orders');
    Route::post('request_bars','OrderController@request_bars');
    Route::post('in_way_bars','OrderController@in_way_bars');
    Route::post('finished_bars','OrderController@in_way_bars');
    Route::post('driver_accept','OrderController@driver_accept');
    Route::post('store_order','OrderController@store_order');
    Route::post('get_ten_last_request','OrderController@get_ten_last_request');
    Route::post('store_address','OrderController@store_address');
});
