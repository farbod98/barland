<?php

namespace Tests\Unit;

use App\Http\Controllers\OrderController;
use App\Models\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateOrder()
    {
        $this->assertFalse(true);

        $arr= [
        "step_one_bar_car_type" => "2",
  "step_one_bar_car_details" =>  [
        0 => "1"
    ],
  "step_one_bar_title" => "3",
  "step_one_bar_box_type" => "3",
  "step_one_bar_weight" => "12",
  "step_one_bar_cargo_value" => "2",
  "step_one_bar_bascule_price" => "0",
  "step_one_bar_lading_price" => "0",
  "step_one_bar_vacate_price" => "0",
  "step_one_bar_description" => null,
  "step_two_bar_sender_place" => "4",
  "step_two_bar_sender_title" => "add1",
  "step_two_bar_sender_province" => "5",
  "step_two_bar_sender_city" => "278",
  "step_two_bar_sender_address" => "شهرستان ورامین, ایران",
  "step_two_bar_sender_no" => "2",
  "step_two_bar_sender_unit" => "2",
  "step_two_bar_sender_zoom" => "5",
  "step_two_bar_receiver_place" => "5",
  "step_two_bar_receiver_province" => "5",
  "step_two_bar_receiver_city" => "279",
  "step_two_bar_receiver_name" => "rec1",
  "step_two_bar_receiver_mobile" => "09192323233",
  "step_two_bar_receiver_phone" => "2",
  "step_two_bar_receiver_address" => "اندوهجرد, بخش شهداد, شهرستان کرمان, کرمان, ایران",
  "step_two_bar_receiver_no" => "2",
  "step_two_bar_receiver_unit" => "2",
  "step_two_bar_receiver_zoom" => "6",
  "step_two_bar_date" => "0",
  "step_two_bar_hour_from" => "16:01",
  "step_two_bar_hour_to" => "16:01",
  "step_four_bar_price" => "133,131",
  "token" => "lKJDWgw8GaYBGqOcSc1RcKB47KOTnB0aGOua1g2e",
  "tariff_request" => "150,000",
  "average_request" => "10,000,000",
];
        $order = new OrderController();
        $input = $order->get_order_fields($arr);
        $order = Order::create($input);
        $this->assertTrue($order);
    }
}
