"use strict";

var KTSessionTimeoutDemo = function () {

    var initDemo = function () {
        $.sessionTimeout({
            title: sessionTimeout_title,
            message: sessionTimeout_message,
            keepAliveUrl: '/submit',
            redirUrl: '/locked',
            logoutUrl: '/logout',
            warnAfter: 3600000, //warn after 5 seconds//after 1 hour
            redirAfter: 35000, //redirect after 10 secons,
            ignoreUserActivity: true,
            countdownMessage: `${sessionTimeout_timer_0} {timer} ${sessionTimeout_timer_1}`,
            countdownBar: true,
            logoutButton:sessionTimeout_logoutButton,
            keepAliveButton:sessionTimeout_keepAliveButton,
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initDemo();
        }
    };

}();

jQuery(document).ready(function() {    
    KTSessionTimeoutDemo.init();
});