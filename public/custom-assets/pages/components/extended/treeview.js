"use strict";

var KTTreeview = function () {
    var demo4 = function() {
        $("#kt_tree_4").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data': [{
                        "text": "Parent Node",
                        "children": [{
                            "text": "Initially selected",
                            "state": {
                                "selected": true
                            }
                        }, {
                            "text": "Custom Icon",
                            "icon": "fa fa-warning kt-font-danger"
                        }, {
                            "text": "Initially open",
                            "icon" : "fa fa-folder kt-font-success",
                            "state": {
                                "opened": true
                            },
                            "children": [
                                {"text": "Another node", "icon" : "fa fa-file kt-font-waring"}
                            ]
                        }, {
                            "text": "Another Custom Icon",
                            "icon": "fa fa-warning kt-font-waring"
                        }, {
                            "text": "Disabled Node",
                            "icon": "fa fa-check kt-font-success",
                            "state": {
                                "disabled": true
                            }
                        }, {
                            "text": "Sub Nodes",
                            "icon": "fa fa-folder kt-font-danger",
                            "children": [
                                {"text": "Item 1", "icon" : "fa fa-file kt-font-waring"},
                                {"text": "Item 2", "icon" : "fa fa-file kt-font-success"},
                                {"text": "Item 3", "icon" : "fa fa-file kt-font-default"},
                                {"text": "Item 4", "icon" : "fa fa-file kt-font-danger"},
                                {"text": "Item 5", "icon" : "fa fa-file kt-font-info"}
                            ]
                        }]
                    },
                    "Another Node 5"
                ]
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder kt-font-brand"
                },
                "file" : {
                    "icon" : "fa fa-file  kt-font-brand"
                }
            },
            "state" : { "key" : "demo2" },
            "plugins" : [ "contextmenu", "state", "types" ]
        });    
    }
    return {init: function () {demo4();}};
}();

jQuery(document).ready(function() {KTTreeview.init();});